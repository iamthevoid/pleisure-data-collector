package iam.thevoid.pleisure.data.util

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction


//fun <T : Table, E : Any> T.rxBatchInsert(
//    data: Iterable<E>,
//    ignore: Boolean = false,
//    body: BatchInsertStatement.(E) -> Unit
//) = completableTransaction { batchInsert(data, ignore, body) }

//fun <Key : Comparable<Key>, T : IdTable<Key>> T.rxInsertAndGetId(body: T.(InsertStatement<EntityID<Key>>) -> Unit) =
//    singleTransaction { insertAndGetId(body) }

//fun <T : Table> T.rxDeleteAll() = completableTransaction { deleteAll() }

//private fun <T : Table, E : Any> T.singleTransaction(block: T.() -> E) =
//    singleHandled<E> { safeTransaction(statement = { onSuccess(block()) }) { onError(it) } }

//private fun <T : Table> T.completableTransaction(block: T.() -> Unit) =
//    completableHandled {
//        safeTransaction(statement = {
//            block()
//            onComplete()
//        }) { onError(it) }
//    }

private fun <T> safeTransaction(
    db: Database? = null,
    statement: Transaction.() -> T,
    errorHandler: (Throwable) -> Unit = { it.printStackTrace() }
) = transaction(db) {
    try {
        addLogger(StdOutSqlLogger)
        statement()
    } catch (e: Exception) {
        e.printStackTrace()
        errorHandler(e)
    }
}