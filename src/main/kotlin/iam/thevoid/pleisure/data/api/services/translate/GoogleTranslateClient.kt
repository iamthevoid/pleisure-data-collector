package iam.thevoid.pleisure.data.api.services.translate

import iam.thevoid.pleisure.data.api.Client
import io.ktor.client.request.parameter
import java.util.*

object GoogleTranslateClient : Client() {

    override val endpoint = "https://translate.google.com"

    suspend fun translate(query : String, lang : String = Locale.getDefault().language) =
        retryWithDelay {
            get<GoogleTranslation>("$endpoint/translate_a/single") {
                parameter("q", query)
                parameter("client", "at")
                parameter("sl", "auto")
                parameter("tl", lang)
                parameter("dt", "t")
                parameter("ie", "UTF-8")
                parameter("oe", "UTF-8")
                parameter("ii", "1dd3b944-fa62-4b55-b330-74909a99969e")
            }
        }
}