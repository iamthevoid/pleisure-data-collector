package iam.thevoid.pleisure.data.api.seller.redkassa

import iam.thevoid.e.safe
import iam.thevoid.e.safeFloat
import iam.thevoid.pleisure.data.api.seller.Distributor
import iam.thevoid.pleisure.data.db.main.model.*

object RedKassa : Distributor(RedKassaClient.endpoint) {

    override suspend fun remoteSuspendSeller(): Seller =
        RedKassaParser.getSeller(RedKassaClient.seller())

    override suspend fun remoteEvents(seller: Seller, place: Place): List<Event> =
        place.sellerUrl.firstOrNull()?.let { url ->
            RedKassaClient.place(url.url)
                .let { RedKassaParser.getMonths(it) }
                .map { RedKassaParser.getEventsShort(RedKassaClient.eventsPerMonth(url.url, it)) }
                .flatten()
                .map { RedKassaParser.event(RedKassaClient.event(it.id), it, url) }
        }.safe()


    override suspend fun remotePlace(seller: Seller, sellerUrl: SellerUrl): Place =
        RedKassaParser.getPlaceFull(RedKassaClient.place(sellerUrl.url))
            .also { place ->
                place.sellerUrl = listOf(sellerUrl)
                RedKassaClient.address(sellerUrl.url)
                    .let { venueHtml ->
                        RedKassaParser.getVenueInfo(venueHtml)?.apply {
                            place.address?.apply {
                                lat = latitude?.safeFloat()
                                lon = longitude?.safeFloat()
                            }
                        }
                    }
            }

    override suspend fun remoteSellerUrlsForPlaces(
        seller: Seller,
        type: SellerUrlType
    ): List<SellerUrl> =
        RedKassaParser.getSellerUrlForPlaces(RedKassaClient.places())
            .map { SellerUrl.create(seller, SellerUrlType.PLACE, it) }
}