package iam.thevoid.pleisure.data.api.services.location.osm

import iam.thevoid.pleisure.data.api.Client
import iam.thevoid.pleisure.data.api.services.location.AddressProvider
import iam.thevoid.pleisure.data.db.main.model.Address
import io.ktor.client.request.parameter

object OSMClient : Client(), AddressProvider {

    override val endpoint: String
        get() = "https://nominatim.openstreetmap.org"

    override suspend fun forward(query: String, additional : String?) = retryWithDelay {
        get<List<OSMAddress>>("$endpoint/search/$query") {
            parameter("format", "json")
            parameter("addressdetails", "addressdetails")
        }
    }.findWith(additional)?.toAddress()

    override suspend fun reverse(lat: Float, lon: Float): Address? = retryWithDelay {
        get<OSMAddress>("$endpoint/reverse") {
            parameter("format", "json")
            parameter("lat", lat)
            parameter("lon", lon)
        }.toAddress()
    }
}