package iam.thevoid.pleisure.data.api.enumerations

@Suppress("EnumEntryName")
enum class EventGenre(val tags: List<String>) {

    /** --------------------------  BBB ----------------------- */

    ballet(listOf(
        "балет",
        "ballet"
    )),

    bard(listOf(
        "Бард",
        "Bards"
    )),

    basketball(listOf(
        "баскетбол",
        "basketball"
    )),

    blues(listOf(
        "блюз",
        "blues"
    )),

    /** --------------------------  CCC ----------------------- */

    chanson(listOf(
        "шансон",
        "chanson"
    )),

    children(listOf(
        "children",
        "детям",
        "дети"
    )),

    classical(listOf(
        "классическая",
        "classic"
    )),

    comedy(listOf(
        "Комедия",
        "comedy"
    )),

    /** --------------------------  EEE ----------------------- */

    electronic(listOf(
        "электронная",
        "electronic"
    )),

    ethnic(listOf(
        "Этника",
        "ethnic"
    )),

    exhibition(listOf(
        "Выставки",
        "Выставка",
        "exhibition",
        "expo"
    )),

    /** --------------------------  FFF ----------------------- */

    fest(listOf(
        "фестиваль",
        "фестивали",
        "fest"
    )),

    folk(listOf(
        "Фолк",
        "Folk"
    )),

    folklore(listOf(
        "Фольклор",
        "Folklore"
    )),

    /** --------------------------  JJJ ----------------------- */

    jazz(listOf(
        "джаз",
        "jazz"
    )),

    /** --------------------------  HHH ----------------------- */

    hiphop(listOf(
        "hip-hop",
        "hiphop"
    )),

    hockey(listOf(
        "Хоккей",
        "hockey"
    )),

    humor(listOf(
        "юмор",
        "humor"
    )),

    /** --------------------------  MMM ----------------------- */

    musical(listOf(
        "Мюзикл",
        "Musical"
    )),

    musical_play(listOf(
        "Музыкальный спектакль",
        "musical play"
    )),

    /** --------------------------  NNN ----------------------- */

    newage(listOf(
        "new age",
        "newage",
        "нью эйдж",
        "ньюэйдж"
    )),

    newYear(listOf(
        "Новогодние елки",
        "New Year"
    )),

    /** --------------------------  OOO ----------------------- */

    opera(listOf(
        "Опера",
        "Opera"
    )),

    other(listOf(
        "Другие",
        "Другое",
        "Other"
    )),

    /** --------------------------  PPP ----------------------- */

    play(listOf(
        "спектакль",
        "play"
    )),

    pop(listOf(
        "поп",
        "pop"
    )),

    /** --------------------------  RRR ----------------------- */

    rap(listOf(
        "rap"
    )),

    recital(listOf(
        "творческий вечер",
        "creative evening",
        "recital",
        "reading excerpts"
    )),

    romance(listOf(
        "romance"
    )),

    rnb(listOf(
        "rnb",
        "R'n'b",
        "ritm and blues"
    )),

    rock(listOf(
        "рок",
        "rock",
        "rock-n-roll",
        "rock-and-roll",
        "rock and roll"
    )),

    /** --------------------------  SSS ----------------------- */

    stage(listOf(
        "эстрада",
        "stage"
    )),

    show(listOf(
        "шоу"
    )),

    /** --------------------------  TTT ----------------------- */

    tour(listOf(
        "экскурсии",
        "экскурсия",
        "tour"
    )),

    /** --------------------------  UUU ----------------------- */

    undefined(listOf()),
    ;

    companion object {

        fun define(tag: String) : List<EventGenre> {
            val lowerCaseTag = tag.toLowerCase()
            val genres = mutableListOf<EventGenre>()
            for (genre in values()) {
                if (genre.tags.any { lowerCaseTag.contains(it, true) })
                    genres.add(genre)
            }

            return if (genres.isEmpty()) listOf(undefined) else genres
        }
    }
}