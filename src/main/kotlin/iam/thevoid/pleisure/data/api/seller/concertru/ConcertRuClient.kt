package iam.thevoid.pleisure.data.api.seller.concertru

import iam.thevoid.pleisure.data.api.Client
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.request.parameter

object ConcertRuClient : Client() {

    override val endpoint = "https://www.concert.ru"

    const val EVENT = "/Event"
    const val PICTURES = "/Pictures"
    const val MAP_DATA = "/MapData"
    const val PLATFORM = "/Platform/Platform"
    const val PLATFORMS = "/Platforms"

    override val logLevel: LogLevel
        get() = LogLevel.INFO

    suspend fun seller() =
        retryWithDelay { get<String>(endpoint) }

    suspend fun places() =
        retryWithDelay { get<String>("$endpoint$PLATFORMS") }

    suspend fun place(id: String, page: Int = 1) =
        retryWithDelay {
            get<String>("$endpoint$PLATFORM") {
                parameter("ActionPlaceID", id)
                parameter("SortType", 0)
                parameter("page", page)
            }
        }

    suspend fun mapData(id: String) =
        retryWithDelay {
            get<String>("$endpoint/$MAP_DATA") {
                parameter("ActionPlaceID", id)
            }
        }

    suspend fun event(id: String) =
        retryWithDelay {
            get<String>("$endpoint$EVENT") {
                parameter("ActionID", id)
            }
        }
}