package iam.thevoid.pleisure.data.api.services.location.locationlq

import iam.thevoid.pleisure.data.api.Client
import iam.thevoid.pleisure.data.api.services.location.AddressProvider
import iam.thevoid.pleisure.data.db.main.model.Address
import io.ktor.client.features.ClientRequestException
import io.ktor.client.request.parameter
import kotlinx.coroutines.delay
import java.net.HttpURLConnection

object LocationlQClient : Client(), AddressProvider {

    override val endpoint: String
        get() = "https://eu1.locationiq.com"

    suspend fun query(query: String): List<LocationlQAddress> = retryWithDelay {
        try {
            get<List<LocationlQAddress>>("$endpoint/v1/search.php") {
                parameter("q", query)
                parameter("format", "json")
                parameter("addressdetails", 1)
                parameter("key", "8c60f78819c052")
            }
        } catch (e: ClientRequestException) {
            when {
                e.response.status.value == HttpURLConnection.HTTP_NOT_FOUND -> emptyList()
                e.response.status.value == 429 -> {
                    delay(1000)
                    query(query)
                }
                else -> throw e
            }
        }
    }

    override suspend fun forward(query: String, additional: String?) =
        query(query).let { it.findWith(additional)?.toAddress(query) }

    override suspend fun reverse(lat: Float, lon: Float): Address? =
        retryWithDelay {
            try {
                get<LocationlQAddress>("$endpoint/v1/reverse.php") {
                    parameter("lat", lat)
                    parameter("lon", lon)
                    parameter("format", "json")
                    parameter("addressdetails", 1)
                    parameter("key", "8c60f78819c052")
                }.toAddress()
            } catch (e : ClientRequestException) {
                when {
                    e.response.status.value == HttpURLConnection.HTTP_NOT_FOUND -> null
                    e.response.status.value == HttpURLConnection.HTTP_BAD_REQUEST -> null
                    e.response.status.value == 429 -> {
                        delay(1000)
                        reverse(lat, lon)
                    }
                    else -> throw e
                }
            }
        }
}

