package iam.thevoid.pleisure.data.api.services.location.here

import iam.thevoid.pleisure.data.api.Client
import iam.thevoid.pleisure.data.api.services.location.AddressProvider
import iam.thevoid.pleisure.data.db.main.model.Address
import io.ktor.client.request.parameter

object HereClient : Client(), AddressProvider {

    const val reverseEndpoint = "https://reverse.geocoder.api.here.com"

    override val endpoint: String
        get() = "https://geocoder.api.here.com"

    override suspend fun forward(query: String, additional : String?) =
        retryWithDelay {
            get<HereResponse>("$endpoint/6.2/geocode.json") {
                parameter("searchtext", query)
                parameter("maxresults", 100)
                parameter("app_id", "5VGfnHWJK9irRsrTkXaY")
                parameter("app_code", "fwFWFG7qWL9cWROAKUucOA")
            }
        }.response?.view?.firstOrNull()?.result?.findWith(additional)?.toAddress(query)

    override suspend fun reverse(lat: Float, lon: Float): Address? =
        retryWithDelay {
            get<HereResponse>("$reverseEndpoint/6.2/reversegeocode.json") {
                parameter("prox", "$lat,$lon")
                parameter("mode", "retrieveAddresses")
                parameter("app_id", "5VGfnHWJK9irRsrTkXaY")
                parameter("app_code", "fwFWFG7qWL9cWROAKUucOA")
            }.response?.view?.firstOrNull()?.result?.firstOrNull()?.toAddress()
        }
}