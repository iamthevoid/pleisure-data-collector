package iam.thevoid.pleisure.data.api.seller.concertru

import iam.thevoid.e.calendar
import iam.thevoid.e.firstOrNull
import iam.thevoid.e.safe
import iam.thevoid.e.year
import iam.thevoid.pleisure.data.db._manytoone.model.Period
import iam.thevoid.pleisure.utils.*
import iam.thevoid.pleisure.utils.ext.containsAny
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object ConcertRuDateParser {

    enum class Format(pattern: String, locale: Locale = Locale.getDefault()) {
        dateHyphenFormat("dd-MM-yyyy"),
        dateTimePointFormat("dd.MM.yyyy HH:mm"),
        datePointFormat("dd.MM.yyyy"),
        dateMonthFormat("dd.MM"),

        ruDateYearTimeFormat("dd MMMM yyyy HH:mm", Locale("ru")),
        ruDateTimeFormat("dd MMMM HH:mm", Locale("ru")),
        ruDateYearFormat("dd MMMM yyyy", Locale("ru")),
        ruDayInMonthFormat("dd MMMM", Locale("ru"));

        val format = SimpleDateFormat(pattern, locale)
    }

    private val dash by lazy { "($SPACES_RGX$DASH$SPACES_RGX)" }

    private val periodPatterns by lazy {
        listOf(
            "\\d{1,2}.\\d{1,2}.\\d{4}$dash\\d{1,2}.\\d{1,2}.\\d{4}",
            "\\d{1,2}.\\d{1,2}\\.$dash\\d{1,2}.\\d{1,2}.\\d{4}",
            "\\d{1,2}.\\d{1,2}$dash\\d{1,2}.\\d{1,2}.\\d{4}",
            "\\d{1,2}.\\d{1,2}$dash\\d{1,2}.\\d{1,2}"
        )
    }

    private val String.dirty
        get() = "($SPACES_RGX|\\p{L})*$this.*"

    private val String.pattern
        get() = Pattern.compile(this)

    private val daysPeriodPattern by lazy { Pattern.compile(".*$SPACES_RGX\\d{1,2}$dash\\d{1,2}.*") }

    private val openDatePattern by lazy {
        Pattern.compile(
            "(\\p{L}|\\s|\"|$HYPHEN_RGX|$DASH)+",
            Pattern.CASE_INSENSITIVE
        )
    }

    fun parseRawDates(rawDates: List<String>) = rawDates
        .map { it.trim().replace(HYPHEN, DASH).replace(Regex(RU_UNTIL_RGX), DASH) }
        .map { preformattedDate ->
            when {
                openDatePattern.matcher(preformattedDate).matches() -> listOf(Period.open)
                preformattedDate.containsAny(DASH, RU_UNTIL_RGX) -> extractPeriod(preformattedDate)
                else -> listOf(Period.create(extractDate(preformattedDate)))
            }
        }.flatten()

    private fun extractPeriod(periodStr: String): List<Period> =
        preparePeriod(periodStr)
            .let { period ->
                when {
                    period.matches(*(periodPatterns.map { it.pattern }.toTypedArray())) -> period
                        .split(Regex(dash))
                        .let { extractDatesFromPeriod(it) }
                        .let { DateUtil.makeDaysRange(it[0], it[1]) }
                        .map { Period.create(it) }
                    period.matches(daysPeriodPattern) -> extractDaysPeriod(period)
                    else -> throw ParseException("Unparcelable period $period", 0)
                }
            }

    private fun preparePeriod(periodStr: String): String {
        periodPatterns.forEach {
            if (it.dirty.pattern.matcher(periodStr).matches())
                return periodStr.firstOrNull(it).safe()
        }
        return periodStr
    }

    private fun extractDaysPeriod(periodStr: String): List<Period> {
        val parts = periodStr.split(Regex(dash)).map { it.trim() }
        val endDateStr = parts[1]
        val endDate =
            extractDate(endDateStr)
        val startDate = endDate.calendar.apply { set(Calendar.DAY_OF_MONTH, parts.first().toInt()) }.time
        return DateUtil.makeDaysRange(
            startDate,
            endDate
        ).map { Period.create(it) }
    }

    private fun prepareDate(dateStr: String): String =
        dateStr
            .replace(Regex("\\s*г\\.\\s*"), " ")
            .replace(Regex("\\s*г\\. в\\s*"), " ")
            .replace(Regex("\\s*с \\s*"), " ")
            .replace(Regex("\\s*начало в\\s*"), " ")
            .trim()

    private fun extractDatesFromPeriod(period: List<String>): List<Date> {
        require(period.size == 2) { "${period.joinToString("-")} is not the period" }

        val start = extractDate(period[0]).calendar
        val end = extractDate(period[1]).calendar

        while (start.after(end))
            start.set(Calendar.YEAR, start.year - 1)

        return listOf(start.time, end.time)
    }

    private fun extractDate(dateStr: String): Date =
        DateUtil.extractDate(prepareDate(dateStr), *(Format.values().map { it.format }.toTypedArray()))

    private fun String.matches(vararg patterns: Pattern) =
        patterns.any { it.matcher(this).matches() }
}