package iam.thevoid.pleisure.data.api.services.location

import iam.thevoid.e.isNotNull
import iam.thevoid.e.remove
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.api.services.location.here.HereClient
import iam.thevoid.pleisure.data.api.services.location.locationlq.LocationlQClient
import iam.thevoid.pleisure.data.api.services.location.mapbox.MapboxClient
import iam.thevoid.pleisure.data.api.services.location.osm.OSMClient
import iam.thevoid.pleisure.data.db.main.model.Address
import iam.thevoid.pleisure.data.db.main.model.Place
import iam.thevoid.pleisure.data.db.main.model.full
import iam.thevoid.pleisure.utils.*
import iam.thevoid.pleisure.utils.ext.findNotNullSuspended

// Try to get address from Here and then from LocationLQ if not found
object LocationHelper {

    private val addressProviders by lazy {
        listOf<AddressProvider>(
            OSMClient,
            HereClient,
            LocationlQClient,
            MapboxClient
        )
    }

    suspend fun address(place: Place, additional : String?) =
        place.address?.takeIf { it.full() }
            ?: reverse(place.address?.lat, place.address?.lon)
            ?: forward(place.address?.raw, place.name, additional)

    private suspend fun reverse(lat: Float?, lon: Float?) =
        lat?.let { lt -> lon?.let { ln -> Pair(lt, ln) } }
            ?.takeIf { (lt, ln) -> lt != 0.0f && ln != 0.0f }
            ?.let { (lt, ln) -> addressProviders.findNotNullSuspended { it.reverse(lt, ln) } }

    private suspend fun forward(raw: String?, name: String, additional: String?): Address? =
        byRaw(raw, additional) ?: byName(name, additional) ?: byRaw(Associations.prepare(raw.safe()), additional)

    private suspend fun byRaw(raw: String?, additional: String?): Address? {
        var query: String
        var address: Address?
        val city = additional?.takeIf { Associations.cityFor(it) != null }

        raw.takeIf { it.safe().isNotEmpty() }?.let { rawAddress ->

            query = "${city.safe()} ${removeBraceContainments(rawAddress)}".trim()
            address = query(query, city)
            if (address.isNotNull())
                return address

            query = query.remove(city.safe()).trim()
            address = query(query, city)
            if (address.isNotNull())
                return address

            query = removeMetroStation(query)
            address = query(query, city)
            if (address.isNotNull())
                return address

            query = removeBuilding(query)
            address = query(query, city)
            if (address.isNotNull())
                return address

            query = removeEnbankment(query)
            address = query(query, city)
            if (address.isNotNull())
                return address
        }

        return null
    }

    private suspend fun byName(name: String, additional: String?): Address? {
        var query: String
        var address: Address?
        val city = additional?.takeIf { Associations.cityFor(it) != null }
        name.takeIf { it.isNotEmpty() }?.let {

            query = "${city.safe()} ${removeBraceContainments(name)}".trim()
            address = query(query, city)
            if (address.isNotNull())
                return address

            query = query.remove(city.safe()).trim()
            address = query(query, city)
            if (address.isNotNull())
                return address

            query = removePavilion(query)
            address = query(query, city)
            if (address.isNotNull())
                return address
        }

        return null
    }

    suspend fun query(query: String, additional: String?): Address? =
        query.takeIf { it.isNotBlank() }?.let { prepared ->
            addressProviders.findNotNullSuspended { it.forward(prepared, additional) }
        }
}