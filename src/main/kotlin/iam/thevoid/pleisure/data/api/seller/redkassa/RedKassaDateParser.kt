package iam.thevoid.pleisure.data.api.seller.redkassa

import iam.thevoid.e.parseSafe
import iam.thevoid.e.safeInt
import iam.thevoid.pleisure.data.db._manytoone.model.Period
import iam.thevoid.pleisure.utils.DASH
import iam.thevoid.pleisure.utils.DateUtil
import java.text.SimpleDateFormat
import java.util.*

object RedKassaDateParser {

// listOf("22.12.2019"),
// listOf("с 11 декабря 2019 г. по 9 января 2020 г.")

    enum class Format(val pattern: String, locale: Locale = Locale.getDefault()) {
        DATE_POINT("dd.MM.yyyy"),
        DATE_YEAR_RU("dd MMMM yyyy", Locale("ru"));

        val format = SimpleDateFormat(pattern, locale)
    }

    fun periods(rawTime : String, rawDate: String): List<Period> =
        Format.DATE_POINT.format.parseSafe(rawDate)?.let { date -> listOf(date) }?.toPeriods(rawTime)
            ?: rawDate.takeIf { it.isNotBlank() }
                ?.replace("с ", "")
                ?.replace("по", DASH)
                ?.replace("г.", "")
                ?.let { extractPeriodDates(it) }?.toPeriods(rawTime)
            ?: listOf(Period.unknown)

    private fun List<Date>.toPeriods(rawTime : String) =
        rawTime.split(":").takeIf { it.size == 2 }?.let { it[0].safeInt() * 60 + it[1].safeInt() }
            ?.let { timeInt -> map { date -> Period.create(timeInt, date) } }

    private fun extractPeriodDates(periodStr: String): List<Date> =
        periodStr
            .split(DASH)
            .map { it.trim() }
            .mapNotNull { Format.DATE_YEAR_RU.format.parse(it) }
            .let { DateUtil.makeDaysRange(it[0], it[1]) }
}