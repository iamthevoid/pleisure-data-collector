package iam.thevoid.pleisure.data.api.services.location.mapbox

import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.api.Client
import iam.thevoid.pleisure.data.api.services.location.AddressProvider
import iam.thevoid.pleisure.data.db.main.model.Address
import iam.thevoid.pleisure.utils.ext.fromJson
import io.ktor.client.request.parameter

object MapboxClient : Client(), AddressProvider {

    @Suppress("SpellCheckingInspection")
    private const val accessToken = "pk.eyJ1IjoiaWFtdGhldm9pZCIsImEiOiJjazI5MmExajgzY2k4M2htdng4ZHN2ZWZwIn0.kxjYCM4DbybA6Or9o6pifg"

    override val endpoint: String
        get() = "https://api.mapbox.com"

    override suspend fun forward(query: String, additional : String?) = retryWithDelay {
        get<String>("$endpoint/geocoding/v5/mapbox.places/$query.json") {
            parameter("access_token", accessToken)
            parameter("types", "country,region,district,place,locality,neighborhood,address")
        }.fromJson<MapboxResponse>()?.features.safe().findWith(additional)?.toAddress()
    }

    override suspend fun reverse(lat: Float, lon: Float): Address? = retryWithDelay {
        get<String>("$endpoint/geocoding/v5/mapbox.places/$lon,$lat.json") {
            parameter("access_token", accessToken)
            parameter("types", "country,region,district,place,locality,neighborhood,address")
        }.fromJson<MapboxAddress>()?.toAddress()
    }
}