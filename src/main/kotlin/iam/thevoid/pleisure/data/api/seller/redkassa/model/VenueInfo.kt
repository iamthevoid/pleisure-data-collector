package iam.thevoid.pleisure.data.api.seller.redkassa.model

/*
{
  "vueElementId": "#vue_venue",
  "pageAlias": "venue",
  "afishaSectionAlias": "afisha",
  "repertoireSectionAlias": "repertoire",
  "monthParameterSlug": "month",
  "defaultSelectedSection": "afisha",
  "afishaListingUrl": "/Venue/AfishaListing",
  "venueId": "9e894d7d-a351-4280-8a36-3cab3dbe599a",
  "selectedMonth": "december",
  "latitude": "",
  "longitude": "",
  "mapPinIconUrl": "/Themes/Old/Content/img/contacts/ico-red_pin.svg",
  "mapElementId": "venue_address__map",
  "repertoireListingUrl": "/Catalog/EventListingItems",
  "isAfishaSectionAvailable": true,
  "isRepertoireSectionAvailable": true
}
 */

data class VenueInfo(
    val vueElementId: String?,
    val pageAlias: String?,
    val afishaSectionAlias: String?,
    val repertoireSectionAlias: String?,
    val monthParameterSlug: String?,
    val defaultSelectedSection: String?,
    val venueId: String?,
    val selectedMonth: String?,
    val latitude: String?,
    val longitude: String?,
    val mapPinIconUrl: String?,
    val mapElementId: String?,
    val repertoireListingUrl: String?,
    val isAfishaSectionAvailable: Boolean,
    val isRepertoireSectionAvailable: Boolean
)