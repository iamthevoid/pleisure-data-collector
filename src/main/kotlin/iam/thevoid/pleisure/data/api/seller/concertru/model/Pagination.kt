package iam.thevoid.pleisure.data.api.seller.concertru.model

data class Pagination(val from: Int, val to: Int, val total: Int, val hasMore: Boolean = total > to)