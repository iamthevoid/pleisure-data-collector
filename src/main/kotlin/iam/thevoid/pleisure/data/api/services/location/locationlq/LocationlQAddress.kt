package iam.thevoid.pleisure.data.api.services.location.locationlq

import com.google.gson.annotations.SerializedName
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.api.services.location.Associations
import iam.thevoid.pleisure.data.db.main.model.Address
import iam.thevoid.pleisure.data.db.main.model.full
import iam.thevoid.pleisure.utils.ext.containsContentIgnoreCase


/*
{
        "place_id": "0",
        "osm_type": "way",
        "osm_id": "163638607",
        "licence": "© LocationIQ.com CC BY 4.0, Data © OpenStreetMap contributors, ODbL 1.0",
        "lat": "55.807484",
        "lon": "37.511623",
        "display_name": "80 к17, Ленинградский проспект, Sokol, Moscow, JAkimanka, Moscow, Russia, 125315",
        "boundingbox": [
            "55.807484",
            "55.808132",
            "37.510696",
            "37.511623"
        ],
        "importance": 0.2,
        "address": {
            "house_number": "80 к17",
            "road": "Ленинградский проспект",
            "neighbourhood": "Sokol",
            "city": "Moscow",
            "county": "JAkimanka",
            "state": "Moscow",
            "country": "Russia",
            "postcode": "125315",
            "country_code": "ru"
        }
    }
 */

class LocationlQAddress {
    @SerializedName("lat")
    var lat: Float = 0f
    @SerializedName("lon")
    var lon: Float = 0f
    @SerializedName("display_name")
    var displayName: String = ""
    @SerializedName("address")
    var address : Address =
        Address()

    class Address {
        @SerializedName("country")
        var country: String = ""
        @SerializedName("country_code")
        var countryCode: String = ""
        @SerializedName("city")
        var city: String = ""
        @SerializedName("town")
        var town: String = ""
        @SerializedName("hamlet")
        var hamlet: String = ""
        @SerializedName("village")
        var village: String = ""
        @SerializedName("state")
        var state: String = ""

    }
}

fun LocationlQAddress.toAddress(raw : String = displayName) = Address.create(
    address.run { Associations.cityFor(state, city, town, hamlet, village) },
    Associations.countryFor(address.country),
    Associations.codeFor(address.countryCode),
    raw,
    lat,
    lon
).takeIf { it.full() }

fun List<LocationlQAddress>.findWith(additional: String?) =
    additional?.let {
        find { address ->
            address.address.run {
                Associations.cityFor(city)?.containsContentIgnoreCase(it).safe() ||
                Associations.cityFor(town)?.containsContentIgnoreCase(it).safe() ||
                        Associations.cityFor(state)?.containsContentIgnoreCase(it).safe() ||
                        Associations.cityFor(hamlet)?.containsContentIgnoreCase(it).safe() ||
                        Associations.cityFor(village)?.containsContentIgnoreCase(it).safe() ||
                        Associations.countryFor(country)?.containsContentIgnoreCase(it).safe()
            }.safe()
        }
    } ?: find { it.toAddress().full().safe() }