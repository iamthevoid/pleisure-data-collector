package iam.thevoid.pleisure.data.api.seller.concertru

import iam.thevoid.pleisure.data.api.seller.Distributor
import iam.thevoid.pleisure.data.api.seller.concertru.model.EventShort
import iam.thevoid.pleisure.data.db.main.model.*

object ConcertRu : Distributor(ConcertRuClient.endpoint) {

    override suspend fun remoteSuspendSeller(): Seller =
        ConcertRuParser.getSeller(ConcertRuClient.seller())

    override suspend fun remoteSellerUrlsForPlaces(seller: Seller, type: SellerUrlType): List<SellerUrl> {
        return ConcertRuClient.places()
            .let { ConcertRuParser.getSellerUrlsForPlaces(it) }
            .map { idUrl -> SellerUrl.create(seller, type, idUrl) }
    }

    override suspend fun remotePlace(seller: Seller, sellerUrl: SellerUrl): Place {
        return ConcertRuClient.place(sellerUrl.url).let {
            ConcertRuParser.getPlaceFull(sellerUrl, it).apply {
                ConcertRuParser.latLon(ConcertRuClient.mapData(sellerUrl.url))?.also { (lat, lon) ->
                    address?.apply {
                        this.lat = lat
                        this.lon = lon
                    }
                }
            }
        }
    }

    override suspend fun remoteEvents(seller: Seller, place: Place): List<Event> =
        place.sellerUrl.map { sellerUrl ->
            mutableListOf<EventShort>().apply {
                ConcertRuClient.place(sellerUrl.url)
                    .let { ConcertRuParser.getEventsPage(it) }
                    .also { addAll(it.second) }
                    .takeIf { firstPage -> firstPage.first.to > 1 }
                    ?.let { firstPage -> with(firstPage.first) { total / to } + 1 }
                    ?.let { lastPage -> 2..lastPage }
                    ?.map { page ->
                        ConcertRuParser
                            .getEventsPage(ConcertRuClient.place(sellerUrl.url, page))
                            .second.also { eventsRaw -> addAll(eventsRaw) }
                    }
            }.map { short ->
                ConcertRuClient.event(short.id)
                    .let { ConcertRuParser.getEvent(it, sellerUrl, short.image) }
            }
        }.flatten()
}