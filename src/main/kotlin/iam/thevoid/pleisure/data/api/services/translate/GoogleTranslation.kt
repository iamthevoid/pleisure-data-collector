package iam.thevoid.pleisure.data.api.services.translate

class GoogleTranslation : ArrayList<Any>() {

    val output: String
        get() = translation[0] as? String ?: ""

    val source: String
        get() = translation[1] as? String ?: ""


    @Suppress("UNCHECKED_CAST")
    private val translation: List<Any?>
        get() {
            if (isEmpty())
                throw IllegalStateException("Translate response is empty")

            val first = first() as? ArrayList<ArrayList<Any>>
                ?: throw IllegalStateException("Translation response structure changed")
            if (first.isEmpty())
                throw IllegalStateException("Translate body has no elements")

            val translation = first.first()

            if (translation.size < 2)
                throw IllegalStateException("Not enough translate data")

            return translation
        }
}