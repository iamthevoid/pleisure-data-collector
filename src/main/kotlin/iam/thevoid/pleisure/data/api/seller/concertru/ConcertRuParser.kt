package iam.thevoid.pleisure.data.api.seller.concertru

import iam.thevoid.e.*
import iam.thevoid.pleisure.data.api.seller.concertru.model.EventShort
import iam.thevoid.pleisure.data.api.seller.concertru.model.Pagination
import iam.thevoid.pleisure.data.db._manytoone.model.Period
import iam.thevoid.pleisure.data.db.main.model.*
import iam.thevoid.pleisure.utils.*
import iam.thevoid.pleisure.utils.ext.mapNotError
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element


object ConcertRuParser : HtmlParser() {

    val endpoint by lazy { ConcertRuClient.endpoint }
    val pictures by lazy { ConcertRuClient.PICTURES }

    fun latLon(string: String) =
        "\\[\\d+\\.\\d+,\\s*\\d+\\.\\d+\\]".let {coordsPattern ->
            string.split("\n")
                .map { it.remove("\\s") }
                .filter { it.isNotEmpty() }
                .firstOrNull { it.contains(Regex(coordsPattern)) }
                ?.firstOrNull(coordsPattern)
                ?.trim()
                ?.split(",")
                ?.map { it.remove("(\\[|\\])") }
                ?.map { it.safeFloat() }
                ?.let { Pair(it.firstOrNull(), it.getOrNull(1)) }
                ?.takeIf { (lat, lon) -> lat != null && lon != null }
                ?.let {  (lat, lon) -> Pair(lat.safe(), lon.safe()) }
        }

    fun getSellerUrlsForPlaces(html: String): List<String> =
        Jsoup.parse(html).getElementsByClass("platformsList__itemList list-unstyled")
            .map { it.getElementsByTag(LI) }
            .flatten()
            .map { it.getElementsByTag(A) }
            .flatten()
            .mapNotError { extractPlaceUrl(it) }

    fun getPlaceFull(sellerUrl: SellerUrl, html: String): Place =
        Jsoup.parse(html).let { doc ->
            Place.create(
                sellerUrl = listOf(sellerUrl),
                address = Address.createFromRaw(extractPlaceAddressFromEventPage(doc).reportEmpty("place ${sellerUrl.url} address")),
                name = extractPlaceNameFromEventPage(doc).reportEmpty("place ${sellerUrl.url} name"),
                description = extractPlaceAboutFromEventPage(doc).reportEmpty("place ${sellerUrl.url} description")
            )
        }

    fun getEventsPage(html: String) =
        Jsoup.parse(html).let {
            Pair(
                extractPagination(it),
                extractEvents(it)
            )
        }

    // now - tags - "Клубы", "RnB & Hip-Hop & Rap", need to split and make genres and type and add all to tags
    // add fetching genres by band name
    // add fetching artist by even name
    // add fetching place
    fun getEvent(
        html: String,
        sellerUrl: SellerUrl,
        imageSmall: String
    ): Event = Jsoup.parse(html).let { document ->
        extractEventTags(document).let { tags ->

            val type = tags.let { if (it.isNotEmpty()) Type.create(it[0]) else Type.UNKNOWN }
            val genres = extractGenresFromConcertRuTag(tags).orElse(listOf(Genre.UNKNOWN))

            Event.create(
                extractEventName(document),
                extractEventDescription(document),
                extractEventImageUrl(document)
                    .map { image -> Image.create(origin = "$endpoint$pictures/$image", small = "$endpoint$imageSmall") }
                    .orElse(listOf(Image.create(small = "$endpoint$imageSmall"))),
                type,
                null,
                extractPeriods(document).orElse(listOf(Period.unknown)),
                tags = genres.map { tag -> Tag.create(tag) }.toMutableList().apply { add(Tag.create(type.name)) },
                sellerUrl = listOf(sellerUrl),
                genres = genres.map { Genre.create(it) }
            )
        }
    }

    fun getSeller(html: String): Seller =
        Jsoup.parse(html).let { Seller.create(
            endpoint,
            getSellerName(it),
            getSellerLogoUrl(it),
            "/Platform/Platform?ActionPlaceID=${SellerUrl.ID_REPL}&SortType=0",
            "/Event?ActionID=${SellerUrl.ID_REPL}"
        ) }

    private fun getSellerName(document: Document): String =
        ownText(document, "$DIV.footer__title")

    private fun getSellerLogoUrl(document: Document): String =
        attr(document, SRC, "$A.header__logo > $IMG[$SRC]")

    private fun extractEventName(html: Document): String =
        html.ownText("$H2.eventHeader__title")
            .orElse(html.ownText("$H1.eventHeader__title"))
            .orElse(attr(html.select("input[id=action]"), VALUE))

    private fun extractEventDescription(html: Document): String =
        html(html.select("$DIV.eventInfo__body").select(P))

    private fun extractEventImageUrl(html: Document): List<String> =
        html.select("$DIV.eventSlider__item")
            .map { filenameFromString(attr(it, STYLE)) }
            .toSet()
            .toList()
            .filter { it.isNotEmpty() }

    private fun extractPeriods(html: Document): List<Period> = ConcertRuDateParser.parseRawDates(extractEventDates(html))

    private fun extractEventDates(html: Document): List<String> =
        html.select(TR)
            .filter { it.className() == "tr_class?'1':'2'" }
            .map { "${it.ownText("$SPAN.eventTabs__tableDate")} ${it.ownText("$DIV.eventTabs__tableTime")}" }

    private fun extractPlaceNameFromEventPage(html: Document): String =
        html.ownText("$DIV.left > $H2")

    private fun extractPlaceAddressFromEventPage(html: Document): String =
        html.ownText("$DIV.left > $H4")
            .or(html.ownText("$DIV.platformAbout > $P").takeIf { it.length < 200 })
            .safe()

    private fun extractPlaceAboutFromEventPage(html: Document): String =
        html.ownText("$DIV.platformAbout > $P")

    private fun extractEventTags(html: Document): List<String> =
        splitToTags(html.ownText("$H2.eventInfo__title"))

    private fun extractPagination(document: Document): Pagination =
        try {
            document.select("$DIV.pagination__status")
                .map { it.getElementsByTag(SPAN) }
                .flatten()
                .map { it.text().split("-").map { value -> value.toInt() } }
        } catch (e: Exception) {
            listOf(listOf(0, 0), listOf(0))
        }.let { spans ->
            if (spans.size == 2 && spans[0].size == 2)
                Pagination(spans[0][0], spans[0][1], spans[1][0])
            else
                Pagination(0, 0, 0)
        }

    private fun extractPlaceUrl(element: Element) =
        numberFromString(attr(element, HREF))?.toString() ?: ""

    private fun extractEvents(document: Document) =
        document.getElementsByClass("EVENT event_horizontal").mapNotError { EventShort(extractEventId(it), extractEventImage(it)) }

    private fun extractEventId(html: Element) =
        attr(html, HREF).safe().split("=").let { if (it.size == 2) it[1] else "" }

    private fun extractEventImage(html: Element) =
        attr(html, SRC, "$A.event__img > $IMG[$SRC]")

    private fun extractGenresFromConcertRuTag(tags: List<String>) =
        tags.let { if (it.size >= 2) extractGenres(tags[1]) else emptyList() }

    private fun extractGenres(raw: String): List<String> {
        var rawCopy = raw
        val res = mutableListOf<String>()
        for (pair in ampGenres) {
            if (pair.first.find(raw) != null) {
                rawCopy = rawCopy.remove(pair.first)
                res += pair.second
            }
        }

        res.addAll(rawCopy.split("&").filterNot { it.isBlank() || it.isEmpty() }.map { it.trim() })

        return res
    }
}