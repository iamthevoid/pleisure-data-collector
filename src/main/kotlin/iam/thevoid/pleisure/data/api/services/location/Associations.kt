package iam.thevoid.pleisure.data.api.services.location

import iam.thevoid.pleisure.data.db.address.Assoc
import iam.thevoid.pleisure.data.db.address.Entity
import iam.thevoid.pleisure.data.db.address.LocationStorage
import iam.thevoid.pleisure.utils.ext.containsContentIgnoreCase
import iam.thevoid.pleisure.utils.ext.find

object Associations {

    fun countryFor(raw: String?) =
        assoc(raw, { LocationStorage.country.countries() }, { LocationStorage.country.associations(it) })

    fun cityFor(raw: String?): String? =
        assoc(raw, { LocationStorage.city.cities() }, { LocationStorage.city.associations(it) })

    fun cityFor(raw: String?, vararg another: String?): String? =
        cityFor(raw) ?: another.firstOrNull { cityFor(it) != null }?.let { cityFor(it) }

    fun codeFor(raw: String?) =
        assoc(raw, { LocationStorage.code.countryCodes() }, { LocationStorage.code.associations(it) })

    private fun <E : Entity, A : Assoc> assoc(value: String?, factory: () -> List<E>, mapper: (E) -> List<A>): String? {
        value ?: return null
        factory().forEach { entity ->
            val associations = mapper(entity)
            if (entity.name == value ||
                associations.map { it.value }.containsContentIgnoreCase(value) ||
                entity.anotherAssociation(value)
            ) return entity.name
        }
        return null
    }

    private val addressPartAssoc = mapOf(
        "улица" to listOf("ул"),
        "дом" to listOf("д"),
        "город" to listOf("г"),
        "бульвар" to listOf("б-р", "бул"),
        "набережная" to listOf("наб"),
        "строение" to listOf("стр"),
        "площадь" to listOf("пл"),
        "шоссе" to listOf("ш"),
        "имени" to listOf("им"),
        "переулок" to listOf("пер"),
        "владение" to listOf("вл"),
        "проспект" to listOf("пр", "просп", "пр-т", "пр-кт")
    )

    fun prepare(addressQuery: String) =
        addressQuery.split(Regex("[^\\p{L}\\p{N}\\p{Pd}]+"))
            .filter { it.isNotBlank() }
            .joinToString(separator = " ") { part ->
                addressPartAssoc.find { (_, value) -> value.containsContentIgnoreCase(part) }
                    ?.first ?: part
            }
}