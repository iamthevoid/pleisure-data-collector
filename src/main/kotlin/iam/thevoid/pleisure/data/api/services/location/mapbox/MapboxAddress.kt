package iam.thevoid.pleisure.data.api.services.location.mapbox

import com.google.gson.annotations.SerializedName
import iam.thevoid.e.or
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.api.services.location.Associations
import iam.thevoid.pleisure.data.db.main.model.Address
import iam.thevoid.pleisure.data.db.main.model.full
import iam.thevoid.pleisure.utils.ext.containsContentIgnoreCase

data class MapboxAddress(
    @SerializedName("relevance")
    val relevance: Float = 0f,
    @SerializedName("text")
    val text: String = "",
    @SerializedName("place_name")
    val placeName: String = "",
    @SerializedName("center")
    val center: List<Float> = listOf(),
    @SerializedName("geometry")
    val geometry: MapboxGeometry = MapboxGeometry(),
    @SerializedName("context")
    val context: List<Map<String, String>> = listOf()
) {


    internal fun get(what: String) = context.find { it["id"]?.startsWith(what).safe() }

    internal fun city() = get("place")?.get("text") ?: region()

    internal fun region() = get("region")?.get("text")

    internal fun country() = get("country")?.get("text")

    internal fun code() = get("country")?.get("short_code")

    internal fun lat() = center.or(geometry.coordinates)?.getOrNull(1).safe()

    internal fun lon() = center.or(geometry.coordinates)?.getOrNull(0).safe()

}

fun MapboxAddress.toAddress() = Address.create(
    Associations.cityFor(city()),
    Associations.countryFor(country()),
    Associations.codeFor(code()),
    text,
    lat(),
    lon()
).takeIf { it.full() }

fun List<MapboxAddress>.findWith(additional: String?) =
    additional?.let {
        find { address ->
            address.run {
                Associations.cityFor(city())?.containsContentIgnoreCase(it).safe() &&
                        Associations.cityFor(region())?.containsContentIgnoreCase(it).safe() ||
                        Associations.countryFor(country())?.containsContentIgnoreCase(it).safe() ||
                        text.containsContentIgnoreCase(it)
            }.safe()
        }
    } ?: find { it.toAddress().full().safe() }