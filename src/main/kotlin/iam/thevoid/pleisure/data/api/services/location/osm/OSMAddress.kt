package iam.thevoid.pleisure.data.api.services.location.osm

import com.google.gson.annotations.SerializedName
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.api.services.location.Associations
import iam.thevoid.pleisure.data.db.main.model.Address
import iam.thevoid.pleisure.data.db.main.model.full
import iam.thevoid.pleisure.utils.ext.containsContentIgnoreCase

/*
[
  {
    "place_id": 84261500,
    "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
    "osm_type": "way",
    "osm_id": 33195195,
    "boundingbox": [
      "59.9271697",
      "59.9280084",
      "30.3297556",
      "30.3314874"
    ],
    "lat": "59.92754785",
    "lon": "30.3306694849865",
    "display_name": "Большой драматический театр имени Г. А. Товстоногова, 65, набережная реки Фонтанки, Апраксин двор, округ № 78, Центральный район, Санкт-Петербург, Северо-Западный федеральный округ, 191023, РФ",
    "class": "amenity",
    "type": "theatre",
    "importance": 1.0935975425568891,
    "icon": "https://nominatim.openstreetmap.org/images/mapicons/tourist_theatre.p.20.png",
    "address": {
      "theatre": "Большой драматический театр имени Г. А. Товстоногова",
      "house_number": "65",
      "road": "набережная реки Фонтанки",
      "suburb": "Апраксин двор",
      "city": "округ № 78",
      "state_district": "Центральный район",
      "state": "Санкт-Петербург",
      "postcode": "191023",
      "country": "РФ",
      "country_code": "ru"
    }
  }
]
 */

data class OSMAddress(
    @SerializedName("lat")
    val lat: Float,
    @SerializedName("lon")
    val lon: Float,
    @SerializedName("display_name")
    val raw: String,
    @SerializedName("address")
    val address: Detail?
) {

    data class Detail(
        @SerializedName("city")
        val city: String?,
        @SerializedName("state")
        val state: String?,
        @SerializedName("country")
        val country: String?,
        @SerializedName("county")
        val county: String?,
        @SerializedName("suburb")
        val suburb: String?,
        @SerializedName("hamlet")
        val hamlet: String?,
        @SerializedName("town")
        val town: String?,
        @SerializedName("village")
        val village: String?,
        @SerializedName("country_code")
        val countryCode: String?
    )

    fun toAddress() = Address.create(
        address?.run { Associations.cityFor(state, city, town, suburb, hamlet, village, county) },
        Associations.countryFor(address?.country),
        Associations.codeFor(address?.countryCode),
        raw,
        lat,
        lon
    ).takeIf { it.full() }
}

fun List<OSMAddress>.findWith(additional: String?) =
    additional?.let {
        find { address ->
            address.address?.run {
                Associations.cityFor(city).safe().containsContentIgnoreCase(it) ||
                        Associations.cityFor(town).safe().containsContentIgnoreCase(it) ||
                        Associations.cityFor(state).safe().containsContentIgnoreCase(it) ||
                        Associations.cityFor(suburb).safe().containsContentIgnoreCase(it) ||
                        Associations.cityFor(hamlet).safe().containsContentIgnoreCase(it) ||
                        Associations.cityFor(village).safe().containsContentIgnoreCase(it) ||
                        Associations.countryFor(country).safe().containsContentIgnoreCase(it) ||
                        county.safe().containsContentIgnoreCase(it)
            }.safe()
        }
    } ?: find { it.toAddress().full().safe() }
