package iam.thevoid.pleisure.data.api.enumerations

/*
{'Мюзикл', 'Спорт', 'Концерт', 'Детям', 'Цирк', 'Театры', 'Клубы', 'Экскурсии', 'Шоу', 'Кино', 'Разное'}
 */

@Suppress("EnumEntryName")
enum class EventType(val tags: List<String>) {

    /** --------------------------  CCC ----------------------- */

    children(
        listOf(
            "children",
            "дети",
            "детям"
        )
    ),

    cinema(
        listOf(
            "cinema",
            "кино"
        )
    ),

    circus(
        listOf(
            "circus",
            "цирк"
        )
    ),

    club(
        listOf(
            "club",
            "клуб"
        )
    ),

    concert(
        listOf(
            "Concert",
            "концерт"
        )
    ),

    /** --------------------------  MMM ----------------------- */

    musical(
        listOf(
            "Мюзикл",
            "Музикл",
            "musical"
        )
    ),


    /** --------------------------  OOO ----------------------- */

    other(
        listOf(
            "Другое",
            "other"
        )
    ),

    /** --------------------------  SSS ----------------------- */

    show(
        listOf(
            "show",
            "шоу"
        )
    ),

    sport(
        listOf(
            "Sport",
            "Спорт"
        )
    ),

    /** --------------------------  TTT ----------------------- */

    theater(
        listOf(
            "theater",
            "театр"
        )
    ),

    tour(
        listOf(
            "tour",
            "экскурсия",
            "экскурсии"
        )
    ),


    /** --------------------------  UUU ----------------------- */

    undefined(
        listOf()
    ),
    ;

    companion object {

        fun define(tag: String): List<EventType> {
            val lowerCaseTag = tag.toLowerCase()
            val types = mutableListOf<EventType>()
            for (type in values()) {
                if (type.tags.any { lowerCaseTag.contains(it, true) })
                    types.add(type)
            }

            return if (types.isEmpty()) listOf(undefined) else types
        }
    }


}