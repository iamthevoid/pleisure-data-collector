package iam.thevoid.pleisure.data.api.services.location.here

import com.google.gson.annotations.SerializedName
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.api.services.location.Associations
import iam.thevoid.pleisure.data.api.services.location.here.HereAddress.Location.Address.Companion.COUNTRY_NAME
import iam.thevoid.pleisure.data.db.main.model.Address
import iam.thevoid.pleisure.data.db.main.model.full
import iam.thevoid.pleisure.utils.ext.containsContentIgnoreCase

data class HereAddress(
    @SerializedName("Relevance")
    var relevance: Float? = null,
    @SerializedName("MatchLevel")
    var matchLevel: String? = null,
    @SerializedName("MatchQuality")
    var matchQuality: MatchQuality? = null,
    @SerializedName("MatchType")
    var matchType: String? = null,
    @SerializedName("Location")
    var location: Location? = null
) {

    data class MatchQuality(
        @SerializedName("Street")
        var street: List<Float>? = null,
        @SerializedName("HouseNumber")
        var houseNumber: Float? = null
    )

    data class Location(
        @SerializedName("LocationId")
        var locationId: String? = null,
        @SerializedName("LocationType")
        var locationType: String? = null,
        @SerializedName("DisplayPosition")
        var displayPosition: Point? = null,
        @SerializedName("NavigationPosition")
        var navigationPosition: List<Point>? = null,
        @SerializedName("MapView")
        var mapView: MapView? = null,
        @SerializedName("Address")
        var address: Address? = null

    ) {

        data class Point(
            @SerializedName("Latitude")
            var lat: Float? = null,
            @SerializedName("Longitude")
            var lon: Float? = null
        )

        data class MapView(
            @SerializedName("TopLeft")
            var topLeft: Point? = null,
            @SerializedName("bottomRight")
            var bottomRight: Point? = null
        )

        data class Address(
            @SerializedName("Label")
            var label: String? = null,
            @SerializedName("Country")
            var country: String? = null,
            @SerializedName("State")
            var state: String? = null,
            @SerializedName("County")
            var county: String? = null,
            @SerializedName("City")
            var city: String? = null,
            @SerializedName("District")
            var district: String? = null,
            @SerializedName("Street")
            var street: String? = null,
            @SerializedName("HouseNumber")
            var houseNumber: String? = null,
            @SerializedName("PostalCode")
            var postalCode: String? = null,
            @SerializedName("AdditionalData")
            var additional: List<AdditionalBlock>? = null
        ) {

            companion object {
                const val COUNTRY_NAME = "CountryName"
                const val COUNTY_NAME = "CountyName"
                const val STATE_NAME = "StateName"
            }

            data class AdditionalBlock(
                @SerializedName("key")
                var key: String? = null,
                @SerializedName("value")
                var value: String? = null
            )
        }
    }
}

fun HereAddress.toAddress(raw: String = location?.address?.label.safe()) = Address.create(
    location?.address?.run { Associations.cityFor(city, state, district) },
    Associations.countryFor(location?.address?.additional?.find { it.key == COUNTRY_NAME }?.value),
    Associations.codeFor(location?.address?.country),
    raw,
    location?.displayPosition?.lat,
    location?.displayPosition?.lon
).takeIf { it.full() }

fun List<HereAddress>.findWith(additional: String?) =
    additional?.let {
        find { address ->
            address.location?.address?.run {
                Associations.cityFor(city).safe().containsContentIgnoreCase(it) ||
                        Associations.cityFor(state).safe().containsContentIgnoreCase(it) ||
                        Associations.countryFor(country).safe().containsContentIgnoreCase(it) ||
                        county.safe().containsContentIgnoreCase(it) ||
                        district.safe().containsContentIgnoreCase(it) ||
                        street.safe().containsContentIgnoreCase(it)
            }.safe()
        }
    } ?: find { it.toAddress().full().safe() }