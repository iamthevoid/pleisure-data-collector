package iam.thevoid.pleisure.data.api.services.location.mapbox

import io.ktor.client.HttpClient
import io.ktor.client.features.HttpClientFeature
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.JsonSerializer
import io.ktor.client.features.json.defaultSerializer
import io.ktor.client.request.HttpRequestPipeline
import io.ktor.client.request.accept
import io.ktor.client.response.HttpResponseContainer
import io.ktor.client.response.HttpResponsePipeline
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.contentType
import io.ktor.util.AttributeKey
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.io.ByteReadChannel
import kotlinx.coroutines.io.readRemaining

class MapboxFeature internal constructor(
    val serializer: JsonSerializer,
    @KtorExperimentalAPI val acceptContentTypes: List<ContentType>
) {
    @Deprecated("Install feature properly instead of direct instantiation.", level = DeprecationLevel.ERROR)
    constructor(serializer: JsonSerializer) : this(
        serializer,
        listOf(ContentType.Application.Json)
    )

    /**
     * [JsonFeature] configuration that is used during installation
     */
    class Config {
        /**
         * Serializer that will be used for serializing requests and deserializing response bodies.
         *
         * Default value for [serializer] is [defaultSerializer].
         */
        var serializer: JsonSerializer? = null

        /**
         * List of content types that are handled by this feature.
         * It also affects `Accept` request header value.
         * Please note that wildcard content types are supported but no quality specification provided.
         */
        @KtorExperimentalAPI
        var acceptContentTypes: List<ContentType> = listOf(ContentType.Application.Json)
            set(newList) {
                require(newList.isNotEmpty()) { "At least one content type should be provided to acceptContentTypes" }
                field = newList
            }
    }

    /**
     * Companion object for feature installation
     */
    companion object Feature : HttpClientFeature<Config, MapboxFeature> {
        override val key: AttributeKey<MapboxFeature> = AttributeKey("Json")

        override fun prepare(block: Config.() -> Unit): MapboxFeature {
            val config = Config().apply(block)
            val serializer = config.serializer ?: defaultSerializer()
            val allowedContentTypes = config.acceptContentTypes.toList()

            return MapboxFeature(serializer, allowedContentTypes)
        }

        override fun install(feature: MapboxFeature, scope: HttpClient) {
            scope.requestPipeline.intercept(HttpRequestPipeline.Transform) { payload ->
                feature.acceptContentTypes.forEach { context.accept(it) }

                val contentType = ContentType.Application.Json

                context.headers.remove(HttpHeaders.ContentType)

                val serializedContent = feature.serializer.write(payload, contentType)

                proceedWith(serializedContent)
            }

            scope.responsePipeline.intercept(HttpResponsePipeline.Transform) { (info, body) ->
                if (body !is ByteReadChannel) return@intercept

                if (feature.acceptContentTypes.none { context.response.contentType()?.match(it) == true })
                    return@intercept
                try {
                    proceedWith(HttpResponseContainer(info, feature.serializer.read(info, body.readRemaining())))
                } finally {
                    context.close()
                }
            }
        }
    }
}
