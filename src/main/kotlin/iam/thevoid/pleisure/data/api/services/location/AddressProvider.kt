package iam.thevoid.pleisure.data.api.services.location

import iam.thevoid.pleisure.data.db.main.model.Address

interface AddressProvider {
    suspend fun forward(query : String, additional : String? = null) : Address?
    suspend fun reverse(lat: Float, lon: Float) : Address?
}