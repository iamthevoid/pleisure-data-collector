package iam.thevoid.pleisure.data.api.seller.redkassa

import iam.thevoid.pleisure.data.api.Client
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.request.parameter

object RedKassaClient : Client() {

    override val endpoint = "https://redkassa.ru/"

    override val logLevel: LogLevel
        get() = LogLevel.INFO

    suspend fun seller() =
        retryWithDelay { get<String>(endpoint) }

    suspend fun places() =
        retryWithDelay { get<String>("$endpoint/venues") }

    suspend fun event(id: String) =
        retryWithDelay { get<String>("$endpoint/events/$id") }

    suspend fun address(name: String) =
        retryWithDelay { get<String>("$endpoint/venue/$name#address") }

    suspend fun place(name: String) =
        retryWithDelay { get<String>("$endpoint/venue/$name") }

    suspend fun eventsPerMonth(venueId: String, month: String) =
        retryWithDelay {
            get<String>("$endpoint/Venue/$venueId") {
                parameter("month", month)
            }
        }

}