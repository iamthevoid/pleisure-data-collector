@file:Suppress("MemberVisibilityCanBePrivate", "SpellCheckingInspection")

package iam.thevoid.pleisure.data.api.seller.redkassa

import iam.thevoid.e.remove
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.api.seller.redkassa.model.RedKassaEventShort
import iam.thevoid.pleisure.data.api.seller.redkassa.model.VenueInfo
import iam.thevoid.pleisure.data.db.main.model.*
import iam.thevoid.pleisure.utils.HtmlParser
import iam.thevoid.pleisure.utils.ext.fromJson
import iam.thevoid.pleisure.utils.ext.hasQueryParameter
import iam.thevoid.pleisure.utils.ext.queryParameter
import iam.thevoid.pleisure.utils.ext.removeDuplicates
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.net.URI

object RedKassaParser : HtmlParser() {

    private val endpoint by lazy { RedKassaClient.endpoint }

    /**
     * SELLER
     */

    fun getSeller(html: String): Seller =
        with(Jsoup.parse(html)) {
            Seller.create(
                endpoint,
                getSellerName(),
                getSellerLogoUrl(),
                "venue/${SellerUrl.ID_REPL}",
                "events/${SellerUrl.ID_REPL}"
            )
        }

    private fun Document.getSellerName() =
        attr(select("$META[name=author]"), "content")

    private fun Document.getSellerLogoUrl() =
        attr(select("$IMG.logo__img"), "src")

    /**
     * Places URLs
     */

    fun getSellerUrlForPlaces(html: String): List<String> =
        with(Jsoup.parse(html)) { getSellerUrls() }

    private fun Document.getSellerUrls(): List<String> =
        select("$A.venues-place__item").map { attr(it, HREF).remove("/venue/") }

    /**
     * Place
     */

    fun getPlaceFull(html: String) =
        with(Jsoup.parse(html)) {
            Place.create(
                getPlaceName(),
                address = Address.create(raw = getRawAddress())
            )
        }

    private fun Document.getPlaceName() =
        ownText("$H1.theatre-header__title")

    private fun Document.getRawAddress() =
        ownText("$P.theatre-header__address")

    /**
     * Venue
     */

    fun getVenueInfo(html: String) =
        with(Jsoup.parse(html)) { getVenueInfoRaw().fromJson<VenueInfo>() }

    private fun Document.getVenueInfoRaw() =
        ownText("$DIV.dn[id=venue-page-options]")

    /**
     * Months
     */

    fun getMonths(html: String) =
        with(Jsoup.parse(html)) { getRawMonths() }.mapNotNull { raw ->
            URI.create(raw).takeIf { it.hasQueryParameter("month") }?.queryParameter("month")
        }

    private fun Document.getRawMonths() =
        select("$A.dates-slider__month").map { attr(it, HREF) }

    /**
     * Event (short)
     */

    fun getEventsShort(html: String) =
        Jsoup.parse(html).select("$DIV.afisha__row")
            .map {
                RedKassaEventShort(
                    it.getEventShortNameRaw(),
                    it.getEventShortTimeRaw(),
                    it.getEventShortDateRaw(),
                    it.getEventShortTagRaw(),
                    it.getEventShortIdRaw(),
                    it.getEventShortImageRaw()
                )
            }.removeDuplicates()

    private fun Element.getEventShortIdRaw() =
        select("$A.afisha__event-image").first()
            .let { attr(it, HREF).replace("/events/", "") }.split("/").first()

    private fun Element.getEventShortImageRaw(): String? =
        select("$A.afisha__event-image").select(IMG).firstOrNull()?.let { attr(it, SRC) }

    private fun Element.getEventShortNameRaw() =
        ownText("$A.afisha__event-link")

    private fun Element.getEventShortTagRaw(): List<String> =
        select("$SPAN.afisha__event-type").ownText().filter { it.isNotBlank() }.removeDuplicates()

    private fun Element.getEventShortDateRaw() =
        ownText("$SPAN.afisha__event-dates")

    private fun Element.getEventShortTimeRaw() =
        ownText("$DIV.afisha__time")

    /**
     * Event (Full)
     */

    /*
               Event.create(
                extractEventName(document),
                extractEventImageUrl(document)
                    .map { image -> Image.create(origin = "$endpoint$pictures/$image", small = "$endpoint$imageSmall") }
                    .orElse(listOf(Image.create(small = "$endpoint$imageSmall"))),
                type,
                null,
                extractPeriods(document).orElse(listOf(Period.unknown)),
                tags = genres.map { tag -> Tag.create(tag) }.toMutableList().apply { add(Tag.create(type.name)) },
                sellerUrl = listOf(sellerUrl),
                genres = genres.map { Genre.create(it) }
            )
     */

    fun event(html: String, eventShort: RedKassaEventShort, sellerUrl: SellerUrl): Event =
        Jsoup.parse(html).let { it ->
            val genresRaw = it.getEventGenre()
            val typeRaw = it.getEventType()
            Event.create(
                name = eventShort.name ?: getEventName(it),
                image = listOf(
                    Image.create(
                        small = eventShort.imageUrl,
                        logo = it.getEventLogoImage(),
                        origin = it.getEventImage()
                    )
                ),
                type = typeRaw?.let(Type.Companion::create) ?: Type.UNKNOWN,
                periods = RedKassaDateParser.periods(eventShort.time, eventShort.date),
                tags = eventShort.tag.toMutableList().apply { addAll(genresRaw); typeRaw?.also { type -> add(type) } }
                    .removeDuplicates().map { name -> Tag.create(name) },
                sellerUrl = listOf(sellerUrl),
                genres = genresRaw.takeIf { genres -> genres.isNotEmpty() }
                    ?.map { name -> Genre.create(name) } ?: listOf(Genre.create())
            )
        }

    private fun Document.getEventImage() =
        select("picture > $SOURCE").attr("srcset").takeIf { it.isNotBlank() }
            ?.split(", ")?.firstOrNull()

    private fun Document.getEventLogoImage() =
        select("picture > $IMG").attr(SRC).takeIf { it.isNotBlank() }

    private fun Document.getEventType() =
        select("$LI > $A.breadcrumbs__link").getOrNull(1).ownText().takeIf { it.isNotBlank() }

    private fun Document.getEventGenre() =
        select("$LI > $A.breadcrumbs__link").getOrNull(2).ownText().takeIf { it.isNotBlank() }
            ?.split(",")?.map { it.trim() }.safe()

    private fun getEventName(document: Document): String =
        ownText(document, "$H1.h1 mb5")
}