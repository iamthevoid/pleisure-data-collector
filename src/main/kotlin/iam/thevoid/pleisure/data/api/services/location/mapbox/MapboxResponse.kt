package iam.thevoid.pleisure.data.api.services.location.mapbox

import com.google.gson.annotations.SerializedName

data class MapboxResponse(
    @SerializedName("type")
    val type: String = "",
    @SerializedName("query")
    val query: List<String> = listOf(),
    @SerializedName("features")
    val features: List<MapboxAddress> = emptyList(),
    @SerializedName("attribution")
    val attribution: String = ""
)

