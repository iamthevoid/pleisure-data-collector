package iam.thevoid.pleisure.data.api

import iam.thevoid.pleisure.utils.RetryWithDelay
import io.ktor.client.HttpClient
import io.ktor.client.features.json.GsonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.logging.DEFAULT
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import javax.net.ssl.SSLException

abstract class Client {

    companion object {
        private const val RETRY_TIMES = 20
        private const val CONNECT_TIMEOUT = 30
        private const val READ_TIMEOUT = 20
    }

    open val logLevel
        get() = LogLevel.ALL

    abstract val endpoint: String

    protected val ktorClient
        get() = HttpClient {
            install(Logging) {
                logger = Logger.DEFAULT
                level = logLevel
            }
            install(JsonFeature) {
                serializer = GsonSerializer()
            }
        }

    protected suspend inline fun <reified T> get(url: String, block : HttpRequestBuilder.() -> Unit = {}) =
        ktorClient.use { it.get<T>(url, block) }


    suspend fun <T> retryWithDelay(delayInterval: Long = 1000L, function: (suspend () -> T)): T =
        RetryWithDelay<T>().apply {
            times = RETRY_TIMES
            timeoutMillis = delayInterval
            exceptionFilter = { it is SSLException && it.message == "Socket closed"}
        }.execute(function)

}