package iam.thevoid.pleisure.data.api.seller.redkassa.model

data class RedKassaEventShort(
    val name: String,
    val time: String,
    val date: String,
    val tag: List<String>,
    val id: String,
    val imageUrl: String?
)