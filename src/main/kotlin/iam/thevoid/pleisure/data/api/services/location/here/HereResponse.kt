package iam.thevoid.pleisure.data.api.services.location.here

import com.google.gson.annotations.SerializedName


data class HereResponse(
    @SerializedName("Response")
    var response: Response? = null
) {


    data class Response(
        @SerializedName("MetaInfo")
        var meta: MetaInfo? = null,
        @SerializedName("View")
        var view: List<View>? = null

    ) {

        data class MetaInfo(
            @SerializedName("Timestamp")
            var timestamp: String? = null,
            @SerializedName("NextPageInformation")
            var nextPageInfo: String? = null
        )

        data class View(
            @SerializedName("_type")
            var type: String? = null,
            @SerializedName("ViewId")
            var viewId: Long? = null,
            @SerializedName("Result")
            var result: List<HereAddress>? = null
        )
    }
}