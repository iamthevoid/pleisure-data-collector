package iam.thevoid.pleisure.data.api.seller

import iam.thevoid.e.firstOrNull
import iam.thevoid.e.remove
import iam.thevoid.pleisure.data.api.services.location.LocationHelper
import iam.thevoid.pleisure.data.db.main.model.*

abstract class Distributor(val endpoint: String) {

    protected abstract suspend fun remoteSuspendSeller(): Seller

    protected abstract suspend fun remotePlace(seller: Seller, sellerUrl: SellerUrl): Place

    protected abstract suspend fun remoteSellerUrlsForPlaces(seller: Seller, type: SellerUrlType): List<SellerUrl>

    protected abstract suspend fun remoteEvents(seller: Seller, place: Place): List<Event>

    suspend fun seller(): Seller = remoteSuspendSeller()

    suspend fun sellerUrlsForPlaces(seller: Seller, type: SellerUrlType): List<SellerUrl> =
        remoteSellerUrlsForPlaces(seller, type)

    suspend fun place(seller: Seller, sellerUrl: SellerUrl): Place? =
        remotePlace(seller, sellerUrl)
            .also { place ->
                // remove brace containment from place name and extract it
                val additional = place.name.firstOrNull("\\(.+\\)")?.let { additionalRaw ->
                    place.name = place.name.replace(additionalRaw, "").trim()
                    additionalRaw.remove("\\(|\\)|[Гг]\\.").trim()
                }
                place.address = LocationHelper.address(place, additional)
            }.takeIf { it.address != null }

    suspend fun events(seller: Seller, place: Place): List<Event> = remoteEvents(seller, place)
        .also { events ->
            events.forEach {
                it.place = place
            }
        }
}