package iam.thevoid.pleisure.data.api.services.location.mapbox

import com.google.gson.annotations.SerializedName

data class MapboxGeometry(
    @SerializedName("type")
    val type: String = "",
    @SerializedName("coordinates")
    val coordinates: List<Float> = listOf()
)