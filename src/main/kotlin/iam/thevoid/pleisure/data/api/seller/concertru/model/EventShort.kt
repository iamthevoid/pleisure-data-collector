package iam.thevoid.pleisure.data.api.seller.concertru.model

data class EventShort(
    val id : String,
    val image : String
)