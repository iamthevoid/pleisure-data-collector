package iam.thevoid.pleisure.data.file.metro

import com.google.gson.annotations.SerializedName

data class MetroStation (
    @SerializedName("Station")
    val station : String,
    @SerializedName("Line")
    val line : String,
    @SerializedName("AdmArea")
    val admArea : String,
    @SerializedName("District")
    val district : String
)