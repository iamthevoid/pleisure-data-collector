package iam.thevoid.pleisure.data.file

import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader
import java.io.FileWriter

object FileHandler {

    fun contents(file : File) : String {
        if (!file.exists())
            throw FileNotFoundException()

        return FileReader(file).use { it.readText() }
    }

    fun contentLines(file : File) : List<String> {
        if (!file.exists())
            throw FileNotFoundException()

        return FileReader(file).use { it.readLines() }
    }

    fun res(filename : String, sourceSet : String = "main") : File {
        if (filename.isBlank())
            throw FileNotFoundException()

        val file = File("src/$sourceSet/resources/$filename")
        if (!file.exists())
            throw FileNotFoundException()

        return file
    }

     fun writeToFile(filename: String, any: Any?) {
        val file = File(filename)
         createFile(filename)
        val fr = FileWriter(file, true)
        fr.write("$any\n")
        fr.close()
    }

     fun clearFile(filenames : List<String>) =
         filenames.forEach { clearFile(it) }

     fun clearFile(filename: String) {
        val file = File(filename)
         createFile(filename)
        val fr = FileWriter(file, false)
        fr.write("")
        fr.close()
    }

     fun createFile(filename: String) {
        val file = File(filename)

        file.parentFile.mkdirs()

        if (!file.exists()) {
            file.createNewFile()
        }
    }
}