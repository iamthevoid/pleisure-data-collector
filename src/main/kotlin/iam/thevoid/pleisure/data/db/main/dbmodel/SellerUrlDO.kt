package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.main.model.SellerUrl
import iam.thevoid.pleisure.data.db.main.table.SellerUrls
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class SellerUrlDO(id: EntityID<Int>) : IntEntity(id),
    DataModel<SellerUrl> {

    companion object : IntEntityClass<SellerUrlDO>(SellerUrls)

    var url by SellerUrls.url
    var seller by SellerDO referencedOn SellerUrls.seller
    var type by SellerUrlTypeDO referencedOn SellerUrls.type

    override fun extract() = SellerUrl.create(
        seller.extract(),
        type.extract(),
        url
    )
}