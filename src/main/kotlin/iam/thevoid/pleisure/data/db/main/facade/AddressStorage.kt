package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.AddressDO
import iam.thevoid.pleisure.data.db.main.model.Address

abstract class AddressStorage : Storage<Address, AddressDO>()