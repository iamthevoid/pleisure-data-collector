package iam.thevoid.pleisure.data.db.main.model

class Artist private constructor(
    var name: String,
    var description : String?,
    var image: Image?,
    var genre: List<Genre>,
    var people: List<People>
) {
    companion object {
        fun create(
            name: String,
            description: String? = null,
            image: Image? = null,
            genre: List<Genre> = emptyList(),
            people: List<People> = emptyList()
        ) = Artist(name, description, image, genre, people)
    }
}

//fun Artist.clone() = Artist.create(name, genre.map { it.clone() }, people.map { it.clone() })