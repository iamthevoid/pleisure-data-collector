package iam.thevoid.pleisure.data.db.util

import org.jetbrains.exposed.sql.Op

fun Op.Companion.fromBoolean(boolean: Boolean) =
    if (boolean) Op.TRUE else Op.FALSE