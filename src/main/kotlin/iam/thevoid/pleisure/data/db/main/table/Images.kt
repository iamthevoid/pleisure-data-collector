package iam.thevoid.pleisure.data.db.main.table

import org.jetbrains.exposed.dao.IntIdTable

object Images  : IntIdTable("images") {
    val small = text("small").nullable()
    val medium = text("medium").nullable()
    val big = text("big").nullable()
    val origin = text("origin").nullable()
    val hash = text("hash")
}