package iam.thevoid.pleisure.data.db._manytomany.impl

import iam.thevoid.pleisure.data.db._manytomany.entity.PlaceSellerUrlDO
import iam.thevoid.pleisure.data.db._manytomany.facade.Place_SellerUrlBinding
import iam.thevoid.pleisure.data.db._manytomany.table.PlaceSellerUrl
import iam.thevoid.pleisure.data.db.main.dbmodel.PlaceDO
import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlDO
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

internal object Place_SellerUrlBindingImpl : Place_SellerUrlBinding() {

    override val TABLES: List<Table>
        get() = listOf(PlaceSellerUrl)

    override val PlaceSellerUrl.identity1: Column<EntityID<Int>>
        get() = placeId

    override val PlaceSellerUrl.identity2: Column<EntityID<Int>>
        get() = sellerUrlId

    override fun createInDb(bind1: PlaceDO, bind2: SellerUrlDO): PlaceSellerUrlDO =
        PlaceSellerUrlDO.new {
            placeId = bind1
            sellerUrlId = bind2
        }
}