package iam.thevoid.pleisure.data.db.main.model

import iam.thevoid.pleisure.data.db._manytoone.model.Period

class Event private constructor(
    var name: String,
    var description: String?,
    var image: List<Image>,
    var period: List<Period>,
    var place: Place,
    var tags: List<Tag>,
    var type: Type,
    var sellerUrl: List<SellerUrl>,
    var artist: Artist?,
    var genres: List<Genre>
) {

    init { period.forEach { it.event = this } }

    companion object {
        fun create(
            name: String = "",
            description: String? = null,
            image: List<Image> = emptyList(),
            type: Type = Type.UNKNOWN,
            artist: Artist? = null,
            periods: List<Period> = emptyList(),
            place: Place = Place.create(),
            tags: List<Tag> = emptyList(),
            sellerUrl: List<SellerUrl> = emptyList(),
            genres: List<Genre> = emptyList()
        ) = Event(
            name,
            description,
            image,
            periods,
            place,
            tags,
            type,
            sellerUrl,
            artist,
            genres
        )
    }

    override fun toString(): String {
        return "$name. ${period.firstOrNull()?.toString()} type=$type, genre=${genres.joinToString()}"
    }
}