package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db.main.DataStorage
import iam.thevoid.pleisure.data.db.main.dbmodel.PeopleDO
import iam.thevoid.pleisure.data.db.main.facade.PeopleStorage
import iam.thevoid.pleisure.data.db.main.model.People
import iam.thevoid.pleisure.data.db.main.table.Peoples
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select

internal object PeopleStorageImpl : PeopleStorage() {

    override val TABLES: List<Table>
        get() = listOf(Peoples)

    override fun Transaction.getFromDb(item: People): PeopleDO? =
        PeopleDO.wrapRows(Peoples.select {
            Peoples.name eq item.name and
                    (Peoples.birthdate eq item.birthdate)
        }).firstOrNull()

    override fun Transaction.createEntityInDb(item: People) =
        PeopleDO.new {
            name = item.name
            description = item.description
            birthdate = item.birthdate
            role = item.role
            image = item.image?.let { DataStorage.images.createIfNotExistsInDb(it) }
        }
}