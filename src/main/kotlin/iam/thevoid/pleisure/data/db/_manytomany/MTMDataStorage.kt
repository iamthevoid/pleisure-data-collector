package iam.thevoid.pleisure.data.db._manytomany

import iam.thevoid.pleisure.data.db._manytomany.facade.*
import iam.thevoid.pleisure.data.db._manytomany.impl.*

object MTMDataStorage {
    val artist_genre : Artist_GenreBinding by lazy { Artist_GenreBindingImpl }
    val artist_people : Artist_PeopleBinding by lazy { Artist_PeopleBindingImpl }
    val event_genre : Event_GenreBinding by lazy { Event_GenreBindingImpl }
    val event_image : Event_ImageBinding by lazy { Event_ImageBindingImpl }
    val event_sellerUrl : Event_SellerUrlBinding by lazy { Event_SellerUrlBindingImpl }
    val event_tag : Event_TagBinding by lazy { Event_TagBindingImpl }
    val place_sellerUrl : Place_SellerUrlBinding by lazy { Place_SellerUrlBindingImpl }
}