package iam.thevoid.pleisure.data.db._manytomany.facade

import iam.thevoid.pleisure.data.db.DB
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.sql.*

abstract class MTMStorage<BS1 : IntEntity, BS2 : IntEntity, E : IntEntity, T : Table>(
    private val table: T,
    private val cls: IntEntityClass<E>
) : DB() {

    abstract val T.identity1: Column<EntityID<Int>>

    abstract val T.identity2: Column<EntityID<Int>>

    internal abstract fun createInDb(bind1: BS1, bind2: BS2): E

    internal fun getBy1(bind: BS1): List<E> =
        loggedTransaction { cls.wrapRows(table.select { bound1(bind) }).toList() }

    internal fun getBy2(bind: BS2): List<E> =
        loggedTransaction { cls.wrapRows(table.select { bound2(bind) }).toList() }

    internal fun createIfNotExistsInDb(bind1: BS1, bind2: BS2): E =
        loggedTransaction { cls.wrapRows(table.select(exists(bind1, bind2))).firstOrNull() ?: createInDb(bind1, bind2) }

    private fun exists(bind1: BS1, bind2: BS2): SqlExpressionBuilder.() -> Op<Boolean> =
        { bound1(bind1) and bound2(bind2) }

    private fun SqlExpressionBuilder.bound1(bind: BS1): Op<Boolean> =
        table.identity1 eq bind.id

    private fun SqlExpressionBuilder.bound2(bind: BS2): Op<Boolean> =
        table.identity2 eq bind.id


}