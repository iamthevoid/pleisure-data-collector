package iam.thevoid.pleisure.data.db._manytoone.facade

import iam.thevoid.pleisure.data.db._manytoone.entity.PeriodDO
import iam.thevoid.pleisure.data.db._manytoone.model.Period
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO

abstract class PeriodStorage : MTOStorage<Period, PeriodDO, EventDO>()