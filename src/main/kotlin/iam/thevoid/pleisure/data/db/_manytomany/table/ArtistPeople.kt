package iam.thevoid.pleisure.data.db._manytomany.table

import iam.thevoid.pleisure.data.db.PleisureTable
import iam.thevoid.pleisure.data.db.main.table.Artists
import iam.thevoid.pleisure.data.db.main.table.Peoples

object ArtistPeople: PleisureTable("mapper_artist_people") {
    val artistId = reference("artist_id", Artists)
    val peopleId = reference("people_id", Peoples)
}
