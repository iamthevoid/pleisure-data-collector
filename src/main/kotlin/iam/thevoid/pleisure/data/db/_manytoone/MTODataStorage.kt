package iam.thevoid.pleisure.data.db._manytoone

import iam.thevoid.pleisure.data.db._manytoone.facade.PeriodStorage
import iam.thevoid.pleisure.data.db._manytoone.impl.PeriodStorageImpl

object MTODataStorage {
    val periods : PeriodStorage by lazy { PeriodStorageImpl }
}