package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.DataModel
import iam.thevoid.pleisure.data.db._manytomany.entity.EventGenreDO
import iam.thevoid.pleisure.data.db._manytomany.entity.EventImageDO
import iam.thevoid.pleisure.data.db._manytomany.entity.EventSellerUrlDO
import iam.thevoid.pleisure.data.db._manytomany.entity.EventTagDO
import iam.thevoid.pleisure.data.db._manytomany.table.EventGenre
import iam.thevoid.pleisure.data.db._manytomany.table.EventImage
import iam.thevoid.pleisure.data.db._manytomany.table.EventSellerUrl
import iam.thevoid.pleisure.data.db._manytomany.table.EventTag
import iam.thevoid.pleisure.data.db._manytoone.entity.PeriodDO
import iam.thevoid.pleisure.data.db._manytoone.table.Periods
import iam.thevoid.pleisure.data.db.main.model.Event
import iam.thevoid.pleisure.data.db.main.table.*
import iam.thevoid.pleisure.data.db.util.mtmChildren
import iam.thevoid.pleisure.data.db.util.mtoChildren
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class EventDO(id: EntityID<Int>) : IntEntity(id), DataModel<Event> {

    companion object : IntEntityClass<EventDO>(Events)

    var name by Events.name
    var place by PlaceDO referencedOn Events.place
    var type by TypeDO referencedOn Events.type
    var artist by ArtistDO optionalReferencedOn Events.artist

    override fun extract(): Event =
        Event.create(
            name,
            place = place.extract(),
            type = type.extract(),
            artist = artist?.extract()
        ).apply {
            sellerUrl = mtmChildren(
                EventSellerUrl, { eventId },
                EventSellerUrlDO, { sellerUrlId },
                SellerUrls,
                SellerUrlDO
            )
            image = mtmChildren(EventImage, { eventId },
                EventImageDO, { imageId }, Images,
                ImageDO
            )
            genres = mtmChildren(
                EventGenre, { eventId },
                EventGenreDO, { genreId }, Genres,
                GenreDO
            )

            period = mtoChildren(Periods, PeriodDO)

            tags = mtmChildren(EventTag, { eventId },
                EventTagDO, { tagId }, Tags,
                TagDO
            )
        }
}

