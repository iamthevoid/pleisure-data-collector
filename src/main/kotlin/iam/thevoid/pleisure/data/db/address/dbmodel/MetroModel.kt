package iam.thevoid.pleisure.data.db.address.dbmodel

import iam.thevoid.pleisure.data.db.address.model.Metro
import iam.thevoid.pleisure.data.db.address.table.Metros
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class MetroModel(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<MetroModel>(Metros)

    var name by Metros.name
    var lineName by Metros.lineName
    var cityId by Metros.cityId

    fun toMetro() = Metro(id.value, name, lineName, cityId)
}