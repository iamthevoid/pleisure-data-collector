package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.PleisureTable

object Addresses : PleisureTable("addresses") {
    val city = text("city").nullable()
    val country = text("country").nullable()
    val countryCode = text("country_code").nullable()
    val raw = text("raw")
    val hash = varchar("hash", 64)
    val lat = float("lat").nullable()
    val lon = float("lon").nullable()
}