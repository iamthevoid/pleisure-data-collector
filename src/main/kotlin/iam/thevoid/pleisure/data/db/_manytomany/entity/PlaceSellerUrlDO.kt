package iam.thevoid.pleisure.data.db._manytomany.entity

import iam.thevoid.pleisure.data.db.main.dbmodel.PlaceDO
import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlDO
import iam.thevoid.pleisure.data.db._manytomany.table.PlaceSellerUrl
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class PlaceSellerUrlDO(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<PlaceSellerUrlDO>(PlaceSellerUrl)

    var placeId by PlaceDO referencedOn  PlaceSellerUrl.placeId
    var sellerUrlId by SellerUrlDO referencedOn  PlaceSellerUrl.sellerUrlId
}