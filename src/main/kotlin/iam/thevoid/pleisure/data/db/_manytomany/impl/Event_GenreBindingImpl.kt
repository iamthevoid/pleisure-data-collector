package iam.thevoid.pleisure.data.db._manytomany.impl

import iam.thevoid.pleisure.data.db._manytomany.entity.EventGenreDO
import iam.thevoid.pleisure.data.db._manytomany.facade.Event_GenreBinding
import iam.thevoid.pleisure.data.db._manytomany.table.EventGenre
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.GenreDO
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

internal object Event_GenreBindingImpl : Event_GenreBinding() {

    override val TABLES: List<Table>
        get() = listOf(EventGenre)

    override val EventGenre.identity1: Column<EntityID<Int>>
        get() = eventId

    override val EventGenre.identity2: Column<EntityID<Int>>
        get() = genreId

    override fun createInDb(bind1: EventDO, bind2: GenreDO): EventGenreDO =
        EventGenreDO.new {
            eventId = bind1
            genreId = bind2
        }
}