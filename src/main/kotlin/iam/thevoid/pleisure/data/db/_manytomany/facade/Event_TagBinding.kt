package iam.thevoid.pleisure.data.db._manytomany.facade

import iam.thevoid.pleisure.data.db._manytomany.entity.EventTagDO
import iam.thevoid.pleisure.data.db._manytomany.table.EventTag
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.TagDO

abstract class Event_TagBinding
    : MTMStorage<EventDO, TagDO, EventTagDO, EventTag>(EventTag, EventTagDO)