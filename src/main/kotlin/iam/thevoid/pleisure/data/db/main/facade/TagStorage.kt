package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.TagDO
import iam.thevoid.pleisure.data.db.main.model.Tag

abstract class TagStorage : Storage<Tag, TagDO>()