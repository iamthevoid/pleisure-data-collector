package iam.thevoid.pleisure.data.db._manytomany.table

import iam.thevoid.pleisure.data.db.PleisureTable
import iam.thevoid.pleisure.data.db.main.table.Events
import iam.thevoid.pleisure.data.db.main.table.SellerUrls

object EventSellerUrl : PleisureTable("mapper_event_seller_url") {
    val eventId = reference("event_id", Events)
    val sellerUrlId = reference("seller_url_id", SellerUrls)
}