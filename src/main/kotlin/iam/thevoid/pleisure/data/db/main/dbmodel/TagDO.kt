package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.DataModel
import iam.thevoid.pleisure.data.db.main.model.Tag
import iam.thevoid.pleisure.data.db.main.table.Tags
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class TagDO(id: EntityID<Int>) : IntEntity(id), DataModel<Tag> {

    var name by Tags.name

    companion object : IntEntityClass<TagDO>(Tags)

    override fun extract(): Tag = Tag.create(name)

}