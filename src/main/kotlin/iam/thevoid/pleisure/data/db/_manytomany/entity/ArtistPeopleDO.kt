package iam.thevoid.pleisure.data.db._manytomany.entity

import iam.thevoid.pleisure.data.db.main.dbmodel.ArtistDO
import iam.thevoid.pleisure.data.db.main.dbmodel.PeopleDO
import iam.thevoid.pleisure.data.db._manytomany.table.ArtistPeople
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class ArtistPeopleDO(id : EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<ArtistPeopleDO>(ArtistPeople)

    var artistId by ArtistDO referencedOn  ArtistPeople.artistId
    var peopleId by PeopleDO referencedOn  ArtistPeople.peopleId
}
