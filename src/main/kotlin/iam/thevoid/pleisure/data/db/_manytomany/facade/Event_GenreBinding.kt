package iam.thevoid.pleisure.data.db._manytomany.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.GenreDO
import iam.thevoid.pleisure.data.db._manytomany.entity.EventGenreDO
import iam.thevoid.pleisure.data.db._manytomany.table.EventGenre

abstract class Event_GenreBinding
    : MTMStorage<EventDO, GenreDO, EventGenreDO, EventGenre>(EventGenre, EventGenreDO)