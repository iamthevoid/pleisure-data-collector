package iam.thevoid.pleisure.data.db.address.table

import org.jetbrains.exposed.dao.IntIdTable

object MetroAssociations : IntIdTable("metro_assoc") {
    var metroId = integer("metro_id")
    var assocName = text("assoc_name")
}