package iam.thevoid.pleisure.data.db._manytomany.facade

import iam.thevoid.pleisure.data.db._manytomany.entity.PlaceSellerUrlDO
import iam.thevoid.pleisure.data.db._manytomany.table.PlaceSellerUrl
import iam.thevoid.pleisure.data.db.main.dbmodel.PlaceDO
import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlDO

abstract class Place_SellerUrlBinding
    : MTMStorage<PlaceDO, SellerUrlDO, PlaceSellerUrlDO, PlaceSellerUrl>(PlaceSellerUrl, PlaceSellerUrlDO)