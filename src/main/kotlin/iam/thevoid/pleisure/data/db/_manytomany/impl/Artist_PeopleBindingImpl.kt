package iam.thevoid.pleisure.data.db._manytomany.impl

import iam.thevoid.pleisure.data.db._manytomany.entity.ArtistPeopleDO
import iam.thevoid.pleisure.data.db._manytomany.facade.Artist_PeopleBinding
import iam.thevoid.pleisure.data.db._manytomany.table.ArtistPeople
import iam.thevoid.pleisure.data.db.main.dbmodel.ArtistDO
import iam.thevoid.pleisure.data.db.main.dbmodel.PeopleDO
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

internal object Artist_PeopleBindingImpl : Artist_PeopleBinding() {

    override val TABLES: List<Table>
        get() = listOf(ArtistPeople)

    override val ArtistPeople.identity1: Column<EntityID<Int>>
        get() = artistId

    override val ArtistPeople.identity2: Column<EntityID<Int>>
        get() = peopleId

    override fun createInDb(bind1: ArtistDO, bind2: PeopleDO): ArtistPeopleDO =
        ArtistPeopleDO.new {
            artistId = bind1
            peopleId = bind2
        }
}