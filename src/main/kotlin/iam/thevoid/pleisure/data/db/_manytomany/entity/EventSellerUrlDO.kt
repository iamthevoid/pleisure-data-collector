package iam.thevoid.pleisure.data.db._manytomany.entity

import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlDO
import iam.thevoid.pleisure.data.db._manytomany.table.EventSellerUrl
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class EventSellerUrlDO(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<EventSellerUrlDO>(EventSellerUrl)

    var eventId by EventDO referencedOn EventSellerUrl.eventId
    var sellerUrlId by SellerUrlDO referencedOn EventSellerUrl.sellerUrlId
}