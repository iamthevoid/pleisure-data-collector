package iam.thevoid.pleisure.data.db._manytomany.table

import iam.thevoid.pleisure.data.db.PleisureTable
import iam.thevoid.pleisure.data.db.main.table.Events
import iam.thevoid.pleisure.data.db.main.table.Images

object EventImage : PleisureTable("mapper_event_image") {
    val eventId = reference("event_id", Events)
    val imageId = reference("image_id", Images)
}