package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.NamedPleisureTable

object SellerUrlTypes : NamedPleisureTable("seller_url_types")