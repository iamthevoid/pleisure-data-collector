package iam.thevoid.pleisure.data.db

interface DataModel<T> {
    fun extract() : T
}