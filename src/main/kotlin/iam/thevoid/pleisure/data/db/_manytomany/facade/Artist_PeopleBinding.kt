package iam.thevoid.pleisure.data.db._manytomany.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.ArtistDO
import iam.thevoid.pleisure.data.db.main.dbmodel.PeopleDO
import iam.thevoid.pleisure.data.db._manytomany.entity.ArtistPeopleDO
import iam.thevoid.pleisure.data.db._manytomany.table.ArtistPeople

abstract class Artist_PeopleBinding
    : MTMStorage<ArtistDO, PeopleDO, ArtistPeopleDO, ArtistPeople>(
    ArtistPeople,
    ArtistPeopleDO
)