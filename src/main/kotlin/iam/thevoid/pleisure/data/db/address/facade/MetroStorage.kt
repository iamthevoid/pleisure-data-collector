package iam.thevoid.pleisure.data.db.address.facade

import iam.thevoid.pleisure.data.db.address.model.City
import iam.thevoid.pleisure.data.db.address.model.Metro
import iam.thevoid.pleisure.data.db.address.model.MetroAssoc

interface MetroStorage {

    fun metro() : List<Metro>

    fun metro(city: City) : List<Metro>

    fun associations(metro: Metro) : List<MetroAssoc>

}