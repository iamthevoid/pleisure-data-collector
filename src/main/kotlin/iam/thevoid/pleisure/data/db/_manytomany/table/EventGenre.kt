package iam.thevoid.pleisure.data.db._manytomany.table

import iam.thevoid.pleisure.data.db.main.table.Events
import iam.thevoid.pleisure.data.db.PleisureTable
import iam.thevoid.pleisure.data.db.main.table.Genres

object EventGenre : PleisureTable("mapper_event_genre") {
    val eventId = reference("event_id", Events)
    val genreId = reference("genre_id", Genres)
}
