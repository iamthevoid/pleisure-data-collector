package iam.thevoid.pleisure.data.db.address.dbmodel

import iam.thevoid.pleisure.data.db.address.model.MetroAssoc
import iam.thevoid.pleisure.data.db.address.table.MetroAssociations
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class MetroAssocModel(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<MetroAssocModel>(MetroAssociations)

    var name by MetroAssociations.assocName
    var metroId by MetroAssociations.metroId

    fun toMetroAssoc() = MetroAssoc(name)
}