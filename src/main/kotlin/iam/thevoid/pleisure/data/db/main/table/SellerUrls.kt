package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.PleisureTable

object SellerUrls : PleisureTable("seller_url") {

    // reference on seller
    val seller = reference("seller", Sellers)

    // seller url type
    val type = reference("type", SellerUrlTypes)

    // part of url to prefetch
    val url = text("url")
}