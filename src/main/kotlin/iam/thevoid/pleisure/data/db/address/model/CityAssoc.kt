package iam.thevoid.pleisure.data.db.address.model

import iam.thevoid.pleisure.data.db.address.Assoc

data class CityAssoc(override val value : String) : Assoc