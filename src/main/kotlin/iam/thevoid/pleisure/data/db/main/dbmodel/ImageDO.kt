package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.main.model.Image
import iam.thevoid.pleisure.data.db.main.table.Images
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class ImageDO(id : EntityID<Int>) : IntEntity(id), DataModel<Image> {

    override fun extract(): Image = Image.create(small, medium, big, origin)

    companion object : IntEntityClass<ImageDO>(Images)

    var small by Images.small
    var medium by Images.medium
    var big by Images.big
    var origin by Images.origin
    var hash by Images.hash
}