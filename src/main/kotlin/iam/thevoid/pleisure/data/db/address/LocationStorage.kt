package iam.thevoid.pleisure.data.db.address

import iam.thevoid.pleisure.data.db.address.facade.CityStorage
import iam.thevoid.pleisure.data.db.address.facade.CountryCodeStorage
import iam.thevoid.pleisure.data.db.address.facade.CountryStorage
import iam.thevoid.pleisure.data.db.address.facade.MetroStorage

object LocationStorage {

    private val storage by lazy { LocationStorageImpl }

    val city: CityStorage by lazy { storage }
    val country: CountryStorage by lazy { storage }
    val code: CountryCodeStorage by lazy { storage }
    val metro: MetroStorage by lazy { storage }
}