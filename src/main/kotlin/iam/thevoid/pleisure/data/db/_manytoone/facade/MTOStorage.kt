package iam.thevoid.pleisure.data.db._manytoone.facade

import iam.thevoid.pleisure.data.db.DB
import org.jetbrains.exposed.dao.IntEntity

abstract class MTOStorage<Model, Entity, Relation> : DB() where Entity : iam.thevoid.pleisure.data.db.DataModel<Model>, Relation : IntEntity {

    internal abstract fun createEntityInDb(item: Model, relation: Relation): Entity

    internal open fun createContentsInDb(item: Model, relation: Relation) = Unit

    internal open fun bindEntityWithContents(item: Model, relation: Relation, entity: Entity) = Unit

    internal fun createInDb(item: Model, relation: Relation): Entity =
        loggedTransaction {
            createEntityInDb(item, relation)
                .also {
                    createContentsInDb(item, relation)
                    bindEntityWithContents(item, relation, it)
                }
        }

    internal open fun createIfNotExistsInDb(item: Model, relation: Relation): Entity =
        TODO("Not implemented for ${this::class.java.simpleName}")

    internal open fun createOrUpdateInDb(item: Model, relation: Relation): Entity =
        TODO("Not implemented for ${this::class.java.simpleName}")

    fun createIfNotExists(item: Model, relation: Relation): Model =
        loggedTransaction { createIfNotExistsInDb(item, relation).extract() }

    fun createIfNotExists(list: List<Model>, relation: Relation): List<Model> =
        loggedTransaction { list.map { createIfNotExistsInDb(it, relation).extract() } }

    fun createOrUpdate(item: Model, relation: Relation): Model =
        loggedTransaction { createOrUpdateInDb(item, relation).extract() }

    fun createOrUpdate(list: List<Model>, relation: Relation): List<Model> =
        loggedTransaction { list.map { createOrUpdateInDb(it, relation).extract() } }

    fun create(item: Model, relation: Relation): Model =
        loggedTransaction { createInDb(item, relation).extract() }

    fun create(list: List<Model>, relation: Relation): List<Model> =
        loggedTransaction { list.map { createInDb(it, relation).extract() } }
}