package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db.main.dbmodel.TagDO
import iam.thevoid.pleisure.data.db.main.facade.TagStorage
import iam.thevoid.pleisure.data.db.main.model.Tag
import iam.thevoid.pleisure.data.db.main.table.Tags
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.select

internal object TagStorageImpl : TagStorage() {

    override val TABLES: List<Table>
        get() = listOf(Tags)

    override fun Transaction.getFromDb(item: Tag): TagDO? =
        TagDO.wrapRows(Tags.select { Tags.name eq item.name }).firstOrNull()

    override fun Transaction.createEntityInDb(item: Tag) =
        TagDO.new { name = item.name }
}