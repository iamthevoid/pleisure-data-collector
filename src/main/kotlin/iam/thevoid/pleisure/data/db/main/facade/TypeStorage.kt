package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.TypeDO
import iam.thevoid.pleisure.data.db.main.model.Type

abstract class TypeStorage : Storage<Type, TypeDO>()