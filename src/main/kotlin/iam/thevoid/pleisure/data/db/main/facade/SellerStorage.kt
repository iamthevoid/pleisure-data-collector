package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.SellerDO
import iam.thevoid.pleisure.data.db.main.model.Seller

abstract class SellerStorage : Storage<Seller, SellerDO>() {

    abstract fun seller(url : String) : Seller?

}