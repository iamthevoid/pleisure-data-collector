package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.ImageDO
import iam.thevoid.pleisure.data.db.main.model.Image

abstract class ImageStorage : Storage<Image, ImageDO>()