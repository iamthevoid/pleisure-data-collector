package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.util.mtmChildren
import iam.thevoid.pleisure.data.db._manytomany.entity.ArtistGenreDO
import iam.thevoid.pleisure.data.db._manytomany.entity.ArtistPeopleDO
import iam.thevoid.pleisure.data.db.main.model.Artist
import iam.thevoid.pleisure.data.db.main.table.Artists
import iam.thevoid.pleisure.data.db.main.table.Genres
import iam.thevoid.pleisure.data.db.main.table.Peoples
import iam.thevoid.pleisure.data.db._manytomany.table.ArtistGenre
import iam.thevoid.pleisure.data.db._manytomany.table.ArtistPeople
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class ArtistDO(id: EntityID<Int>) : IntEntity(id), DataModel<Artist> {

    var name by Artists.name
    var description by Artists.description
    var image by ImageDO optionalReferencedOn Artists.image

    companion object : IntEntityClass<ArtistDO>(Artists)

    override fun extract(): Artist =
        Artist.create(
            name,
            description,
            image?.extract()
        ).apply {
            genre = mtmChildren(
                ArtistGenre, { artistId },
                ArtistGenreDO, { genreId }, Genres, GenreDO
            )
            people = mtmChildren(
                ArtistPeople, { artistId },
                ArtistPeopleDO, { peopleId },
                Peoples,
                PeopleDO
            )
        }

}