package iam.thevoid.pleisure.data.db.main.model

import iam.thevoid.e.safe
import iam.thevoid.pleisure.utils.hash

class Image private constructor(
    val logo: String?,
    val small: String?,
    val medium: String?,
    val big: String?,
    val origin: String?
) {

    fun hash() = (small.safe() + medium.safe() + big.safe() + origin.safe()).hash()

    companion object {

        fun create(
            small: String? = null,
            medium: String? = null,
            big: String? = null,
            origin: String? = null,
            logo : String? = null
        ) = Image(logo, small, medium, big, origin)
    }
}

//fun Image.clone() = Image.create(small, medium, big, origin)
