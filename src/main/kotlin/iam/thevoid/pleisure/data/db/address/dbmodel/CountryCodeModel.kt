package iam.thevoid.pleisure.data.db.address.dbmodel

import iam.thevoid.pleisure.data.db.address.model.CountryCode
import iam.thevoid.pleisure.data.db.address.table.CountryCodes
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class CountryCodeModel(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<CountryCodeModel>(CountryCodes)

    val name by CountryCodes.name

    fun toCountryCode() = CountryCode(id.value, name)
}