package iam.thevoid.pleisure.data.db._manytoone.table

import iam.thevoid.pleisure.data.db.PleisureTable
import iam.thevoid.pleisure.data.db.main.table.Events

object
Periods : PleisureTable("periods") {
    val eventId = reference("event_id", Events)
    val startTime = integer("start_time")
    val startDate = text("start_date")
    val endTime = integer("end_time").nullable()
    val endDate = text("end_date").nullable()
}