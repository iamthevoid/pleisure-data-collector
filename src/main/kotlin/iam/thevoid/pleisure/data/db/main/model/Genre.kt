package iam.thevoid.pleisure.data.db.main.model

class Genre private constructor(val name: String) {

    override fun toString(): String = name

    companion object {

        const val UNKNOWN = "unknown"

        fun create(
            name: String = UNKNOWN
        ) = Genre(name)
    }
}

//fun Genre.clone() = Genre.create(name)