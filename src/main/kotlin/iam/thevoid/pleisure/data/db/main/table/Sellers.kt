package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.NamedPleisureTable

object Sellers : NamedPleisureTable("sellers") {
    val url = text("url")
    val logoUrl = text("logo_url")
    val eventUrlPattern = text("event_url_pattern")
    val placeUrlPattern = text("place_url_pattern")
}