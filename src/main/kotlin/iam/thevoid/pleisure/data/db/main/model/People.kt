package iam.thevoid.pleisure.data.db.main.model

class People private constructor(
    val name: String,
    val role: String?,
    val birthdate: String?,
    val description: String?,
    val image: Image?
) {
    companion object {
        fun create(
            name: String = "",
            role: String? = null,
            birthdate: String? = null,
            description: String? = null,
            image : Image? = null
        ) = People(name, role, birthdate, description, image)
    }
}

//fun People.clone() = People.create(name, role, birthdate, description)