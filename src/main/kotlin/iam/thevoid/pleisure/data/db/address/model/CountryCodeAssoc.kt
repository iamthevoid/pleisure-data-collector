package iam.thevoid.pleisure.data.db.address.model

import iam.thevoid.pleisure.data.db.address.Assoc

data class CountryCodeAssoc(override val value : String) : Assoc