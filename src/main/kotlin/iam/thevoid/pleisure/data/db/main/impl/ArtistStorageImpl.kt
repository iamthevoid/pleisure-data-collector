package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db._manytomany.MTMDataStorage
import iam.thevoid.pleisure.data.db.main.DataStorage
import iam.thevoid.pleisure.data.db.main.dbmodel.ArtistDO
import iam.thevoid.pleisure.data.db.main.facade.ArtistStorage
import iam.thevoid.pleisure.data.db.main.model.Artist
import iam.thevoid.pleisure.data.db.main.table.Artists
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.select

internal object ArtistStorageImpl : ArtistStorage() {

    override val TABLES: List<Table>
        get() = listOf(Artists)

    override fun Transaction.getFromDb(item: Artist): ArtistDO? =
        ArtistDO.wrapRows(Artists.select { Artists.name eq item.name }).firstOrNull()

    override fun Transaction.createEntityInDb(item: Artist) =
        ArtistDO.new {
            name = item.name
            description = item.description
            image = item.image?.let { DataStorage.images.createIfNotExistsInDb(it) }
        }

    override fun Transaction.bindEntityWithContents(item: Artist, entity: ArtistDO) {
        item.genre.forEach { genre ->
            MTMDataStorage.artist_genre.createIfNotExistsInDb(
                entity,
                DataStorage.genres.createIfNotExistsInDb(genre)
            )
        }

        item.people.forEach { people ->
            MTMDataStorage.artist_people.createIfNotExistsInDb(
                entity,
                DataStorage.people.createIfNotExistsInDb(people)
            )
        }
    }
}