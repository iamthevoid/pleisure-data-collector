package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.main.model.Type
import iam.thevoid.pleisure.data.db.main.table.Types
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class TypeDO(id : EntityID<Int>) : IntEntity(id), DataModel<Type> {

    var name by Types.name

    companion object : IntEntityClass<TypeDO>(Types)

    override fun extract(): Type = Type.create(name)
}