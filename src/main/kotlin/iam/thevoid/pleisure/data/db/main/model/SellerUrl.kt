package iam.thevoid.pleisure.data.db.main.model

class SellerUrl private constructor(
    val url: String,
    val type: SellerUrlType,
    val seller: Seller
) {
    override fun toString(): String = urlFull

    val urlFull =
        when (type.name) {
            SellerUrlType.EVENT.name -> seller.eventUrlPattern
            else -> seller.placeUrlPattern
        }.replace(ID_REPL, url).let { "${seller.url}$it" }

    companion object {

        const val ID_REPL = "{ID}"

        fun create(
            seller: Seller = Seller.create(),
            sellerUrlType: SellerUrlType = SellerUrlType.create(),
            url: String = ""
        ) = SellerUrl(url, sellerUrlType, seller)
    }
}

//fun SellerUrl.clone() = SellerUrl.create(seller.clone(), type.clone(), url)