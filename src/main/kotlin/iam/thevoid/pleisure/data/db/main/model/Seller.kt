package iam.thevoid.pleisure.data.db.main.model

class Seller private constructor(
    val name: String,
    val url: String,
    val logoUrl: String,
    val placeUrlPattern : String,
    val eventUrlPattern : String
) {
    companion object {

        fun create(
            url: String = "",
            name: String = "",
            logoUrl: String = "",
            placeUrlPattern: String = "",
            eventUrlPattern: String = ""
        ) = Seller(
            name,
            url,
            logoUrl,
            placeUrlPattern,
            eventUrlPattern
        )
    }
}

//fun Seller.clone() = Seller.create(url, name, logoUrl)