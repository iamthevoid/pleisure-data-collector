package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.NamedPleisureTable

object Artists : NamedPleisureTable("artists") {
    val image = optReference("image", Images)
    val description = text("description").nullable()
    val genres = text("genre")
    val people = text("people")
}