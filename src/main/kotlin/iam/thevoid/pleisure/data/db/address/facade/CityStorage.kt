package iam.thevoid.pleisure.data.db.address.facade

import iam.thevoid.pleisure.data.db.address.model.City
import iam.thevoid.pleisure.data.db.address.model.CityAssoc

interface CityStorage {

    fun cities() : List<City>

    fun associations(city : City) : List<CityAssoc>

}