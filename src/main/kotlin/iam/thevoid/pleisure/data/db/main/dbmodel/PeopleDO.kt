package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.main.model.People
import iam.thevoid.pleisure.data.db.main.table.Peoples
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class PeopleDO(id: EntityID<Int>) : IntEntity(id), DataModel<People> {

    companion object : IntEntityClass<PeopleDO>(Peoples)

    var name by Peoples.name
    var role by Peoples.role
    var description by Peoples.description
    var image by ImageDO optionalReferencedOn Peoples.image
    var birthdate by Peoples.birthdate

    override fun extract(): People = People.create(name, role, birthdate, description, image?.extract())
}