package iam.thevoid.pleisure.data.db.address

interface Entity {
    val id : Int
    val name : String
    fun anotherAssociation(value : String) = false
}