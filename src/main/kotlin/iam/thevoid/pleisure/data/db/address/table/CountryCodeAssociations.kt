package iam.thevoid.pleisure.data.db.address.table

import org.jetbrains.exposed.dao.IntIdTable

object CountryCodeAssociations : IntIdTable("country_code_assoc") {
    var countryCodeId = integer("country_code_id")
    var assocName = text("assoc_name")
}