package iam.thevoid.pleisure.data.db.address.table

import org.jetbrains.exposed.dao.IntIdTable

object CityAssociations : IntIdTable("city_assoc") {
    var cityId = integer("city_id")
    var assocName = text("assoc_name")
}