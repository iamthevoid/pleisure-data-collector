package iam.thevoid.pleisure.data.db.main

import iam.thevoid.pleisure.data.db.main.facade.*
import iam.thevoid.pleisure.data.db.main.impl.*

object DataStorage {

    suspend operator fun <T> invoke(block : suspend DataStorage.() -> T) = DataStorage.block()

    val addresses : AddressStorage by lazy { AddressStorageImpl }
    val artists : ArtistStorage by lazy { ArtistStorageImpl }
    val images : ImageStorage by lazy { ImageStorageImpl }
    val genres : GenreStorage by lazy { GenreStorageImpl }
    val people : PeopleStorage by lazy { PeopleStorageImpl }
    val places : PlaceStorage by lazy { PlaceStorageImpl }
    val sellers : SellerStorage by lazy { SellerStorageImpl }
    val tags : TagStorage by lazy { TagStorageImpl }
    val types : TypeStorage by lazy { TypeStorageImpl }
    val events : EventStorage by lazy { EventStorageImpl }
    val sellerUrls : SellerUrlStorage by lazy { SellerUrlStorageImpl }
    val sellerUrlTypes : SellerUrlTypeStorage by lazy { SellerUrlTypeStorageImpl }
}