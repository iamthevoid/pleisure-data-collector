package iam.thevoid.pleisure.data.db.address.model

import iam.thevoid.pleisure.data.db.address.Entity

data class Metro(
    override val id: Int,
    override val name: String,
    val lineName: String?,
    val cityId: Int
) : Entity