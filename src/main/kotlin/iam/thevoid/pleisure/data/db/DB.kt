package iam.thevoid.pleisure.data.db

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

abstract class DB {

    companion object {

        const val DB_MAIN = "jdbc:sqlite:data.db"
        const val DB_TEST = "jdbc:sqlite:test.db"

        const val DRIVER_SQLITE = "org.sqlite.JDBC"
    }

     open val PATH: String
        get() = DB_TEST

     private val DRIVER: String
        get() = DRIVER_SQLITE


    protected abstract val TABLES: List<Table>

    protected val db by lazy { Database.connect(PATH, DRIVER) }

    private fun init() {
        db
        transaction {
            addLogger(StdOutSqlLogger)
            for (table in TABLES)
                if (!table.exists())
                    SchemaUtils.create(table)
        }
    }

    fun <T> loggedTransaction(statement: Transaction.() -> T) =
        init().run {
            transaction {
                addLogger(StdOutSqlLogger)
                statement()
            }
        }

}