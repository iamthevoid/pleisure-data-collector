package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.PleisureTable

object Places : PleisureTable("places") {
    val name = text("name")
    val description = text("description")
    val url = text("url")
    val address = reference("address", Addresses)
    val hash = varchar("hash", 64)
}