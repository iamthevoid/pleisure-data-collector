package iam.thevoid.pleisure.data.db.address.dbmodel

import iam.thevoid.pleisure.data.db.address.model.City
import iam.thevoid.pleisure.data.db.address.table.Cities
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class CityModel(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<CityModel>(Cities)

    val name by Cities.name
    val nameEn by Cities.nameEn

    fun toCity() = City(id.value, name, nameEn)
}