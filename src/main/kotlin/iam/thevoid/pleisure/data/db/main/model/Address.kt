package iam.thevoid.pleisure.data.db.main.model

import iam.thevoid.e.*
import iam.thevoid.pleisure.data.api.services.location.Associations
import iam.thevoid.pleisure.utils.ext.charactersOnly
import iam.thevoid.pleisure.utils.hash
import iam.thevoid.pleisure.utils.prepareRawAddress

class Address private constructor(
    val city: String?,
    val country: String?,
    val countryCode: String?,
    val raw: String?,
    var lat: Float?,
    var lon: Float?
) {

    fun lat(precision : Int? = null) =
        precision?.let { lat.safe().format(it).safeFloat() } ?: lat

    fun lon(precision : Int? = null) =
        precision?.let { lon.safe().format(it).safeFloat() } ?: lon

    companion object {

        fun createFromRaw(raw: String? = null) =
            create(
                raw = prepareRawAddress(raw.safe())
            )

        fun create(
            city: String? = null,
            country: String? = null,
            countryCode: String? = null,
            raw: String? = null,
            lat: Float? = null,
            lon: Float? = null
        ) = Address(
            Associations.cityFor(city),
            Associations.countryFor(country),
            Associations.codeFor(countryCode),
            raw,
            lat,
            lon
        )
    }
}

//fun Address.clone() = Address.create(city, country, countryCode, raw, lat, lon)

fun Address?.hash() = this?.raw
    ?.safe()
    ?.charactersOnly()
    ?.toLowerCase()
    .hash()

fun Address?.empty() =
    this == null || city.isNullOrEmpty() && country.isNullOrEmpty() && countryCode.isNullOrEmpty()

fun Address?.basis() = this != null &&
        (raw.isNotNullOrBlank() ||
                lat.isNotNull() &&
                lon.isNotNull())

fun Address?.full() = this != null &&
        city.isNotNullOrBlank() &&
        country.isNotNullOrBlank() &&
        countryCode.isNotNullOrBlank() &&
        raw.isNotNullOrBlank() &&
        lat.isNotNull() &&
        lon.isNotNull()

