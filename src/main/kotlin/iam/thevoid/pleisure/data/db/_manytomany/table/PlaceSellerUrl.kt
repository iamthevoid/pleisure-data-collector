package iam.thevoid.pleisure.data.db._manytomany.table

import iam.thevoid.pleisure.data.db.PleisureTable
import iam.thevoid.pleisure.data.db.main.table.Places
import iam.thevoid.pleisure.data.db.main.table.SellerUrls

object PlaceSellerUrl : PleisureTable("mapper_place_seller_url") {
    val placeId = reference("place_id", Places)
    val sellerUrlId = reference("seller_url_id", SellerUrls)
}