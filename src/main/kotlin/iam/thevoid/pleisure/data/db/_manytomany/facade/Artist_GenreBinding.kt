package iam.thevoid.pleisure.data.db._manytomany.facade

import iam.thevoid.pleisure.data.db._manytomany.entity.ArtistGenreDO
import iam.thevoid.pleisure.data.db._manytomany.table.ArtistGenre
import iam.thevoid.pleisure.data.db.main.dbmodel.ArtistDO
import iam.thevoid.pleisure.data.db.main.dbmodel.GenreDO

abstract class Artist_GenreBinding
    : MTMStorage<ArtistDO, GenreDO, ArtistGenreDO, ArtistGenre>(ArtistGenre, ArtistGenreDO)