package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.NamedPleisureTable

object Types : NamedPleisureTable("event_types")