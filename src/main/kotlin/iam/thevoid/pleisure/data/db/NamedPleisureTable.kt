package iam.thevoid.pleisure.data.db

open class NamedPleisureTable(name: String) : PleisureTable(name) {
    val name = text("name")
}