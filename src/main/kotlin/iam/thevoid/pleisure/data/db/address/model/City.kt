package iam.thevoid.pleisure.data.db.address.model

import iam.thevoid.pleisure.data.db.address.Entity

data class City(
    override val id: Int,
    override val name : String,
    val nameEn : String
) : Entity {
    override fun anotherAssociation(value: String): Boolean =
        nameEn == value
}