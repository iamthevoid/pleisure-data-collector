package iam.thevoid.pleisure.data.db._manytomany.table

import iam.thevoid.pleisure.data.db.PleisureTable
import iam.thevoid.pleisure.data.db.main.table.Events
import iam.thevoid.pleisure.data.db.main.table.Tags

object EventTag : PleisureTable("mapper_event_tag") {
    val eventId = reference("event_id", Events)
    val tagId = reference("tag_id", Tags)
}
