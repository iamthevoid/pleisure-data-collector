package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db.main.dbmodel.ImageDO
import iam.thevoid.pleisure.data.db.main.facade.ImageStorage
import iam.thevoid.pleisure.data.db.main.model.Image
import iam.thevoid.pleisure.data.db.main.table.Images
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.select

internal object ImageStorageImpl : ImageStorage() {

    override val TABLES: List<Table>
        get() = listOf(Images)

    override fun Transaction.getFromDb(item: Image): ImageDO? =
        ImageDO.wrapRows(Images.select { Images.hash eq item.hash() }).firstOrNull()

    override fun Transaction.createEntityInDb(item: Image) =
        ImageDO.new {
            big = item.big
            small = item.small
            medium = item.medium
            origin = item.origin
            hash = item.hash()
        }
}