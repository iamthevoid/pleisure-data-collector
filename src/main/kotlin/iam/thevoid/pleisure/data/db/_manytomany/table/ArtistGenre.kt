package iam.thevoid.pleisure.data.db._manytomany.table

import iam.thevoid.pleisure.data.db.PleisureTable
import iam.thevoid.pleisure.data.db.main.table.Artists
import iam.thevoid.pleisure.data.db.main.table.Genres

object ArtistGenre: PleisureTable("mapper_artist_genre") {
    val artistId = reference("artist_id", Artists)
    val genreId = reference("genre_id", Genres)
}
