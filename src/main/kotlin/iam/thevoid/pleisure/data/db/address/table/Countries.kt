package iam.thevoid.pleisure.data.db.address.table

import org.jetbrains.exposed.dao.IntIdTable

object Countries : IntIdTable("country") {
    var name = text("fl_name")
    var nameEn = text("en_name")
}