package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db.main.dbmodel.TypeDO
import iam.thevoid.pleisure.data.db.main.facade.TypeStorage
import iam.thevoid.pleisure.data.db.main.model.Type
import iam.thevoid.pleisure.data.db.main.table.Types
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.select

internal object TypeStorageImpl : TypeStorage() {

    override val TABLES: List<Table>
        get() = listOf(Types)

    override fun Transaction.getFromDb(item: Type): TypeDO? =
        TypeDO.wrapRows(Types.select { Types.name eq item.name }).firstOrNull()

    override fun Transaction.createEntityInDb(item: Type) =
        TypeDO.new { name = item.name }
}