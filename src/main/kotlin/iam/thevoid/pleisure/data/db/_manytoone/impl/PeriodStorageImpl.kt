package iam.thevoid.pleisure.data.db._manytoone.impl

import iam.thevoid.e.isNull
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.db._manytoone.entity.PeriodDO
import iam.thevoid.pleisure.data.db._manytoone.facade.PeriodStorage
import iam.thevoid.pleisure.data.db._manytoone.model.Period
import iam.thevoid.pleisure.data.db._manytoone.table.Periods
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.util.fromBoolean
import org.jetbrains.exposed.sql.*

internal object PeriodStorageImpl : PeriodStorage() {

    override val TABLES: List<Table>
        get() = listOf(Periods)

    override fun createIfNotExistsInDb(item: Period, relation : EventDO): PeriodDO = PeriodDO.wrapRows(
        Periods.select {
            Periods.startTime.eq(item.startTime)
                .and(Periods.eventId.eq(relation.id))
                .and(Periods.startDate.eq(item.startDate))
                .and(Periods.endTime.isNull().and(Op.fromBoolean(item.endTime.isNull()))
                    .or(Periods.endTime.isNotNull().and(Periods.endTime.eq(item.endTime.safe()))))
                .and(
                    Periods.endDate.isNull().and(Op.fromBoolean(item.endDate.isNull()))
                        .or(Periods.endDate.isNotNull().and(
                                Periods.endDate.eq(item.endDate.safe()))))
        }
    ).firstOrNull() ?: createInDb(item, relation)

    override fun createEntityInDb(item: Period, relation: EventDO) =
        PeriodDO.new {
            eventId = relation
            startTime = item.startTime
            startDate = item.startDate
            endTime = item.endTime
            endDate = item.endDate
        }
}