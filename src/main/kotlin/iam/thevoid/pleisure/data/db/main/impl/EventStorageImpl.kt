package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db._manytomany.MTMDataStorage
import iam.thevoid.pleisure.data.db._manytoone.MTODataStorage
import iam.thevoid.pleisure.data.db._manytoone.table.Periods
import iam.thevoid.pleisure.data.db.main.DataStorage
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.facade.EventStorage
import iam.thevoid.pleisure.data.db.main.model.Event
import iam.thevoid.pleisure.data.db.main.table.Events
import iam.thevoid.pleisure.data.db.main.table.Genres
import iam.thevoid.pleisure.data.db.main.table.Places
import iam.thevoid.pleisure.data.db.main.table.Tags
import org.jetbrains.exposed.sql.*

internal object EventStorageImpl : EventStorage() {

    override val TABLES: List<Table>
        get() = listOf(Events, Periods, Genres, Tags)

    override fun Transaction.getFromDb(item: Event): EventDO? {
        val byName = Events.innerJoin(Places)
            .select { Events.name.eq(item.name).and(Places.hash.eq(item.place.hash)) }.alias("similar")
        val joined = Periods.innerJoin(byName, { eventId }, { byName[Events.id] })
        val withPeriods = joined.select { Periods.eventId.eq(byName[Events.id]) }.alias("with_periods")
        val eventStartTimes = item.period.map { it.startTime }
        val eventStartDates = item.period.map { it.startDate }
        val rows = withPeriods
            .slice(withPeriods.columns)
            .select {
                withPeriods[Periods.startTime].inList(eventStartTimes)
                    .and(withPeriods[Periods.startDate].inList(eventStartDates))
            }
        return rows.firstOrNull()?.get(withPeriods[Periods.eventId])?.let { id -> EventDO.findById(id) }
    }

    override fun Transaction.createEntityInDb(item: Event) = EventDO.new {
        name = item.name
        place = item.place.let { DataStorage.places.createIfNotExistsInDb(it) }
        artist = item.artist?.let { DataStorage.artists.createIfNotExistsInDb(it) }
        type = item.type.let { DataStorage.types.createIfNotExistsInDb(it) }
    }

    override fun Transaction.bindEntityWithContents(item: Event, entity: EventDO) {
        item.period.forEach { MTODataStorage.periods.createIfNotExistsInDb(it, entity) }
        item.image.forEach { image ->
            MTMDataStorage.event_image.createIfNotExistsInDb(
                entity,
                DataStorage.images.createIfNotExistsInDb(image)
            )
        }
        item.sellerUrl.forEach { sellerUrl ->
            MTMDataStorage.event_sellerUrl.createIfNotExistsInDb(
                entity,
                DataStorage.sellerUrls.createIfNotExistsInDb(sellerUrl)
            )
        }
        item.genres.forEach { genre ->
            MTMDataStorage.event_genre.createIfNotExistsInDb(
                entity,
                DataStorage.genres.createIfNotExistsInDb(genre)
            )
        }
        item.tags.forEach { tag ->
            MTMDataStorage.event_tag.createIfNotExistsInDb(
                entity,
                DataStorage.tags.createIfNotExistsInDb(tag)
            )
        }
    }
}