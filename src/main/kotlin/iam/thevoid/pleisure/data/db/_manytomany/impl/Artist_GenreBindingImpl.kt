package iam.thevoid.pleisure.data.db._manytomany.impl

import iam.thevoid.pleisure.data.db._manytomany.entity.ArtistGenreDO
import iam.thevoid.pleisure.data.db._manytomany.facade.Artist_GenreBinding
import iam.thevoid.pleisure.data.db._manytomany.table.ArtistGenre
import iam.thevoid.pleisure.data.db.main.dbmodel.ArtistDO
import iam.thevoid.pleisure.data.db.main.dbmodel.GenreDO
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

internal object Artist_GenreBindingImpl : Artist_GenreBinding() {

    override val TABLES: List<Table>
        get() = listOf(ArtistGenre)

    override val ArtistGenre.identity1: Column<EntityID<Int>>
        get() = artistId

    override val ArtistGenre.identity2: Column<EntityID<Int>>
        get() = genreId

    override fun createInDb(bind1: ArtistDO, bind2: GenreDO): ArtistGenreDO =
        ArtistGenreDO.new {
            artistId = bind1
            genreId = bind2
        }
}