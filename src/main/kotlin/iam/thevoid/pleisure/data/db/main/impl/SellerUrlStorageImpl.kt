package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db.main.DataStorage
import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlDO
import iam.thevoid.pleisure.data.db.main.facade.SellerUrlStorage
import iam.thevoid.pleisure.data.db.main.model.Seller
import iam.thevoid.pleisure.data.db.main.model.SellerUrl
import iam.thevoid.pleisure.data.db.main.model.SellerUrlType
import iam.thevoid.pleisure.data.db.main.table.SellerUrlTypes
import iam.thevoid.pleisure.data.db.main.table.SellerUrls
import iam.thevoid.pleisure.data.db.main.table.Sellers
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select

internal object SellerUrlStorageImpl : SellerUrlStorage() {

    override val TABLES: List<Table>
        get() = listOf(SellerUrls)

    override fun Transaction.getFromDb(item: SellerUrl): SellerUrlDO? =
        SellerUrlDO.wrapRows(SellerUrls.select { SellerUrls.url eq item.url }).firstOrNull()

    override fun Transaction.createEntityInDb(item: SellerUrl): SellerUrlDO =
        SellerUrlDO.new {
            url = item.url
            seller = DataStorage.sellers.createIfNotExistsInDb(item.seller)
            type = DataStorage.sellerUrlTypes.createIfNotExistsInDb(item.type)
        }

    override fun sellerUrls(seller: Seller, type: SellerUrlType): List<SellerUrl> =
        loggedTransaction {
            SellerUrlDO.wrapRows(SellerUrls.innerJoin(Sellers).innerJoin(SellerUrlTypes)
                .slice(SellerUrls.columns)
                .select { Sellers.name eq seller.name and (SellerUrlTypes.name eq type.name) })
                .map { it.extract() }
        }
}