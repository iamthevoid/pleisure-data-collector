package iam.thevoid.pleisure.data.db._manytoone.model

import iam.thevoid.e.parseSafe
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.db.main.model.Event
import iam.thevoid.pleisure.utils.DateUtil
import iam.thevoid.pleisure.utils.ext.formatDate
import iam.thevoid.pleisure.utils.ext.timeImMinutes
import iam.thevoid.pleisure.utils.formatTimePart
import java.util.*

class Period private constructor(
    val startTime: Int,
    val startDate: String,
    val endTime: Int?,
    val endDate: String?,
    var event: Event? = null
) {

    val start
        get() = Calendar.getInstance().apply {
            time = DateUtil.dateFormat(DATE_FORMAT).parseSafe(startDate) { Date(0) }
            set(Calendar.HOUR_OF_DAY, startTime / 60)
            set(Calendar.MINUTE, startTime % 60)
        }.time

    fun isOpenDate() =
        startTime == MINUTE_FIRST && endTime == MINUTE_LAST && startDate == START_OF_DATE && endDate == END_OF_DATE

    fun isOutdated() =
        startTime == MINUTE_FIRST && endTime == MINUTE_LAST && startDate == START_OF_DATE && endDate == OUTDATED

    fun isUnknown() =
        startTime == MINUTE_FIRST && endTime == null && startDate == START_OF_DATE && endDate == null

    override fun toString(): String =
        "start: ${format(startTime, startDate)}${endDate?.let { ", end: ${format(endTime, it)}" } ?: ""}"

    private fun format(time: Int?, date: String) =
        "${formatTimePart(time.safe() / 60)}:${formatTimePart(time.safe() % 60)} $date"

    companion object {

        private const val DATE_FORMAT = "dd-MM-yyyy"

        private const val MINUTE_FIRST = 0
        private const val MINUTE_LAST = 1439

        private const val START_OF_DATE = "1.1.1900"
        private const val END_OF_DATE = "31.12.2099"
        private const val OUTDATED = "1.1.2000"


        val open by lazy {
            create(
                MINUTE_FIRST,
                START_OF_DATE,
                MINUTE_LAST,
                END_OF_DATE
            )
        }
        val outdated by lazy {
            create(
                MINUTE_FIRST,
                START_OF_DATE,
                MINUTE_LAST,
                OUTDATED
            )
        }
        val unknown by lazy {
            create(
                MINUTE_FIRST,
                START_OF_DATE
            )
        }

        fun create(start: Date, end: Date? = null) =
            create(
                start.timeImMinutes(),
                start.formatDate(),
                end?.timeImMinutes(),
                end?.formatDate()
            )

        fun create(
            startTime: Int = Date().timeImMinutes(),
            startDate: Date = Date(),
            endTime: Int? = null,
            endDate: Date? = null
        ) = create(startTime, startDate.formatDate(), endTime, endDate?.formatDate())

        fun create(
            startTime: Int = Date().timeImMinutes(),
            startDate: String = Date().formatDate(),
            endTime: Int? = null,
            endDate: String? = null
        ) = Period(
            startTime,
            startDate,
            endTime,
            endDate
        )
    }
}

//fun Period.clone() = Period.create(start.clone(), end.clone())