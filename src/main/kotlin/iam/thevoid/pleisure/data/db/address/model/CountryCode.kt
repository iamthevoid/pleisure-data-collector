package iam.thevoid.pleisure.data.db.address.model

import iam.thevoid.pleisure.data.db.address.Entity

data class CountryCode(
    override val id: Int,
    override val name : String
) : Entity