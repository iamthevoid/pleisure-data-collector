package iam.thevoid.pleisure.data.db.address.table

import org.jetbrains.exposed.dao.IntIdTable

object Metros : IntIdTable("metro") {
    var name = text("name")
    var lineName = text("line_name").nullable()
    var cityId = integer("city_id")
}