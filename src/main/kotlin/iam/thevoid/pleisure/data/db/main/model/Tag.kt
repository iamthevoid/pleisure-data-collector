package iam.thevoid.pleisure.data.db.main.model

class Tag private constructor(val name: String) {

    override fun toString(): String = name

    companion object {
        fun create(name: String = "") = Tag(name)
    }
}

//fun Tag.clone() = Tag.create(name)