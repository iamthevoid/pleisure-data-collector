package iam.thevoid.pleisure.data.db._manytomany.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlDO
import iam.thevoid.pleisure.data.db._manytomany.entity.EventSellerUrlDO
import iam.thevoid.pleisure.data.db._manytomany.table.EventSellerUrl

abstract class Event_SellerUrlBinding
    : MTMStorage<EventDO, SellerUrlDO, EventSellerUrlDO, EventSellerUrl>(EventSellerUrl, EventSellerUrlDO)