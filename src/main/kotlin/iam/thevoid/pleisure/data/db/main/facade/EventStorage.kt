package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.model.Event

abstract class EventStorage : Storage<Event, EventDO>()