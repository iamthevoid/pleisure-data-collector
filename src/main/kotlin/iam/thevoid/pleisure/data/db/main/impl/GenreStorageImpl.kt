package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db.main.dbmodel.GenreDO
import iam.thevoid.pleisure.data.db.main.facade.GenreStorage
import iam.thevoid.pleisure.data.db.main.model.Genre
import iam.thevoid.pleisure.data.db.main.table.Genres
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.select

internal object GenreStorageImpl : GenreStorage() {

    override val TABLES: List<Table>
        get() = listOf(Genres)

    override fun Transaction.getFromDb(item: Genre): GenreDO? =
        GenreDO.wrapRows(Genres.select { Genres.name eq item.name }).firstOrNull()

    override fun Transaction.createEntityInDb(item: Genre) =
        GenreDO.new { name = item.name }
}