package iam.thevoid.pleisure.data.db

import org.jetbrains.exposed.dao.IntIdTable

open class PleisureTable(name : String) : IntIdTable(name)