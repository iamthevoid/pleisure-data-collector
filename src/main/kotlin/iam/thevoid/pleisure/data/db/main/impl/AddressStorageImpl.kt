package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.db.main.dbmodel.AddressDO
import iam.thevoid.pleisure.data.db.main.facade.AddressStorage
import iam.thevoid.pleisure.data.db.main.model.Address
import iam.thevoid.pleisure.data.db.main.model.hash
import iam.thevoid.pleisure.data.db.main.table.Addresses
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.select

internal object AddressStorageImpl : AddressStorage() {

    override val TABLES: List<Table>
        get() = listOf(Addresses)

    override fun Transaction.getFromDb(item: Address): AddressDO? =
        AddressDO.wrapRows(Addresses.select { Addresses.hash eq item.hash() }).firstOrNull()

    override fun Transaction.createEntityInDb(item: Address): AddressDO =
        AddressDO.new {
            city = item.city
            country = item.country
            countryCode = item.countryCode
            raw = item.raw.safe()
            lat = item.lat
            lon = item.lon
            hash = item.hash()
        }
}