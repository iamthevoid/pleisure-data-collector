package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.PlaceDO
import iam.thevoid.pleisure.data.db.main.model.Place
import iam.thevoid.pleisure.data.db.main.model.SellerUrl

abstract class PlaceStorage : Storage<Place, PlaceDO>() {
    abstract fun place(url : SellerUrl) : Place?
}