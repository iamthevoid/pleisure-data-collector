package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db._manytomany.entity.PlaceSellerUrlDO
import iam.thevoid.pleisure.data.db._manytomany.table.PlaceSellerUrl
import iam.thevoid.pleisure.data.db.main.model.Place
import iam.thevoid.pleisure.data.db.main.table.Places
import iam.thevoid.pleisure.data.db.main.table.SellerUrls
import iam.thevoid.pleisure.data.db.util.mtmChildren
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class PlaceDO(id: EntityID<Int>) : IntEntity(id), DataModel<Place> {
    companion object : IntEntityClass<PlaceDO>(Places)

    var name by Places.name
    var description by Places.description
    var url by Places.url
    var address by AddressDO referencedOn Places.address

    var hash by Places.hash

    override fun extract(): Place =
        Place.create(name, description, url, address.extract()).apply {
            sellerUrl =
                mtmChildren(
                    PlaceSellerUrl, { placeId },
                    PlaceSellerUrlDO, { sellerUrlId },
                    SellerUrls,
                    SellerUrlDO
                )
        }
}