package iam.thevoid.pleisure.data.db._manytomany.impl

import iam.thevoid.pleisure.data.db._manytomany.entity.EventTagDO
import iam.thevoid.pleisure.data.db._manytomany.facade.Event_TagBinding
import iam.thevoid.pleisure.data.db._manytomany.table.EventTag
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.TagDO
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

internal object Event_TagBindingImpl : Event_TagBinding() {

    override val TABLES: List<Table>
        get() = listOf(EventTag)

    override val EventTag.identity1: Column<EntityID<Int>>
        get() = eventId

    override val EventTag.identity2: Column<EntityID<Int>>
        get() = tagId

    override fun createInDb(bind1: EventDO, bind2: TagDO): EventTagDO =
        EventTagDO.new {
            eventId = bind1
            tagId = bind2
        }
}