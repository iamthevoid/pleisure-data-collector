package iam.thevoid.pleisure.data.db._manytomany.entity

import iam.thevoid.pleisure.data.db._manytomany.table.EventImage
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.ImageDO
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class EventImageDO(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<EventImageDO>(EventImage)

    var eventId by EventDO referencedOn EventImage.eventId
    var imageId by ImageDO referencedOn EventImage.imageId
}