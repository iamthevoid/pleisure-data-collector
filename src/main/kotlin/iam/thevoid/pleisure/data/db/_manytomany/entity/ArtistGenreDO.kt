package iam.thevoid.pleisure.data.db._manytomany.entity

import iam.thevoid.pleisure.data.db.main.dbmodel.ArtistDO
import iam.thevoid.pleisure.data.db.main.dbmodel.GenreDO
import iam.thevoid.pleisure.data.db._manytomany.table.ArtistGenre
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class ArtistGenreDO(id : EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<ArtistGenreDO>(ArtistGenre)

    var artistId by ArtistDO referencedOn  ArtistGenre.artistId
    var genreId by GenreDO referencedOn  ArtistGenre.genreId

}