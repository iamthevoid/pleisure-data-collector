package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.NamedPleisureTable

object Genres : NamedPleisureTable("genres")