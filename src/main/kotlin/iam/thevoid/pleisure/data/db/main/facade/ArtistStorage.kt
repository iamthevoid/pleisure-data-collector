package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.ArtistDO
import iam.thevoid.pleisure.data.db.main.model.Artist

abstract class ArtistStorage : Storage<Artist, ArtistDO>()