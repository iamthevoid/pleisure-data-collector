package iam.thevoid.pleisure.data.db.address.table

import org.jetbrains.exposed.dao.IntIdTable

object CountryAssociations : IntIdTable("country_assoc") {
    var countryId = integer("country_id")
    var assocName = text("assoc_name")
}