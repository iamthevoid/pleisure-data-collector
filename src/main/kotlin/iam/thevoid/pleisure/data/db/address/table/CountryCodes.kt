package iam.thevoid.pleisure.data.db.address.table

import org.jetbrains.exposed.dao.IntIdTable

object CountryCodes : IntIdTable("country_code") {
    var name = text("name")
}