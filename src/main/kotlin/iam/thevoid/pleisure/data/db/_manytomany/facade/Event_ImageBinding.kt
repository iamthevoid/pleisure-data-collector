package iam.thevoid.pleisure.data.db._manytomany.facade

import iam.thevoid.pleisure.data.db._manytomany.entity.EventImageDO
import iam.thevoid.pleisure.data.db._manytomany.table.EventImage
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.ImageDO

abstract class Event_ImageBinding
    : MTMStorage<EventDO, ImageDO, EventImageDO, EventImage>(EventImage, EventImageDO)