package iam.thevoid.pleisure.data.db.address.facade

import iam.thevoid.pleisure.data.db.address.model.CountryCode
import iam.thevoid.pleisure.data.db.address.model.CountryCodeAssoc

interface CountryCodeStorage {

    fun countryCodes() : List<CountryCode>

    fun associations(code: CountryCode) : List<CountryCodeAssoc>

}