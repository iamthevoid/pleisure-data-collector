package iam.thevoid.pleisure.data.db.main.model

class SellerUrlType private constructor(val name: String) {

    companion object {
        private const val TYPE_UNKNOWN = "unknown"
        private const val TYPE_PLACE = "place"
        private const val TYPE_EVENT = "event"

        val EVENT by lazy {
            create(
                TYPE_EVENT
            )
        }

        val PLACE by lazy { create(TYPE_PLACE) }

        fun create(type: String = TYPE_UNKNOWN) = SellerUrlType(type)
    }
}

//fun SellerUrlType.clone() = SellerUrlType.create(type)