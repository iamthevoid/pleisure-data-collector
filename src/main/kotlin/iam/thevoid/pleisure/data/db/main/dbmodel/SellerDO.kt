package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.main.model.Seller
import iam.thevoid.pleisure.data.db.main.table.Sellers
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class SellerDO(id: EntityID<Int>) : IntEntity(id), DataModel<Seller> {

    companion object : IntEntityClass<SellerDO>(Sellers)

    var name by Sellers.name
    var url by Sellers.url
    var logoUrl by Sellers.logoUrl
    var eventUrlPattern by Sellers.eventUrlPattern
    var placeUrlPattern by Sellers.placeUrlPattern

    override fun extract(): Seller = Seller.create(url, name, logoUrl, placeUrlPattern, eventUrlPattern)
}