package iam.thevoid.pleisure.data.db.main.model

import iam.thevoid.pleisure.utils.ext.charactersOnly
import iam.thevoid.pleisure.utils.hash

class Place private constructor(
    var name: String,
    var description: String,
    var url: String,
    var sellerUrl: List<SellerUrl>,
    var address: Address?
) {
    companion object {
        fun create(
            name: String = "",
            description: String = "",
            url: String = "",
            address: Address = Address.createFromRaw(),
            sellerUrl: List<SellerUrl> = emptyList()
        ) = Place(name, description, url, sellerUrl, address)
    }

    val hash
        get() = "${name.charactersOnly().toLowerCase()}${address?.lat(2)}${address?.lon(2)}".hash()
}
