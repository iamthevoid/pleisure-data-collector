package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.main.model.SellerUrlType
import iam.thevoid.pleisure.data.db.main.table.SellerUrlTypes
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class SellerUrlTypeDO(id: EntityID<Int>) : IntEntity(id),
    DataModel<SellerUrlType> {

    companion object : IntEntityClass<SellerUrlTypeDO>(SellerUrlTypes)

    var type by SellerUrlTypes.name

    override fun extract(): SellerUrlType = SellerUrlType.create(type)
}