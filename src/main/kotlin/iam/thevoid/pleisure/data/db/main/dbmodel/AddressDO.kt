package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.main.model.Address
import iam.thevoid.pleisure.data.db.main.table.Addresses
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class AddressDO(id: EntityID<Int>) : IntEntity(id),
    DataModel<Address> {
    companion object : IntEntityClass<AddressDO>(Addresses)

    var city: String? by Addresses.city
    var country: String?  by Addresses.country
    var countryCode: String?  by Addresses.countryCode
    var raw: String  by Addresses.raw
    var lat: Float?  by Addresses.lat
    var lon: Float?  by Addresses.lon

    var hash: String  by Addresses.hash

    override fun extract(): Address = Address.create(city, country, countryCode, raw, lat, lon)
}