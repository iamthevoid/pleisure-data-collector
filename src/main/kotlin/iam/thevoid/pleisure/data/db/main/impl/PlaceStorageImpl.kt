package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db._manytomany.MTMDataStorage
import iam.thevoid.pleisure.data.db._manytomany.table.PlaceSellerUrl
import iam.thevoid.pleisure.data.db.main.DataStorage
import iam.thevoid.pleisure.data.db.main.dbmodel.PlaceDO
import iam.thevoid.pleisure.data.db.main.facade.PlaceStorage
import iam.thevoid.pleisure.data.db.main.model.Place
import iam.thevoid.pleisure.data.db.main.model.SellerUrl
import iam.thevoid.pleisure.data.db.main.table.Places
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.select

internal object PlaceStorageImpl : PlaceStorage() {

    override val TABLES: List<Table>
        get() = listOf(Places, PlaceSellerUrl)

    override fun Transaction.getFromDb(item: Place): PlaceDO? =
        PlaceDO.wrapRows(Places.select { Places.hash eq item.hash }).firstOrNull()

    override fun place(url: SellerUrl): Place? =
        loggedTransaction { MTMDataStorage.place_sellerUrl.getBy2(DataStorage.sellerUrls.createIfNotExistsInDb(url))
            .map { it.placeId }.firstOrNull()?.extract() }

    override fun Transaction.createEntityInDb(item: Place) =
        PlaceDO.new {
            hash = item.hash
            name = item.name
            description = item.description
            url = item.url
            address = DataStorage.addresses.createIfNotExistsInDb(item.address!!)
        }

    override fun Transaction.bindEntityWithContents(item: Place, entity: PlaceDO) {
        item.sellerUrl.forEach {
            MTMDataStorage.place_sellerUrl.createIfNotExistsInDb(
                entity,
                DataStorage.sellerUrls.createIfNotExistsInDb(it)
            )
        }
    }
}