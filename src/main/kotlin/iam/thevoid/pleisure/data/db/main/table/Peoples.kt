package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.NamedPleisureTable

object Peoples : NamedPleisureTable("people_") {
    val role = text("role").nullable()
    val birthdate = text("birthdate").nullable()
    val description = text("description").nullable()
    val image = optReference("image", Images)
}