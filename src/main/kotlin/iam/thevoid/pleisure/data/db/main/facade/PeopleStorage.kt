package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.PeopleDO
import iam.thevoid.pleisure.data.db.main.model.People

abstract class PeopleStorage : Storage<People, PeopleDO>()