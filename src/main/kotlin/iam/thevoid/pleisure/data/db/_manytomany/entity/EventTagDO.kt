package iam.thevoid.pleisure.data.db._manytomany.entity

import iam.thevoid.pleisure.data.db._manytomany.table.EventTag
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.TagDO
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class EventTagDO(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<EventTagDO>(EventTag)

    var eventId by EventDO referencedOn EventTag.eventId
    var tagId by TagDO referencedOn EventTag.tagId

}
