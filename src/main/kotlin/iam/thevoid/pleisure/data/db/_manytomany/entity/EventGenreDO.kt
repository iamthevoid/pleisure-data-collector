package iam.thevoid.pleisure.data.db._manytomany.entity

import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.GenreDO
import iam.thevoid.pleisure.data.db._manytomany.table.EventGenre
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class EventGenreDO(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<EventGenreDO>(EventGenre)

    var eventId by EventDO referencedOn EventGenre.eventId
    var genreId by GenreDO referencedOn EventGenre.genreId
}