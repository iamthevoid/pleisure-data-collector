package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.GenreDO
import iam.thevoid.pleisure.data.db.main.model.Genre

abstract class GenreStorage : Storage<Genre, GenreDO>()