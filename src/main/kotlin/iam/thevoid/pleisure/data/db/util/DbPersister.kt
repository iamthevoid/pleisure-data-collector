package iam.thevoid.pleisure.data.db.util

import iam.thevoid.pleisure.data.db.DataModel
import iam.thevoid.pleisure.utils.ext.toJson
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.select

fun <T : IntEntity> List<T>.toIdsArray() = map { it.id.value }.toJson()

fun <Parent, Child, Mapper, MapperDOC, MapperDO, ChildTable, ChildDOC, ChildDO> Parent.mtmChildren(
    mapper: Mapper,
    toParentIdMapping: Mapper.() -> Column<EntityID<Int>>,
    mapperDo: MapperDOC,
    toChildReferenceMapping: MapperDO.() -> ChildDO,
    childrenTable: ChildTable,
    childrenDo: ChildDOC
): List<Child>
        where
        Mapper : Table,
        Parent : IntEntity,
        MapperDOC : IntEntityClass<MapperDO>,
        MapperDO : IntEntity,
        ChildTable : IntIdTable,
        ChildDOC : IntEntityClass<ChildDO>,
        ChildDO : IntEntity,
        ChildDO : DataModel<Child> =
    mapper.select { mapper.toParentIdMapping() eq id }
        .map { mapperDo.wrapRow(it).toChildReferenceMapping() }
        .map { cId -> childrenTable.select { childrenTable.id eq cId.id } }
        .flatten()
        .map { childrenDo.wrapRow(it).extract() }

fun <ParentRelation, ChildEntity, ChildModel, ChildTable, ChildWrapper> ParentRelation.mtoChildren(
    table: ChildTable,
    wrapper: ChildWrapper
): List<ChildModel>
        where ParentRelation : IntEntity,
              ChildEntity : DataModel<ChildModel>,
              ChildWrapper : IntEntityClass<ChildEntity>,
              ChildTable : IntIdTable =
    wrapper.wrapRows(table.select { table.id eq id }).map { it.extract() }