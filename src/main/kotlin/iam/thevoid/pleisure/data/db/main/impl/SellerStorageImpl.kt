package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db.main.dbmodel.SellerDO
import iam.thevoid.pleisure.data.db.main.facade.SellerStorage
import iam.thevoid.pleisure.data.db.main.model.Seller
import iam.thevoid.pleisure.data.db.main.table.Sellers
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.select

internal object SellerStorageImpl : SellerStorage() {

    override val TABLES: List<Table>
        get() = listOf(Sellers)

    override fun Transaction.getFromDb(item: Seller): SellerDO? =
        SellerDO.wrapRows(Sellers.select { Sellers.url eq item.url }).firstOrNull()

    override fun Transaction.createEntityInDb(item: Seller) = SellerDO.new {
        name = item.name
        logoUrl = item.logoUrl
        url = item.url
        eventUrlPattern = item.eventUrlPattern
        placeUrlPattern = item.placeUrlPattern
    }

    override fun seller(url: String): Seller? =
        loggedTransaction {
            SellerDO.wrapRows(Sellers.select { Sellers.url eq url }.limit(1))
                .firstOrNull()?.extract()
        }
}