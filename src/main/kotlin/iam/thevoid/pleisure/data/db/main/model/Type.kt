package iam.thevoid.pleisure.data.db.main.model

class Type private constructor(var name: String) {

    override fun toString(): String = name

    companion object {

        val UNKNOWN = create("unknown")

        fun create(name: String = "") = Type(name)
    }
}

//fun Type.clone() = Type.create(name, genres.map { it.clone() })