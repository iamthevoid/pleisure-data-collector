package iam.thevoid.pleisure.data.db.address.table

import org.jetbrains.exposed.dao.IntIdTable

object Cities : IntIdTable("city") {
    var name = text("fl_name")
    var nameEn = text("en_name")
}