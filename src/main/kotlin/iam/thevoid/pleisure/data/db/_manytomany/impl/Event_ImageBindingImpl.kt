package iam.thevoid.pleisure.data.db._manytomany.impl

import iam.thevoid.pleisure.data.db._manytomany.entity.EventImageDO
import iam.thevoid.pleisure.data.db._manytomany.facade.Event_ImageBinding
import iam.thevoid.pleisure.data.db._manytomany.table.EventImage
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.ImageDO
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

internal object Event_ImageBindingImpl : Event_ImageBinding() {

    override val TABLES: List<Table>
        get() = listOf(EventImage)

    override val EventImage.identity1: Column<EntityID<Int>>
        get() = eventId

    override val EventImage.identity2: Column<EntityID<Int>>
        get() = imageId

    override fun createInDb(bind1: EventDO, bind2: ImageDO): EventImageDO =
        EventImageDO.new {
            eventId = bind1
            imageId = bind2
        }
}