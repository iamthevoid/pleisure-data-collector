package iam.thevoid.pleisure.data.db._manytomany.impl

import iam.thevoid.pleisure.data.db._manytomany.entity.EventSellerUrlDO
import iam.thevoid.pleisure.data.db._manytomany.facade.Event_SellerUrlBinding
import iam.thevoid.pleisure.data.db._manytomany.table.EventSellerUrl
import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlDO
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

internal object Event_SellerUrlBindingImpl : Event_SellerUrlBinding() {

    override val TABLES: List<Table>
        get() = listOf(EventSellerUrl)

    override val EventSellerUrl.identity1: Column<EntityID<Int>>
        get() = eventId

    override val EventSellerUrl.identity2: Column<EntityID<Int>>
        get() = sellerUrlId

    override fun createInDb(bind1: EventDO, bind2: SellerUrlDO): EventSellerUrlDO =
        EventSellerUrlDO.new {
            eventId = bind1
            sellerUrlId = bind2
        }
}