package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlDO
import iam.thevoid.pleisure.data.db.main.model.Seller
import iam.thevoid.pleisure.data.db.main.model.SellerUrl
import iam.thevoid.pleisure.data.db.main.model.SellerUrlType

abstract class SellerUrlStorage : Storage<SellerUrl, SellerUrlDO>() {

    abstract fun sellerUrls(seller : Seller, type : SellerUrlType) : List<SellerUrl>

}