package iam.thevoid.pleisure.data.db.main.dbmodel

import iam.thevoid.pleisure.data.db.main.model.Genre
import iam.thevoid.pleisure.data.db.main.table.Genres
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class GenreDO(id : EntityID<Int>) : IntEntity(id), DataModel<Genre> {

    var name by Genres.name

    companion object : IntEntityClass<GenreDO>(Genres)

    override fun extract(): Genre = Genre.create(name)
}