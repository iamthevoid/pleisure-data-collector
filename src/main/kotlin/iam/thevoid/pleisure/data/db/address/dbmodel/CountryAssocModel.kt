package iam.thevoid.pleisure.data.db.address.dbmodel

import iam.thevoid.pleisure.data.db.address.model.CountryAssoc
import iam.thevoid.pleisure.data.db.address.table.CountryAssociations
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class CountryAssocModel(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<CountryAssocModel>(CountryAssociations)

    val name by CountryAssociations.assocName
    val cityId by CountryAssociations.countryId

    fun toCountryAssoc() = CountryAssoc(name)
}