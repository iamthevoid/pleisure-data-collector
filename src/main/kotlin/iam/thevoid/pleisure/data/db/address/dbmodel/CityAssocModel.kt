package iam.thevoid.pleisure.data.db.address.dbmodel

import iam.thevoid.pleisure.data.db.address.model.CityAssoc
import iam.thevoid.pleisure.data.db.address.table.CityAssociations
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class CityAssocModel(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<CityAssocModel>(CityAssociations)

    val name by CityAssociations.assocName
    val cityId by CityAssociations.cityId

    fun toCityAssoc() = CityAssoc(name)
}