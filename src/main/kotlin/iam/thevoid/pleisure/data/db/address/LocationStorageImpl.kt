package iam.thevoid.pleisure.data.db.address

import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.db.DB
import iam.thevoid.pleisure.data.db.address.dbmodel.*
import iam.thevoid.pleisure.data.db.address.facade.CityStorage
import iam.thevoid.pleisure.data.db.address.facade.CountryCodeStorage
import iam.thevoid.pleisure.data.db.address.facade.CountryStorage
import iam.thevoid.pleisure.data.db.address.facade.MetroStorage
import iam.thevoid.pleisure.data.db.address.model.*
import iam.thevoid.pleisure.data.db.address.table.*
import iam.thevoid.pleisure.utils.ext.cast
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import java.util.*

internal object LocationStorageImpl : DB(), MetroStorage,
    CityStorage, CountryStorage, CountryCodeStorage {

    private const val CITY = "city"
    private const val COUNTRY = "country"
    private const val CODE = "code"
    private const val METRO = "metro"

    private val cache by lazy { WeakHashMap<Any, MutableList<Any>>() }

    private fun <T, V> cache(key: T, values: List<V>) {
        val list = cache[key]
        if (list.isNullOrEmpty())
            cache[key] = values.cast<Any>().toMutableList()
        else
            list.addAll(values.cast())
    }

    override val TABLES: List<Table>
        get() = listOf(Cities, CityAssociations, Metros, MetroAssociations)

    override fun metro(): List<Metro> =
        cache[METRO]?.cast<Metro>()?.takeIf { it.isNotEmpty() }
            ?: loggedTransaction { MetroModel.wrapRows(Metros.selectAll()).map { it.toMetro() } }
                .also { cache(METRO, it) }

    override fun metro(city: City): List<Metro> =
        cache[METRO]?.cast<Metro>()?.filter { it.cityId == city.id }?.takeIf { it.isNotEmpty() }
            ?: loggedTransaction {
                MetroModel.wrapRows(Metros.select { Metros.cityId eq city.id }).map { it.toMetro() }
            }
                .also { cache(METRO, it) }

    override fun associations(metro: Metro): List<MetroAssoc> =
        cache[metro]?.cast() ?: loggedTransaction {
            MetroModel.wrapRows(Metros.select { Metros.name eq metro.name }).takeIf { !it.empty() }?.first()?.let {
                MetroAssocModel.wrapRows(MetroAssociations.select { MetroAssociations.metroId eq it.id.value })
                    .map { it.toMetroAssoc() }
            }.safe()
        }.also { cache(metro, it) }


    override fun cities(): List<City> =
        cache[CITY]?.cast<City>()?.takeIf { it.isNotEmpty() }
            ?: loggedTransaction { CityModel.wrapRows(Cities.selectAll()).map { it.toCity() } }
                .also { cache(CITY, it) }

    override fun associations(city: City): List<CityAssoc> =
        cache[city]?.cast() ?: loggedTransaction {
            CityModel.wrapRows(Cities.select { Cities.name eq city.name }).takeIf { !it.empty() }?.first()?.let {
                CityAssocModel.wrapRows(CityAssociations.select { CityAssociations.cityId eq it.id.value })
                    .map { it.toCityAssoc() }
            }.safe()
        }.also { cache(city, it) }


    override fun countries(): List<Country> =
        cache[COUNTRY]?.cast() ?: loggedTransaction {
            CountryModel.wrapRows(Countries.selectAll()).map { it.toCountry() }
        }.also { cache(COUNTRY, it) }

    override fun associations(country: Country): List<CountryAssoc> =
        cache[country]?.cast() ?: loggedTransaction {
            CountryModel.wrapRows(Countries.select { Countries.name eq country.name }).takeIf { !it.empty() }?.first()
                ?.let {
                    CountryAssocModel.wrapRows(CountryAssociations.select { CountryAssociations.countryId eq it.id.value })
                        .map { it.toCountryAssoc() }
                }.safe()
        }.also { cache(country, it) }



    override fun countryCodes(): List<CountryCode> =
        cache[CODE]?.cast() ?: loggedTransaction {
            CountryCodeModel.wrapRows(CountryCodes.selectAll()).map { it.toCountryCode() }
        }.also { cache(CODE, it) }

    override fun associations(code: CountryCode): List<CountryCodeAssoc> =
        cache[code]?.cast() ?: loggedTransaction {
            CountryCodeModel.wrapRows(CountryCodes.select { CountryCodes.name eq code.name }).takeIf { !it.empty() }?.first()
                ?.let {
                    CountryCodeAssocModel.wrapRows(CountryCodeAssociations.select { CountryCodeAssociations.countryCodeId eq it.id.value })
                        .map { it.toCountryCodeAssoc() }
                }.safe()
        }.also { cache(code, it) }

}