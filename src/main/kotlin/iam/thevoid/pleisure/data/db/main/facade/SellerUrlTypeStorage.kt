package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlTypeDO
import iam.thevoid.pleisure.data.db.main.model.SellerUrlType

abstract class SellerUrlTypeStorage : Storage<SellerUrlType, SellerUrlTypeDO>()