package iam.thevoid.pleisure.data.db.address.model

import iam.thevoid.pleisure.data.db.address.Assoc

data class CountryAssoc(override val value : String) : Assoc