package iam.thevoid.pleisure.data.db.address.dbmodel

import iam.thevoid.pleisure.data.db.address.model.Country
import iam.thevoid.pleisure.data.db.address.table.Countries
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class CountryModel(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<CountryModel>(Countries)

    val name by Countries.name
    val nameEn by Countries.nameEn

    fun toCountry() = Country(id.value, name, nameEn)
}