package iam.thevoid.pleisure.data.db.main.facade

import iam.thevoid.pleisure.data.db.DB
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.sql.Transaction

abstract class Storage<Model, Entity : DataModel<Model>> : DB() {

    internal abstract fun Transaction.createEntityInDb(item: Model): Entity

    internal open fun Transaction.bindEntityWithContents(item: Model, entity: Entity) = Unit

    private fun Transaction.createInDb(item: Model): Entity =
        createEntityInDb(item).also { entity ->
            commit()
            bindEntityWithContents(item, entity)
        }

    fun createIfNotExistsInDb(item: Model) : Entity =
        loggedTransaction { getFromDb(item) ?: createInDb(item) }


    internal open fun Transaction.getFromDb(item: Model): Entity? =
        TODO("Not implemented for ${this::class.java.simpleName}")

    internal open fun Transaction.createOrUpdateInDb(item: Model): Entity =
        TODO("Not implemented for ${this::class.java.simpleName}")

    fun createIfNotExists(item: Model): Model = loggedTransaction { createIfNotExistsInDb(item).extract() }

    fun createIfNotExists(list: List<Model>): List<Model> =
        loggedTransaction { list.map { createIfNotExistsInDb(it).extract() } }

    fun createOrUpdate(item: Model): Model = loggedTransaction { createOrUpdateInDb(item).extract() }

    fun createOrUpdate(list: List<Model>): List<Model> =
        loggedTransaction { list.map { createOrUpdateInDb(it).extract() } }

    fun create(item: Model): Model = loggedTransaction { createInDb(item).extract() }

    fun create(list: List<Model>): List<Model> = loggedTransaction { list.map { createInDb(it).extract() } }

}