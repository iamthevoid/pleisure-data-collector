package iam.thevoid.pleisure.data.db.address.facade

import iam.thevoid.pleisure.data.db.address.model.Country
import iam.thevoid.pleisure.data.db.address.model.CountryAssoc

interface CountryStorage {

    fun countries() : List<Country>

    fun associations(country: Country) : List<CountryAssoc>

}