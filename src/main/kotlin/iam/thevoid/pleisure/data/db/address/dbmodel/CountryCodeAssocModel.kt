package iam.thevoid.pleisure.data.db.address.dbmodel

import iam.thevoid.pleisure.data.db.address.model.CountryCodeAssoc
import iam.thevoid.pleisure.data.db.address.table.CountryCodeAssociations
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class CountryCodeAssocModel(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<CountryCodeAssocModel>(CountryCodeAssociations)

    val name by CountryCodeAssociations.assocName
    val cityId by CountryCodeAssociations.countryCodeId

    fun toCountryCodeAssoc() = CountryCodeAssoc(name)
}