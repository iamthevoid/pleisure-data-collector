package iam.thevoid.pleisure.data.db.main.table

import iam.thevoid.pleisure.data.db.NamedPleisureTable

// Has sources ( producers ) and pictures referenced on producers
// + producer url

object Events : NamedPleisureTable("events") {

    val description = text("description").nullable()
    val type = reference("type", Types)
    val artist = optReference("artist", Artists)
    val place = reference("place", Places)

}