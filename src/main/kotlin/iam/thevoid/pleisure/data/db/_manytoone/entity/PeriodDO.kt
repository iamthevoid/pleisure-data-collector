package iam.thevoid.pleisure.data.db._manytoone.entity

import iam.thevoid.pleisure.data.db.main.dbmodel.EventDO
import iam.thevoid.pleisure.data.db._manytoone.model.Period
import iam.thevoid.pleisure.data.db._manytoone.table.Periods
import iam.thevoid.pleisure.data.db.DataModel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class PeriodDO(id: EntityID<Int>) : IntEntity(id), DataModel<Period> {
    override fun extract(): Period = Period.create(startTime, startDate, endTime, endDate)

    companion object : IntEntityClass<PeriodDO>(Periods)

    var eventId by EventDO referencedOn Periods.eventId
    var startTime by Periods.startTime
    var startDate by Periods.startDate
    var endTime by Periods.endTime
    var endDate by Periods.endDate
}