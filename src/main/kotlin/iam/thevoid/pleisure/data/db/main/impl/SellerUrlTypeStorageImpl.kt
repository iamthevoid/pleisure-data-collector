package iam.thevoid.pleisure.data.db.main.impl

import iam.thevoid.pleisure.data.db.main.dbmodel.SellerUrlTypeDO
import iam.thevoid.pleisure.data.db.main.facade.SellerUrlTypeStorage
import iam.thevoid.pleisure.data.db.main.model.SellerUrlType
import iam.thevoid.pleisure.data.db.main.table.SellerUrlTypes
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.select

internal object SellerUrlTypeStorageImpl : SellerUrlTypeStorage() {

    override val TABLES: List<Table>
        get() = listOf(SellerUrlTypes)

    override fun Transaction.createEntityInDb(item: SellerUrlType): SellerUrlTypeDO =
        SellerUrlTypeDO.new { type = item.name }

    override fun Transaction.getFromDb(item: SellerUrlType): SellerUrlTypeDO? =
        SellerUrlTypeDO
            .wrapRows(SellerUrlTypes.select { SellerUrlTypes.name eq item.name }.limit(1))
            .firstOrNull()
}