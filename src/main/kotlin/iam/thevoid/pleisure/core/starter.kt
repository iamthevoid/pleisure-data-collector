package iam.thevoid.pleisure.core

fun main() {
    init()
    run()
}

private fun init() {

}

private fun run() {
    Daemon.run()
//    Daemon.run().blockingAwait()
}
