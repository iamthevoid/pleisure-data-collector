package iam.thevoid.pleisure.core

import iam.thevoid.e.forEachApply
import iam.thevoid.pleisure.data.api.seller.Distributor
import iam.thevoid.pleisure.data.api.seller.concertru.ConcertRu
import iam.thevoid.pleisure.data.db.main.DataStorage
import iam.thevoid.pleisure.data.db.main.model.*
import iam.thevoid.pleisure.data.file.FileHandler
import iam.thevoid.pleisure.utils.expandError
import kotlinx.coroutines.runBlocking

object Daemon {

    private val distributors by lazy {
        listOf(
//            RedKassa//,
            ConcertRu
        )
    }

    fun run() {
        runBlocking {
            distributors.forEachApply {
                retrieveSeller()?.let { seller ->
                    retrieveSellerUrlsForPlaces(seller)?.forEach { sellerUrl ->
                        retrievePlace(seller, sellerUrl)?.also { place ->
                            safe {
                                events(seller, place).also { events ->
                                    saveEvents(events)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun saveEvents(event: List<Event>) {
        DataStorage { events.createIfNotExists(event) }
    }

    private suspend fun Distributor.retrieveSeller(): Seller? =
        safe {
            DataStorage {
                sellers.run {
                    seller(endpoint) ?: seller().also { create(it) }
                }
            }
        }

    private suspend fun Distributor.retrieveSellerUrlsForPlaces(seller: Seller): List<SellerUrl>? =
        safe {
            DataStorage {
                sellerUrlTypes.createIfNotExists(SellerUrlType.PLACE)
                    .let { type -> sellerUrlsForPlaces(seller, type).also { sellerUrls.createIfNotExists(it) } }
            }
        }

    private suspend fun Distributor.retrievePlace(seller: Seller, sellerUrl: SellerUrl): Place? =
        safe {
            DataStorage { places.place(sellerUrl) ?: place(seller, sellerUrl)?.also { places.create(it) } }
        }

    private fun Distributor.reportDaemonError(throwable: Throwable) =
        FileHandler.writeToFile("log/Error_${this::class.java.simpleName}.txt", expandError(throwable))
            .also { throwable.printStackTrace() }

    private suspend fun <T> Distributor.safe(block: suspend () -> T) : T? =
        try {
            block()
        } catch (e: Exception) {
            reportDaemonError(e)
            null
        }
}