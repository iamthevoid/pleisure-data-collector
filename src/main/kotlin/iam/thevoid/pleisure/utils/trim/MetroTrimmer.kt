package iam.thevoid.pleisure.utils.trim

import iam.thevoid.e.firstOrNull
import iam.thevoid.e.remove
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.db.address.LocationStorage
import iam.thevoid.pleisure.utils.ext.containsContentIgnoreCase
import iam.thevoid.pleisure.utils.ext.contentEqualsIgnoreCase
import iam.thevoid.pleisure.utils.ext.removeAtStart
import iam.thevoid.pleisure.utils.ext.takeAtStart

object MetroTrimmer {

    private const val METRO_PREF_SURROUNDED_BY_TRASH =
        "((\\p{L}|\\s)*с(т(анц\\p{L}+)*)*[^\\p{L}\\p{N}]+)*м(етро)*[^\\p{L}]"
    private const val NON_ADDRESS = "[^\\p{L}\\p{N}\\p{Pi}\\p{Pf}\"\']"
    private const val ADDRESS_SEPARATOR = "([,;\\s\'\"\\p{Pi}\\p{Pf}]|(или))*"
    private const val ADDRESS_PART = "([\\p{L}|\\s|-]|\\.)+"
    private const val ADDRESS_PART_SPLITTERS = "(\\s|\\p{Pd}|\\.)"
    private const val PURE_WORD = "\\p{L}+"

    val metro by lazy { LocationStorage.metro.metro() }

    private val shortens = listOf(
        listOf("пр.", "просп.", "пр-т.", "проспект"),
        listOf("им.", "имени"),
        listOf("ул.", "улица")
    )

    fun removeMetroStation(raw: String): String {
        var prefix = raw.firstOrNull(METRO_PREF_SURROUNDED_BY_TRASH, setOf(RegexOption.IGNORE_CASE))
            ?: return raw

        (if (!raw.startsWith(prefix)) {
            raw.firstOrNull("$NON_ADDRESS+$METRO_PREF_SURROUNDED_BY_TRASH", setOf(RegexOption.IGNORE_CASE))
                .also { prefix = it.safe() }
                .let { real -> if (real == null) -1 else raw.indexOf(real) }
        } else 0).takeIf { it >= 0 } ?: return raw

        val parts = raw.split(prefix).takeIf { it.size >= 2 } ?: return raw

        val replacement = buildString {
            parts.takeLast(parts.size - 1).forEach { part ->
                var findIn = part
                append(prefix)
                loop@ while (true) {
                    val spacing = partStartSpacing(findIn)
                    val word = partWord(findIn, spacing)
                    val partsOfWord = partWordParts(word)
                    when {
                        word == null -> {
                            append(spacing)
                            break@loop
                        }
                        strongOverlap(word) or (partsOfWord.size > 1 && shortenOverlap(partsOfWord) != null) -> {
                            append("$spacing$word")
                            findIn = findIn.remove("$spacing$word")
                        }
                        weakOverlap(word) -> {
                            append("$spacing${extractFromWeak(word)}")
                            break@loop
                        }
                        else -> {
                            append(spacing)
                            break@loop
                        }
                    }
                }
            }
        }

        val res = if (replacement.trim() != prefix.trim()) raw.remove(replacement) else raw

        return res.trim()
            .run { if (endsWith(".")) substring(0, length - 1) else this }
            .removeAtStart("$NON_ADDRESS*").trim()
    }

    private fun partStartSpacing(findIn: String) =
        findIn.takeAtStart(ADDRESS_SEPARATOR).safe()
            .let { "$it${findIn.remove(it).takeAtStart(METRO_PREF_SURROUNDED_BY_TRASH).safe()}" }

    private fun partWord(findIn: String, spacing: String): String? {
        val full = findIn.removeAtStart(spacing).takeAtStart(ADDRESS_PART).safe()
        return if (full.contains(".")) {
            val isConcern = shortens.any { it.any { shorten -> full.contains(shorten) } }
            if (isConcern) full else full.split(".").first()
        } else full
    }

    private fun extractFromWeak(word: String): String {
        val parts = word.safe().split(Regex(ADDRESS_PART_SPLITTERS)).mapNotNull { it.firstOrNull(PURE_WORD) }.filter { it.isNotEmpty() }.toMutableList()
        val station = (findInKnown { word.containsContentIgnoreCase(it) }
            ?: shortenOverlap(parts))?.name.safe()

        val result = mutableListOf<String>()
        val stationParts = station.split(" ").filter { it.isNotEmpty() }
        for (i in stationParts.indices) {
            val sp = stationParts[i]
            if (sp == parts[i] || shortens.find { it.contains(sp) }.safe().contains(parts[i]))
                result.add(parts[i])
        }

        var stop: String? = null
        for (part in parts) {
            if (result.contains(part))
                continue
            if (shortens.find { it.any { shorten -> shorten.containsContentIgnoreCase(part) } } != null) {
                stop = part
                break
            }
        }

        return if (stop == null) word else word.split(stop).first()
    }

    private fun partWordParts(word: String?) =
        word.safe().split(" ").mapNotNull { it.firstOrNull(PURE_WORD) }.filter { it.isNotEmpty() }

    private fun shortenOverlap(parts: List<String>) = findInKnown { station ->
        val partsOfOriginal = station.split(" ").mapNotNull { it.firstOrNull(PURE_WORD) }

        var overlapCount = 0
        for (part in parts) {
            if (!partsOfOriginal.any { it.contentEqualsIgnoreCase(part) }) {
                val shorten = shortens.find { it.any { shorten -> shorten.firstOrNull(PURE_WORD).safe().contains(part) } }.safe()
                if (shorten.any { partsOfOriginal.any { original -> it.firstOrNull(PURE_WORD).safe().contentEqualsIgnoreCase(original) } })
                    overlapCount++
            } else {
                overlapCount++
            }
        }

        partsOfOriginal.size == parts.size && partsOfOriginal.size - overlapCount == 0
    }

    private fun strongOverlap(word: String) = anyInKnown { it.contentEqualsIgnoreCase(word) }

    private fun weakOverlap(word: String) =
        anyInKnown { word.containsContentIgnoreCase(it) && word.firstOrNull("$it$NON_ADDRESS") != null }

    private fun anyInKnown(predicate: (String) -> Boolean) =
        metro.any { metro -> predicate(metro.name) || LocationStorage.metro.associations(metro).any { predicate(it.value) } }

    private fun findInKnown(predicate: (String) -> Boolean) =
        metro.find { metro -> predicate(metro.name) || LocationStorage.metro.associations(metro).any { predicate(it.value) } }
}