package iam.thevoid.pleisure.utils

import iam.thevoid.e.safe
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

abstract class HtmlParser {

    companion object {
        const val A = "a"
        const val DIV = "div"
        const val HREF = "href"
        const val IMG = "img"
        const val H1 = "h1"
        const val H2 = "h2"
        const val H4 = "h4"
        const val LI = "li"
        const val META = "meta"
        const val P = "p"
        const val SCRIPT = "script"
        const val SOURCE = "source"
        const val SPAN = "span"
        const val SRC = "src"
        const val STYLE = "style"
        const val VALUE = "value"
        const val TR = "tr"
        const val UL = "ul"
    }

    fun attr(element: Element, attrKey: String, query: String = ""): String {
        val workElement = if (query.isBlank()) element.allElements else element.select(query)
        val attr = workElement.attr(attrKey)
        if (attr.isEmpty()) {
            logEmptyAttr(attrKey, query)
            Log.d("E: Input element")
            element.log()
            Log.d("E: Found element")
            workElement.forEach { it.log() }
        }

        return attr
    }

    fun attr(elements: Elements, attrKey: String, query: String = ""): String {
        val workElement = if (query.isBlank()) elements else elements.select(query)
        val attr = workElement.attr(attrKey)
        if (attr.isEmpty()) {
            logEmptyAttr(attrKey, query)
            Log.d("E: Input element")
            elements.forEach { it.log() }
            Log.d("E: Found element")
            workElement.forEach { it.log() }
        }

        return attr
    }

    fun Element?.ownText(query: String = ""): String {
        this ?: return ""
        val workElement = if (query.isBlank()) this else select(query).firstOrNull()
        val selected = workElement?.ownText().safe()
        if (selected.isEmpty()) {
            logEmptyOwnText(query)
            Log.d("E: Input element")
            log()
            Log.d("E: Found element")
            workElement.log()
        }

        return selected
    }

    fun Elements.ownText(query: String = ""): List<String> {
        val workElements = if (query.isBlank()) this else select(query)
        val selected = workElements.map { it.text() }.filter { it.isNotBlank() }
        if (selected.isEmpty()) {
            logEmptyOwnText(query)
            Log.d("E: Input element")
            forEach { it.log() }
            Log.d("E: Found element")
            workElements.forEach { it.log() }
        }

        return selected
    }

    fun ownText(document: Document, query: String = ""): String {
        val workElements = if (query.isBlank()) document.allElements else document.select(query)
        val selected = workElements.firstOrNull()?.text().safe()
        if (selected.isEmpty()) {
            logEmptyOwnText(query)
            Log.d("E: Input element")
            document.log()
            Log.d("E: Found element")
            workElements.forEach { it.log() }
        }

        return selected
    }

    fun html(elements: Elements, query: String = ""): String {
        val workElements = if (query.isBlank()) elements else elements.select(query)
        val selected = workElements.html()
        if (selected.isEmpty()) {
            logEmptyHtml(query)
            Log.d("E: Input element")
            elements.forEach { it.log() }
            Log.d("E: Found element")
            workElements.forEach { it.log() }
        }

        return selected
    }

    fun html(document: Document, query: String = ""): String {
        val workElements = if (query.isBlank()) document.allElements else document.select(query)
        val selected = workElements.html()
        if (selected.isEmpty()) {
            logEmptyHtml(query)
            Log.d("E: Input element")
            document.log()
            Log.d("E: Found element")
            workElements.forEach { it.log() }
        }

        return selected
    }

    private fun logEmptyAttr(attr : String, query: String) =
        Log.d("Attr not found \"$attr\".${withQuery(query)}")

    private fun logEmptyHtml(query: String) =
        Log.d("Html not found.${withQuery(query)}")

    private fun logEmptyOwnText(query: String) =
        Log.d("Own text not found.${withQuery(query)}")

    private fun withQuery(query: String) =
        if (query.isNotBlank()) " With query \"$query\"" else ""

    private fun Element?.log() =
        Log.d("E: ${"$this".replace("\\s".toRegex(), "")}")

    class Exception(message : String) : kotlin.Exception(message)
}