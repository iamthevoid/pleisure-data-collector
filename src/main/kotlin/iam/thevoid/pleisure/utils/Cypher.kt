package iam.thevoid.pleisure.utils

import java.security.MessageDigest

fun String?.hash() = hash((this ?: "~null").toByteArray())

fun hash(bytes: ByteArray): String =
    MessageDigest
        .getInstance("SHA-256")
        .digest(bytes)
        .fold("") { str, it -> str + "%02x".format(it) }

