package iam.thevoid.pleisure.utils

import com.ibm.icu.text.Transliterator
import iam.thevoid.e.remove
import iam.thevoid.e.safe
import iam.thevoid.pleisure.utils.trim.MetroTrimmer
import java.util.*
import kotlin.reflect.full.declaredMemberProperties

const val REGEX_JSON = "(\\{.+})|(\\[.+])"
const val REGEX_NUM = "\\d+"
const val REGEX_SPACES = "\\s"
const val REGEX_SEPARATOR = "/"
const val REGEX_FILENAME = "(\\d|\\w)+\\.(\\d|\\w)+"

const val TRNLT_CYRILLIC_TO_LATIN = "Russian-Latin/BGN"

fun transliterate(string: String, vararg replacements: Pair<String, String>): String {
    var res = string
    for (pair in replacements) {
        res = res.replace(pair.first, pair.second)
    }
    return Transliterator.getInstance(TRNLT_CYRILLIC_TO_LATIN).transliterate(res)
}

val ampGenres by lazy {
    listOf(
        Pair(Regex("drum\\s*&\\s*bass", RegexOption.IGNORE_CASE), "Drum & Bass"),
        Pair(Regex("r\\s*&\\s*b", RegexOption.IGNORE_CASE), "R&B"),
        Pair(Regex("ritm\\s*&\\s*blues", RegexOption.IGNORE_CASE), "R&B")
    )
}

fun numberFromString(string: String?): Int? =
    REGEX_NUM.toRegex().find(string.safe(), 0)?.groupValues?.firstOrNull()?.toInt()

fun filenameFromString(string: String?): String =
    REGEX_FILENAME.toRegex().find(string.safe(), 0)?.groupValues?.firstOrNull().safe()

fun splitToTags(string: String?): List<String> =
    string?.split(REGEX_SEPARATOR)?.filter { it.isNotBlank() }?.map { it.trim() } ?: emptyList()

fun deleteSpaceSymbols(string: String) = string.remove(REGEX_SPACES)

fun removeBraceContainments(raw: String) =
    raw.remove(Regex("\\(.*\\)"))

fun removeMetroStation(raw: String) =
    MetroTrimmer.removeMetroStation(raw)

fun removeEnbankment(raw: String) =
    raw.remove(Regex("(\\s|\\p{L})*наб(ережная)*[^\\p{L}]+"))

fun removePavilion(raw: String) =
    raw.remove(Regex("[^(\\p{L}|\\d)]*пав(ильон)*[^\\p{L}]+"))

fun removeBuilding(raw: String) =
    raw.remove(Regex("[^(\\p{L}|\\d)]*стр(оение)*[^\\p{L}]+"))

fun prepareRawAddress(raw: String): String = raw
    .replace(Regex(" (\\p{P})"), "$1")
    .replace(Regex("([^\\p{L}]*)[Пп]росп[^(\\p{L})]+"), "$1проспект ")
    .trim()

//@Deprecated("Move redkassa to shared parser")
//fun parseDateOrNull(date: String) =
//    date.parseDateOrThrow(dateFormatRes)
//
//@Deprecated("Move redkassa to shared parser")
//fun parseTimeOrNull(time: String) =
//    time.parseDateOrThrow(timeFormatRes)

fun expandError(throwable: Throwable): String =
    with(StringBuilder()) {
        append("${Date()}\n")
        var cause: Throwable? = throwable
        while (cause != null) {
            append("$cause\n")
            for (line in cause.stackTrace) {
                append("\t$line\n")
            }
            cause = cause.cause
        }
        append("\n")
    }.toString()

fun formatTimePart(part : Int) : String {
    require(part < 60)
    return "$part".let { if (it.length == 1) "0$it" else it }
}