package iam.thevoid.pleisure.utils.ext

import com.google.gson.Gson
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

val gson by lazy { Gson() }

inline fun <reified T : Any> String.fromJson() : T? = try {
    gson.fromJson(this, T::class.java)
} catch (e: Exception) {
    null
}

inline fun <reified T : Any> String.listFromJson() : List<T> =
    if (isNullOrBlank()) emptyList() else gson.fromJson(this, ListOfJson(T::class.java))

fun Any.toJson() : String = gson.toJson(this)

class ListOfJson<T>(private val wrapped: Class<T>) : ParameterizedType {
    override fun getRawType(): Type = List::class.java
    override fun getActualTypeArguments(): Array<Type> = arrayOf(wrapped)
    override fun getOwnerType(): Type = wrapped
}