package iam.thevoid.pleisure.utils.ext

fun <K,V> Map<K,V>.find(predicate : (Pair<K,V>) -> Boolean) =
    filter { predicate(it.key to it.value) }.keys.firstOrNull()?.let { Pair(it, get(it)) }