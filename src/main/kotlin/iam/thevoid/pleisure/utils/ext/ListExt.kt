package iam.thevoid.pleisure.utils.ext

import iam.thevoid.pleisure.utils.Log

fun <T : Any> List<Any?>.cast(): List<T> = mapNotNull { it as? T }

fun <T> List<T>.removeDuplicates() = toSet().toList()

fun <T> List<T>.skipUntil(predicate : (T) -> Boolean) : ArrayList<T> {
    val list = arrayListOf<T>()
    var found = false
    for (item in this) {
        found = found || predicate(item)
        if (!found)
            continue
        list.add(item)
    }
    return list
}

inline fun <T, R : Any> Iterable<T>.mapNotError(mapper: (T) -> R?) =
    mapNotNull {
        try {
            mapper(it)
        } catch (e: Throwable) {
            Log.e(e)
            null
        }
    }

fun List<String>.containsContentIgnoreCase(string: String?): Boolean {
    if (string == null) return false
    forEach {
        if (it.contentEqualsIgnoreCase(string))
            return true
    }
    return false
}

fun <T, R> List<T>.findNotNull(mapper: (T) -> R?): R? {
    forEach {
        val toCheck = mapper(it)
        if (toCheck != null)
            return toCheck
    }

    return null
}

suspend fun <T, R> List<T>.findNotNullSuspended(mapper: suspend (T) -> R?): R? {
    forEach {
        val toCheck = mapper(it)
        if (toCheck != null)
            return toCheck
    }

    return null
}