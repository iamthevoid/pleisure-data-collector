package iam.thevoid.pleisure.utils.ext

import iam.thevoid.pleisure.utils.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun SimpleDateFormat.parseSafe(date : String, default : Date? = null) : Date? =
    try { parse(date) } catch (e : ParseException) { Log.e(e); default }