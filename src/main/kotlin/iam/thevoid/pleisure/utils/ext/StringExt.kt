package iam.thevoid.pleisure.utils.ext

import iam.thevoid.e.firstOrNull
import iam.thevoid.e.remove
import iam.thevoid.e.safe

fun String.containsAny(vararg containment: String): Boolean = containment.any { contains(Regex(it)) }

fun String.removeAtStart(pattern: String, vararg options: RegexOption) = removeAtStart(Regex(pattern, options.toSet()))

fun String.removeAtStart(regex: Regex) =
    firstOrNull(regex).safe().let { if (startsWith(it)) replaceFirst(it, "") else this }

fun String.takeAtStart(pattern: String, vararg options: RegexOption) = takeAtStart(Regex(pattern, options.toSet()))

fun String.takeAtStart(regex: Regex) =
    firstOrNull(regex)?.takeIf { startsWith(it) }

fun String.charactersOnly() =
    remove("(\\p{M})")
        .remove("(\\p{S})")
        .remove("(\\p{N})")
        .remove("(\\p{P})")
        .remove("(\\p{C})")
        .remove("(\\p{Z})")
        .trim()

fun String.containsContentIgnoreCase(another : String) =
    trim().toLowerCase().contains(another.trim().toLowerCase())

fun String.contentEqualsIgnoreCase(another : String) =
    trim().poor().equals(another.poor().trim(), true)

fun String.poor() = replace("ё", "е")