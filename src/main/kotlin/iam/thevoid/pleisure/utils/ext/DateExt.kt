package iam.thevoid.pleisure.utils.ext

import iam.thevoid.e.safeInt
import iam.thevoid.pleisure.utils.DateUtil
import java.util.*

fun Date?.formatTime(pattern : String = "HH:mm", locale: Locale = Locale.getDefault()) : String
        = DateUtil.dateFormat(pattern, locale).format(this ?: DateUtil.DEFAULT_DATE)

fun Date?.formatDate(pattern : String = "dd-MM-yyyy", locale: Locale = Locale.getDefault()) : String
        = DateUtil.dateFormat(pattern, locale).format(this ?: DateUtil.DEFAULT_DATE)

fun Date.timeImMinutes(): Int =
    formatTime().split(":").let {
        when {
            it.isEmpty() -> 0
            it.size == 1 -> it[0].safeInt()
            else -> it[0].safeInt() * 60 + it[1].safeInt()
        }
    }