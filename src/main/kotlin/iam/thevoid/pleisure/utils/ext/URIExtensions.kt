package iam.thevoid.pleisure.utils.ext

import iam.thevoid.e.isNotNull
import iam.thevoid.e.isNotNullOrBlank
import iam.thevoid.e.safe
import java.net.URI

fun URI.queryAsMap() =
    query.takeIf { it.isNotNullOrBlank() }
        ?.split("&")
        ?.associate { it.split("=").let { keyValue -> keyValue[0] to keyValue[1] } }
        .safe()

fun URI.hasQueryParameter(param : String) =
    queryAsMap()[param].isNotNull()

fun URI.queryParameter(param : String) =
    queryAsMap()[param]