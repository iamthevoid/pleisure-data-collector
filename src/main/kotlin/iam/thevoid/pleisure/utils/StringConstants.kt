
package iam.thevoid.pleisure.utils

const val SPACES_RGX = "\\s*"
const val HYPHEN_RGX = "\\p{Pd}"
const val RU_UNTIL_RGX = "${SPACES_RGX}по$SPACES_RGX"

const val HYPHEN = "-"
const val DASH = "—"
