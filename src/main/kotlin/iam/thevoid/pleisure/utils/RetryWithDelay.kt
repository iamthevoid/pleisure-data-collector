package iam.thevoid.pleisure.utils

import kotlinx.coroutines.delay

class RetryWithDelay<T> {
    /**
     * Use negative value for forever retry. Use positive for retry count
     */
    var times: Int = 10

    /**
     * Retry delay
     */
    var timeoutMillis: Long = 2500

    /**
     * Check exception. If true than rethrow
     */
    var exceptionFilter: (Throwable) -> Boolean = { false }

    /**
     * Local counter of retries
     */
    private var time = 0

    @Suppress("ConvertTwoComparisonsToRangeCheck")
    suspend fun execute(factory: suspend () -> T): T = try {
        factory()
    } catch (e: Throwable) {
        e.printStackTrace()
        if (exceptionFilter(e) || (times > 0 && time > times))
            throw e
        delay(timeoutMillis)
        execute(factory)
    }
}