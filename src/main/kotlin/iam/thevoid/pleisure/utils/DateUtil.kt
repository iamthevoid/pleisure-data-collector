package iam.thevoid.pleisure.utils

import iam.thevoid.e.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtil {

    val DEFAULT_DATE
        get() = Date(0)


    fun parseDateOrThrow(date: String, vararg formats: DateFormat): Date = formats.first().let { format ->
        try {
            format.parse(date)
        } catch (e: ParseException) {
            formats.takeLast(formats.size - 1).let { other ->
                if (other.isEmpty()) throw e
                parseDateOrThrow(date, *other.toTypedArray())
            }
        }
    }

    fun dateFormat(pattern: String, locale: Locale = Locale.getDefault()) =
        SimpleDateFormat(pattern, locale)

    fun extractDate(dateStr: String, vararg format: SimpleDateFormat): Date {

        val date = parseDateOrThrow(dateStr, *format).calendar

        // Find out date if not set
        val now = currentDate.calendar
        if (date.year == 1970) {

            while (date.year < now.year)
                date.set(Calendar.YEAR, date.year + 1)

            if (date.before(now))
                if (date.month < now.month || (date.month == now.month && date.dayOfMonth < now.dayOfMonth))
                    date.set(Calendar.YEAR, date.year + 1)
        }

        return date.time
    }

    fun makeDaysRange(start: Date, end: Date): List<Date> {
        val range = mutableListOf<Date>()
        val pointer = Calendar.getInstance().apply { time = start }
        while (pointer.timeInMillis <= end.time) {
            range.add(pointer.time)
            pointer.add(Calendar.DAY_OF_MONTH, 1)
        }
        return range
    }

}