package iam.thevoid.pleisure.utils

import iam.thevoid.e.safe
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

object Log {

    private val logger by lazy { Logger.getGlobal() }

    fun request(vararg objs: Any?) =
            objs.forEach { logD("$it".replace("([\\r\\n\\t])".toRegex(), ""), "http") }

    fun d(vararg objs: Any?, tag: String? = getCallerClassName()) =
        objs.forEach { logD(it, tag) }

    fun e(vararg objs: Any?, tag: String? = getCallerClassName()) =
        objs.forEach { logE(it, tag) }

    fun e(t: Throwable, tag: String? = getCallerClassName()) = logE(t, tag)

    private fun logE(obj : Throwable, tag: String?) {
        logger.log(Level.WARNING, obj) { "Thread: ${Thread.currentThread()}\n$tag" }
    }

    private fun logE(obj : Any?, tag : String?) {
        logger.log(Level.WARNING, "Thread: ${Thread.currentThread()}, $tag: $obj")
    }

    private fun logD(obj : Any?, tag : String? = null) {
        println("${Date()} / ${Thread.currentThread()}:${if (tag.isNullOrBlank()) "" else " $tag:"} $obj")
    }

    private fun getCallerClassNameShort(): String? =
        getCallerClassName().safe().split(".").let { it[it.size - 1] }

    private fun getCallerClassName(): String? {
        val stElements = Thread.currentThread().stackTrace
        for (i in 1 until stElements.size) {
            val ste = stElements[i]
            if (ste.className != Log::class.java.name && ste.className.indexOf("java.lang.Thread") != 0) {
                return ste.className
            }
        }
        return null
    }

}

fun String?.reportEmpty(tag : String = "") : String =
        also { if (isNullOrBlank()) Log.e("${if (tag.isEmpty()) "" else "$tag: "}is ${if (this == null) "null" else if (this.isEmpty()) "empty" else "blank"}") }.safe()