package parser

import iam.thevoid.e.*
import iam.thevoid.pleisure.data.api.seller.concertru.ConcertRuDateParser
import iam.thevoid.pleisure.data.db._manytoone.model.Period
import org.junit.Assert
import org.junit.Test
import java.util.*

class DateTimeTest() {

    @Test
    fun concertRuDates() {
        listOf("25 октября 2019 г. 10:00")
            .testConcertRu { checkDateTime(10, 0, 25, Calendar.OCTOBER, 2019) }
        listOf("22 — 23 июня 14:00 ")
            .testConcertRu {
                checkPeriod(
                    calendar(22, Calendar.JUNE, 14, 0),
                    calendar(23, Calendar.JUNE, 14, 0)
                )
            }
        listOf("11-14 июля 2019 начало в 10:00 ")
            .testConcertRu {
                checkPeriod(
                    calendar(11, Calendar.JULY, 14, 0, 2019),
                    calendar(14, Calendar.JULY, 14, 0, 2019)
                )
            }
        listOf("Без билета не действителен ")
            .testConcertRu { checkIfOpenDate() }
        listOf("Открытая дата ")
            .testConcertRu { checkIfOpenDate() }
        listOf("действителен в один из дней 03.01 по 06.01")
            .testConcertRu {
                checkPeriod(
                    calendar(3, Calendar.JANUARY),
                    calendar(6, Calendar.JANUARY)
                )
            }
        listOf("Талон на подарок \"Рюкзак помощника Деда Мороза\" ")
            .testConcertRu { checkIfOpenDate() }
        listOf("1 ноября 20:00")
            .testConcertRu { checkDateTime(20, 0, 1, Calendar.NOVEMBER) }
        listOf("08.02.2019 - 31.12.2019 ")
            .testConcertRu {
                checkPeriod(
                    calendar(8, Calendar.FEBRUARY, year = 2019),
                    calendar(31, Calendar.DECEMBER, year = 2019)
                )
            }
        listOf("11 июля 2020 19:00")
            .testConcertRu { checkDateTime(19, 0, 11, Calendar.JULY, 2020) }
        listOf("22-25 октября 2019 г. начало в 10:00 ")
            .testConcertRu {
                checkPeriod(
                    calendar(22, Calendar.OCTOBER, 10, 0, 2019),
                    calendar(25, Calendar.OCTOBER, 10, 0, 2019)
                )
            }
        listOf("24.10.2020 19:00 ")
            .testConcertRu { checkDateTime(19, 0, 24, Calendar.OCTOBER, 2020) }
        listOf("15.11.2020 19:00 ")
            .testConcertRu { checkDateTime(19, 0, 15, Calendar.NOVEMBER, 2020) }
        listOf("11.12.2020 19:00:00 ")
            .testConcertRu { checkDateTime(19, 0, 11, Calendar.DECEMBER, 2020) }
        listOf("19.01.2020 ")
            .testConcertRu { checkDate(19, Calendar.JANUARY, 2020) }
        listOf("20.06")
            .testConcertRu { checkDate(20, Calendar.JUNE) }
        listOf("20.06 - 30.06.2019г.")
            .testConcertRu {
                checkPeriod(
                    calendar(20, Calendar.JUNE, year = 2019),
                    calendar(30, Calendar.JUNE, year = 2019)
                )
            }
        listOf("10.06.2019 - 11.06.2019 начало сеанса в 17:30")
            .testConcertRu {
                checkPeriod(
                    calendar(10, Calendar.JUNE, 17, 30, 2019),
                    calendar(11, Calendar.JUNE, 17, 30, 2019)
                )
            }
        listOf("17.08.-18.08.2019 в 10:00")
            .testConcertRu {
                checkPeriod(
                    calendar(17, Calendar.AUGUST, 10, 0, 2019),
                    calendar(18, Calendar.AUGUST, 10, 0, 2019)
                )
            }
        listOf("10-11 августа 2019г. начало в 19:00 ")
            .testConcertRu {
                checkPeriod(
                    calendar(10, Calendar.AUGUST, 19, 0, 2019),
                    calendar(11, Calendar.AUGUST, 19, 0, 2019)
                )
            }
        listOf("2 ноября 23:45", "30 ноября 23:45", "4 января 23:45", "15 февраля 23:45", "7 марта 23:45")
            .testConcertRu {
                checkDateTimes(
                    calendar(2, Calendar.NOVEMBER, 23, 45, syntheticYear(2, Calendar.NOVEMBER)),
                    calendar(30, Calendar.NOVEMBER, 23, 45, syntheticYear(30, Calendar.NOVEMBER)),
                    calendar(4, Calendar.JANUARY, 23, 45, syntheticYear(4, Calendar.JANUARY)),
                    calendar(15, Calendar.FEBRUARY, 23, 45, syntheticYear(15, Calendar.FEBRUARY)),
                    calendar(7, Calendar.MARCH, 23, 45, syntheticYear(7, Calendar.MARCH))
                )
            }
    }

    private fun List<String>.testConcertRu(block: List<Period>.() -> Unit) =
        ConcertRuDateParser.parseRawDates(this).apply {
            block()
            println(
                if (size <= 1)
                    "[${joinToString()}]\n"
                else
                    "${joinToString(separator = "\n", prefix = "[\n", postfix = "\n]")}\n"
            )
        }

    private fun List<Period>.checkIfOpenDate() {
        Assert.assertEquals(1, size)
        Assert.assertTrue(first().isOpenDate())
    }

    private fun List<Period>.checkDateTimes(vararg calendar: Calendar) {
        Assert.assertEquals(calendar.size, size)
        forEachIndexed { index, period ->
            calendar[index].also {
                listOf(period).checkDateTime(it.hourOfDay, it.minute, it.dayOfMonth, it.month, it.year)
            }
        }
    }

    private fun List<Period>.checkDate(dayOMonth: Int, month: Int, year: Int = syntheticYear(dayOMonth, month)) {
        Assert.assertEquals(1, size)
        first().start.apply {
            calendar.also { cal: Calendar ->
                Assert.assertEquals(dayOMonth, cal.dayOfMonth)
                Assert.assertEquals(month, cal.month)
                Assert.assertEquals(year, cal.year)
            }
        }
    }

    private fun List<Period>.checkDateTime(
        hours: Int,
        mins: Int,
        dayOMonth: Int,
        month: Int,
        year: Int = syntheticYear(dayOMonth, month, hours, mins)
    ) {
        checkDate(dayOMonth, month, year)
        first().startTime.also {
            Assert.assertEquals(hours, it / 60)
            Assert.assertEquals(mins, it % 60)
        }
    }

    private fun List<Period>.checkPeriod(start: Calendar, end: Calendar) {
        var count = 1
        while (!(start.dayOfMonth == end.dayOfMonth && start.month == end.month && start.year == end.year)) {
            val get = get(count - 1)
            val date = get.start
            val time = get.startTime
            listOf(get).checkDateTime(time / 60, time % 60, date.dayOfMonth(), date.month(), date.year())
            start.add(Calendar.HOUR, 24)
            count++
        }
        Assert.assertEquals(count, size)
    }

    private fun syntheticYear(dayOMonth: Int, month: Int, hours: Int = 0, mins: Int = 0): Int =
        currentCalendar.let { now ->
            when {
                month > now.month -> now.year
                month < now.month -> now.year + 1
                dayOMonth > now.dayOfMonth -> now.year
                dayOMonth < now.dayOfMonth -> now.year + 1
                hours > now.hourOfDay -> now.year
                hours < now.hourOfDay -> now.year + 1
                mins > now.minute -> now.year
                else -> now.year + 1
            }
        }

    private fun calendar(dayOfMonth: Int, month: Int) =
        calendar(dayOfMonth, month, year = syntheticYear(dayOfMonth, month))

    private fun calendar(
        dayOfMonth: Int,
        month: Int,
        hour: Int = 0,
        minute: Int = 0,
        year: Int = syntheticYear(dayOfMonth, month, hour, minute)
    ) = Calendar.getInstance().apply {
        set(Calendar.YEAR, year)
        set(Calendar.MONTH, month)
        set(Calendar.DAY_OF_MONTH, dayOfMonth)
        set(Calendar.HOUR_OF_DAY, hour)
        set(Calendar.MINUTE, minute)
    }
}