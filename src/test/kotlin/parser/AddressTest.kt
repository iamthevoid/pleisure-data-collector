
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.api.seller.concertru.ConcertRu
import iam.thevoid.pleisure.data.db.main.model.Address
import iam.thevoid.pleisure.data.db.main.model.SellerUrl
import iam.thevoid.pleisure.data.file.FileHandler
import iam.thevoid.pleisure.utils.removeMetroStation
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class AddressTest {

    companion object {

        const val RUSSIA = "Россия"
        const val RU_code = "ru"

        private val barviha = listOf("567")
        private val balashiha = listOf("3434")
        private val chelyabinsk = listOf("3396")
        private val dubna = listOf("3459")
        private val elektrostal = listOf("2673")
        private val egorevsk = listOf("2948")
        private val ekb = listOf("975")
        private val essentuki = listOf("3245")
        private val ivanteevka = listOf("3518")
        private val kaluga = listOf("3220")
        private val kazan = listOf("3164", "1720", "2303", "3525", "1052", "3488", "1471")
        private val kolomna = listOf("1992")
        private val korolev = listOf("602")
        private val krasnodar = listOf("1501")
        private val krasnoyarsk = listOf("3067")
        private val krasnogorsk = listOf("2682", "420", "754", "3398", "2123")
        private val lyubertsy = listOf("2944")
        private val moscow
                by lazy { FileHandler.contentLines(FileHandler.res("concert_ru_places_ids_moscow", "test")) }
        private val mytischi = listOf("173", "847")
        private val nalchick = listOf("3533")
        private val nizhzniyNovgorod = listOf("882", "3553")
        private val novosibirsk = listOf("3414")
        private val orechovoZuevo = listOf("3310")
        private val pavlovskyPosad = listOf("3519")
        private val podolsk = listOf("839")
        private val petrozavodsk = listOf("2207")
        private val rostovOnDon = listOf("3542")
        private val samara = listOf("2020", "3554")
        private val saratov = listOf("3556")
        private val serpuhov = listOf("2950")
        private val spb
                by lazy { FileHandler.contentLines(FileHandler.res("concert_ru_places_ids_spb", "test")) }
        private val stavropol = listOf("3071", "3086")
        private val tomsk = listOf("3549")
        private val tula = listOf("3550", "3545")
        private val ufa = listOf("1184")
        private val vladikavkaz = listOf("3070")
        private val volgograd = listOf("2629")
        private val voronezh = listOf("3547")
        private val zhukovsky = listOf("2947")
        private val yaroslavl = listOf("3552", "2158")
    }

    @Test
    fun removeMetroTest() {
        val metros = FileHandler.contentLines(FileHandler.res("addresses_full", "test"))
        val metrosClean = FileHandler.contentLines(FileHandler.res("addresses_without_metro", "test"))
        metros.forEachIndexed { index, address -> Assert.assertEquals(metrosClean[index], removeMetroStation(address)) }
    }


    @Test
    fun testAddressCorrectFetching() {
        runBlocking {
            var counter = 0
            var size: Int
            val places = FileHandler.contentLines(FileHandler.res("concert_ru_places_ids", "test"))
            places.also { size = it.size }.forEach { id ->
                ConcertRu.place(ConcertRu.seller(), SellerUrl.create(url = id))
                    ?.also { place ->
                        println("${++counter} of $size")
                        val address = place.address
                        println(address?.raw)
                        when {
                            id == "3203" -> address.test("Санкт-Петербург", RUSSIA, RU_code, true)
                            korolev.contains(id) -> address.test("Королёв", RUSSIA, RU_code)
                            barviha.contains(id) -> address.test("Барвиха", RUSSIA, RU_code)
                            lyubertsy.contains(id) -> address.test("Люберцы", RUSSIA, RU_code)
                            tomsk.contains(id) -> address.test("Томск", RUSSIA, RU_code)
                            yaroslavl.contains(id) -> address.test("Ярославль", RUSSIA, RU_code)
                            elektrostal.contains(id) -> address.test("Электросталь", RUSSIA, RU_code)
                            samara.contains(id) -> address.test("Самара", RUSSIA, RU_code)
                            ufa.contains(id) -> address.test("Уфа", RUSSIA, RU_code)
                            essentuki.contains(id) -> address.test("Ессентуки", RUSSIA, RU_code)
                            vladikavkaz.contains(id) -> address.test("Владикавказ", RUSSIA, RU_code)
                            moscow.contains(id) -> address.test("Москва", RUSSIA, RU_code)
                            novosibirsk.contains(id) -> address.test("Новосибирск", RUSSIA, RU_code)
                            kolomna.contains(id) -> address.test("Коломна", RUSSIA, RU_code)
                            spb.contains(id) -> address.test("Санкт-Петербург", RUSSIA, RU_code)
                            voronezh.contains(id) -> address.test("Воронеж", RUSSIA, RU_code)
                            rostovOnDon.contains(id) -> address.test("Ростов-на-Дону", RUSSIA, RU_code)
                            kaluga.contains(id) -> address.test("Калуга", RUSSIA, RU_code)
                            krasnodar.contains(id) -> address.test("Краснодар", RUSSIA, RU_code)
                            krasnoyarsk.contains(id) -> address.test("Красноярск", RUSSIA, RU_code)
                            tula.contains(id) -> address.test("Тула", RUSSIA, RU_code)
                            kazan.contains(id) -> address.test("Казань", RUSSIA, RU_code)
                            nizhzniyNovgorod.contains(id) -> address.test("Нижний Новгород", RUSSIA, RU_code)
                            orechovoZuevo.contains(id) -> address.test("Орехово-Зуево", RUSSIA, RU_code)
                            saratov.contains(id) -> address.test("Саратов", RUSSIA, RU_code)
                            ekb.contains(id) -> address.test("Екатеринбург", RUSSIA, RU_code)
                            krasnogorsk.contains(id) -> address.test("Красногорск", RUSSIA, RU_code)
                            volgograd.contains(id) -> address.test("Волгоград", RUSSIA, RU_code)
                            mytischi.contains(id) -> address.test("Мытищи", RUSSIA, RU_code)
                            chelyabinsk.contains(id) -> address.test("Челябинск", RUSSIA, RU_code)
                            petrozavodsk.contains(id) -> address.test("Петрозаводск", RUSSIA, RU_code)
                            dubna.contains(id) -> address.test("Дубна", RUSSIA, RU_code)
                            podolsk.contains(id) -> address.test("Подольск", RUSSIA, RU_code)
                            pavlovskyPosad.contains(id) -> address.test("Павловский Посад", RUSSIA, RU_code)
                            balashiha.contains(id) -> address.test("Балашиха", RUSSIA, RU_code)
                            serpuhov.contains(id) -> address.test("Серпухов", RUSSIA, RU_code)
                            zhukovsky.contains(id) -> address.test("Жуковский", RUSSIA, RU_code)
                            egorevsk.contains(id) -> address.test("Егорьевск", RUSSIA, RU_code)
                            nalchick.contains(id) -> address.test("Нальчик", RUSSIA, RU_code)
                            ivanteevka.contains(id) -> address.test("Ивантеевка", RUSSIA, RU_code)
                            stavropol.contains(id) -> address.test("Ставрополь", RUSSIA, RU_code)
                            else -> throw RuntimeException("Could not be tested")
                        }
                    }
            }
        }
    }


    private fun Address?.test(city: String, country: String, countryCode: String, cityWeak: Boolean = false) {
        Assert.assertNotNull(this)
        this ?: return
        Assert.assertNotEquals(0, this.city?.length.safe())
        if (!cityWeak)
            Assert.assertEquals(city, this.city)
        Assert.assertEquals(country, this.country)
        Assert.assertEquals(countryCode, this.countryCode)
    }
}
