
import iam.thevoid.pleisure.data.api.seller.concertru.ConcertRuClient
import iam.thevoid.pleisure.data.api.services.location.here.HereClient
import iam.thevoid.pleisure.data.api.services.location.mapbox.MapboxClient
import iam.thevoid.pleisure.data.api.services.translate.GoogleTranslateClient
import kotlinx.coroutines.runBlocking
import org.junit.Test

class NetworkTest {

    @Test
    fun testMapBox() {
        runBlocking {
            val mapbox = MapboxClient.forward("ул. Большая Садовая, дом 10")
            println(mapbox)
        }
    }

    @Test
    fun translateTest() {
        runBlocking { println(GoogleTranslateClient.translate(query = "Молодёжная").output) }
    }

    @Test
    fun addressTest() {
        runBlocking {
            println("${listOf(
                    HereClient.forward(query = "ул. Свободы д.37"),
                    HereClient.forward(query = "Шмитовский 32А, стр. 1"),
                    HereClient.forward(query = "МО, Красногорский р-н, г. Красногорск, ул. Международная, д.12"),
                    HereClient.forward(query = "Балчуг, 7"),
                    HereClient.forward(query = "Эсто-Садок, Краснодарский край, наб. Лаванды"),
                    HereClient.forward(query = "г. Краснодар, ул. Береговая, 144"),
                    HereClient.forward(query = "ул. Лесная 30А"),
                    HereClient.forward(query = "47 км МКАД (внешняя сторона, вл.7)")
            )
            }")
        }
    }

    @Test
    fun place1616() {
        runBlocking {
            println(ConcertRuClient.place("1319")
                    .let { ConcertRuClient.place("1616") }
                    .let { ConcertRuClient.place("1052") })
        }
    }
}