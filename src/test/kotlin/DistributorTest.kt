import iam.thevoid.e.forEachApply
import iam.thevoid.e.safe
import iam.thevoid.pleisure.data.api.seller.Distributor
import iam.thevoid.pleisure.data.api.seller.concertru.ConcertRu
import iam.thevoid.pleisure.data.api.seller.redkassa.RedKassa
import iam.thevoid.pleisure.data.db._manytoone.model.Period
import iam.thevoid.pleisure.data.db.main.model.*
import iam.thevoid.pleisure.data.file.FileHandler
import iam.thevoid.pleisure.utils.expandError
import iam.thevoid.pleisure.utils.ext.skipUntil
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import java.io.File
import java.io.FileNotFoundException
import java.util.*

class DistributorTest {

    val logNamePrefix = "log/"

    private fun reportDaemonError(distributor: Distributor, throwable: Throwable) =
        FileHandler.writeToFile("log/TestError_${distributor::class.java.simpleName}.txt", expandError(throwable))
            .also { throwable.printStackTrace() }

    private fun storedSellerUrl() =
        try {
            FileHandler.contentLines(File("log/url")).firstOrNull()
        } catch (e: FileNotFoundException) {
            null
        }

    private fun storeellerUrl(url: String) {
        FileHandler.clearFile("log/url")
        FileHandler.writeToFile("log/url", url)
    }

    private val distributors = listOf(
        RedKassa
//        ConcertRu
    )


    val broken =
        mapOf(
            ConcertRu to listOf(
                "3399", // no coords
                "3548" // coords are x100
            ),
            RedKassa to listOf(
//                "kc_mid_rossii",        // lat and lon switched places
//                "kzal_pravitelstva_moskvi",        // lat and lon switched places
//                "usadiba_jazz_konakovo",        // lat and lon switched places
//                "shkola_skolkovo",        // lat and lon switched places
//                "bakhmetev",        // lat and lon switched places
//                "konferents_zal_nimba_nn",        // lat and lon switched places
//                "fabrika_bar_club_nn",        // lat and lon switched places
//                "parque_da_bela_vista",        // lat and lon bad
//                "kultura_center_rekord_nn",        // lat and lon switched places
//                "dom_poetov",        // lat and lon switched places
//                "trc_roll_hall",        // lat and lon switched places
//                "batashev_arena",        // lat and lon switched places
//                "avtobus",               // no coords
//                "kz_hrustalniy_yalta",               // no city
//                "kidburg_nn",               // no city
//                "sok_metallurg_forum",               // no city
//                "patriot_alabino",               // no city
//                "aerodrom_kubinka",               // no city
//                "park_patriot",               // no city
//                "inform_plus_nn"        // lat and lon switched places
            )
        )


    @Test
    fun testPipeline() {
        runBlocking {

            distributors.forEachApply {
                try {
                    seller().let { seller ->
                        seller.test()

                        val events = TreeMap<String, MutableList<Event>>()
                        sellerUrlsForPlaces(seller, SellerUrlType.PLACE)
                            .also { println("All = ${it.size}") }
                            .skipUntil {
                                storedSellerUrl() == null ||
                                        it.url == storedSellerUrl()
                            }
                            .also { println("Left = ${it.size}") }
                            .forEach { sellerUrl ->
                                if (broken[this]?.contains(sellerUrl.url).safe())
                                    return@forEach
                                sellerUrl.test()
                                storeellerUrl(sellerUrl.url)

                                place(seller, sellerUrl)?.let { place ->
                                    place.test()

                                    try {
                                        events(seller, place).forEach { event ->
                                            event.test()
                                            val key = "${place.name} (${place.address?.city.safe()})"
                                            val list = events[key] ?: mutableListOf()
                                            list.add(event)
                                            if (!events.containsKey(key))
                                                events[key] = list
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            }
                        println(events)
                    }
                } catch (e: Exception) {
                    reportDaemonError(this, e)
                }
            }

        }
    }

    private fun Seller?.test() {
        Assert.assertNotNull(this)
        this ?: return
        name.test()
        url.test()
        logoUrl.test()
        eventUrlPattern.test()
        placeUrlPattern.test()
    }


    private fun SellerUrl?.test() {
        Assert.assertNotNull(this)
        this ?: return
        url.test()
        type.test()
        seller.test()
        urlFull.test()
    }

    private fun Place?.test() {

        Assert.assertNotNull(this)
        this ?: return
        address.test()
        sellerUrl.test()
        sellerUrl.forEach { it.test() }
        name.test()
    }

    private fun Event?.test() {
        Assert.assertNotNull(this)
        this ?: return
        sellerUrl.test()
        sellerUrl.forEach { it.test() }
        name.test()
        genres.test()
        genres.forEach { it.test() }
        period.test()
        period.forEach { it.test() }
        place.test()
        image.test()
        image.forEach { it.test() }

        tags.test()

    }

    private fun Address?.test() {
        Assert.assertNotNull(this)
        this ?: return
        city.test()
        country.test()
        countryCode.test()
        raw.test()
        lat.test()
        lon.test()
    }

    private fun SellerUrlType?.test() {
        Assert.assertNotNull(this)
        this ?: return
        name.test()
    }

    private fun Genre?.test() {
        Assert.assertNotNull(this)
        this ?: return
        name.test()
//        Assert.assertNotEquals(Genre.UNKNOWN, name)
    }

    private fun Image?.test() {
        Assert.assertNotNull(this)
        this ?: return
        run { listOf(small, medium, big, origin) }.test()
    }

    private fun Period?.test() {
        Assert.assertNotNull(this)
        this ?: return
        startTime.test()
        startDate.test()
    }

    // Base

    private fun <T> List<T>?.test() {
        Assert.assertNotNull(this)
        this ?: return
        Assert.assertNotEquals(0, size)
    }

    private fun String?.test() {
        Assert.assertNotNull(this)
        this ?: return
        Assert.assertNotEquals(0, length)
    }

    private fun Float?.test() {
        Assert.assertNotNull(this)
        this ?: return
        Assert.assertNotEquals(0f, this)
    }

    private fun Int?.test() {
        Assert.assertNotNull(this)
        this ?: return
    }
}